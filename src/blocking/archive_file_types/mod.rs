pub mod compressed;
pub mod tar;
pub mod zip;

use crate::{
    result::{ArchiveError, ArchiveResult},
    ArchiveEntry, ArchiveManifest, ArchiveType, ExtractOptions,
};
use compressed::CompressedFile;
use effitfs::FileType;
use std::io::{BufReader, Seek, SeekFrom, Write};
use tar::TarFile;
use zip::ZipFile;

use super::{
    streams::ArchiveEntryStream,
    utils::fs::{ArchiveFileReadSeekTraits, FsFileProvider},
};

enum InnerArchiveFileType {
    ZipFile(ZipFile),
    TarFile(TarFile),
    Compressed(CompressedFile),
}

impl From<ArchiveType> for InnerArchiveFileType {
    fn from(archive_file_type: ArchiveType) -> Self {
        match archive_file_type {
            // It kills me, that this zip type isn't needed (zip64 or zip32... maybe it would be obvious when zip64 is added)
            // Or - just make zip32 and zip64 separate
            // I guess it only uses ziptype when it's reading entries... the rest is the same...
            ArchiveType::Zip(_) => Self::ZipFile(ZipFile),
            ArchiveType::Tar(t) => Self::TarFile(TarFile::from(t)),
            ArchiveType::Compressed(t) => Self::Compressed(CompressedFile::from(t)),
        }
    }
}

/// A blocking archive reader
pub struct SyncArchiveReader<'a, F: FsFileProvider>
where
    F::R: Seek,
{
    archive_file: &'a ArchiveManifest,
    fs_reader: &'a F,
}

impl<'a, F: FsFileProvider> SyncArchiveReader<'a, F>
where
    F::R: Seek,
{
    pub fn new(archive_file: &'a ArchiveManifest, fs_reader: &'a F) -> Self {
        Self {
            archive_file,
            fs_reader,
        }
    }

    /// Reads a file and returns archive
    pub fn read_manifest(file_path: &str, fs_provider: &F) -> ArchiveResult<ArchiveManifest>
    where
        F: FsFileProvider,
    {
        let reader = fs_provider.open_file(file_path)?;
        let mut reader = BufReader::new(reader);

        let file_path = file_path.to_string();
        let result = ZipFile::detect_binary(&mut reader)?;
        reader.seek(SeekFrom::Start(0))?;
        if result {
            let (a, e) = ZipFile::load(&mut reader)?;
            return Ok(ArchiveManifest::new(file_path, a, e));
        }

        let result = TarFile::detect_binary(&mut reader)?;
        reader.seek(SeekFrom::Start(0))?;
        if result {
            let (a, e) = TarFile::load(&mut reader)?;
            return Ok(ArchiveManifest::new(file_path, a, e));
        }

        let result = CompressedFile::detect_binary(&mut reader)?;
        reader.seek(SeekFrom::Start(0))?;
        if result {
            let (a, e) = CompressedFile::load(&file_path, reader)?;
            return Ok(ArchiveManifest::new(file_path, a, e));
        }

        Err(ArchiveError::UnsupportedFileType(format!(
            "Could not load {}",
            file_path
        )))
    }

    /// Unpack an entry into the write fs
    pub fn unpack_entry<W>(
        &self,
        entry: &ArchiveEntry,
        write_fs: &mut W,
        options: &ExtractOptions,
    ) -> ArchiveResult<(String, usize)>
    where
        W: FsFileProvider,
    {
        let target_path = options.build_entry_path(entry.get_name())?;
        let (target_path, data_len) = match entry.get_file_type() {
            FileType::Dir => {
                write_fs.mkdir_all(target_path.to_string())?;
                (target_path, 0)
            }
            //I saw errors here when target_path == "target/./targetfile.txt";; because it was not normalized probably so it was tryinig to create "."
            FileType::SymLink => {
                match entry.get_metadata().link_target() {
                    Some(link_target) => {
                        let parent_dir = target_path.directory();
                        write_fs.mkdir_all(parent_dir.to_string())?;
                        write_fs.create_symlink(link_target.to_owned(), target_path.to_string())?;
                    }
                    _ => {
                        return Err(ArchiveError::InvalidData(format!(
                            "symlink flag without a link target for entry {}",
                            entry.get_name()
                        )))?
                    }
                };

                (target_path, 0)
            }
            _ => {
                let parent_dir = target_path.directory();
                write_fs.mkdir_all(parent_dir.to_string())?;

                let mut reader = self
                    .fs_reader
                    .open_file(self.archive_file.get_file_path())?;
                let data = self.extract_entry(&mut reader, entry)?;
                {
                    let mut f = match write_fs.create_file(target_path.to_string()) {
                        Ok(f) => f,
                        Err(e) => {
                            println!("Error creating file '{}': {}", target_path, e);
                            return Err(e)?;
                        }
                    };

                    f.write_all(&data)?;
                    f.flush()?;
                }

                (target_path, data.len())
            }
        };

        Ok((target_path.to_string(), data_len))
    }

    /// Extract all entries into the write fs
    pub fn unpack_all<W>(&self, write_fs: &mut W, opt: &ExtractOptions) -> ArchiveResult<()>
    where
        W: FsFileProvider,
    {
        let mut paths = vec![];
        for e in self.archive_file.get_entries() {
            paths.push(e);

            self.unpack_entry(e, write_fs, opt)?;
        }

        paths.sort_by_key(|e| e.get_name().len());
        for e in paths.into_iter().rev() {
            self.sync_entry_ownership(e, write_fs, opt)?;
            self.sync_entry_mode(e, write_fs, opt)?;
            self.sync_entry_timestamps(e, write_fs, opt)?;
        }

        Ok(())
    }

    fn sync_entry_mode<W>(
        &self,
        entry: &ArchiveEntry,
        write_fs: &mut W,
        options: &ExtractOptions,
    ) -> ArchiveResult<()>
    where
        W: FsFileProvider,
    {
        if !options.use_permissions {
            return Ok(());
        }

        let entry_name = entry.get_name();
        let target_path = options.build_entry_path(entry_name)?;

        write_fs.lchmod(
            target_path.to_string(),
            entry.get_metadata().permissions().get_mode(),
        )?;

        Ok(())
    }

    fn sync_entry_ownership<W>(
        &self,
        entry: &ArchiveEntry,
        write_fs: &mut W,
        options: &ExtractOptions,
    ) -> ArchiveResult<()>
    where
        W: FsFileProvider,
    {
        if !options.use_ownership {
            return Ok(());
        }

        let target_path = options.build_entry_path(entry.get_name())?;

        write_fs.lchown(
            target_path.to_string(),
            entry.get_metadata().permissions().get_uid(),
            entry.get_metadata().permissions().get_gid(),
        )?;

        Ok(())
    }

    /// I wasn't smart enough to understand how to keep directory file times set
    fn sync_entry_timestamps<W>(
        &self,
        entry: &ArchiveEntry,
        write_fs: &mut W,
        options: &ExtractOptions,
    ) -> ArchiveResult<()>
    where
        W: FsFileProvider,
    {
        if !options.use_timestamps {
            return Ok(());
        }

        let target_path = options.build_entry_path(entry.get_name())?;

        if cfg!(target_os = "windows") && entry.get_file_type() == FileType::SymLink {
            // Windows does not play well with sym links
            return Ok(());
        }

        write_fs.ltouch(
            target_path.to_string(),
            entry.get_metadata().accessed(),
            entry.get_metadata().modified(),
            entry.get_metadata().created(),
        )?;

        Ok(())
    }

    fn extract_entry<R>(&self, reader: &mut R, entry: &ArchiveEntry) -> ArchiveResult<Vec<u8>>
    where
        R: ArchiveFileReadSeekTraits,
    {
        let inner_type = InnerArchiveFileType::from(self.archive_file.get_type());
        match inner_type {
            InnerArchiveFileType::ZipFile(f) => f.extract_entry(reader, entry),
            InnerArchiveFileType::TarFile(f) => f.extract_entry(reader, entry),
            InnerArchiveFileType::Compressed(f) => f.extract_entry(reader, entry),
        }
    }

    /// Open single entry
    pub fn open_entry(&self, entry: &ArchiveEntry) -> ArchiveResult<ArchiveEntryStream<F::R>> {
        let inner_type = InnerArchiveFileType::from(self.archive_file.get_type());
        let reader = self
            .fs_reader
            .open_file(self.archive_file.get_file_path())?;
        match inner_type {
            InnerArchiveFileType::ZipFile(f) => f.open_entry(reader, entry),
            InnerArchiveFileType::TarFile(f) => f.open_entry(reader, entry),
            InnerArchiveFileType::Compressed(f) => f.open_entry(reader, entry),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{blocking::tests::ReadOnlyByteFsProvider, result::ArchiveError};
    use effitfs::PathBuf;

    use super::SyncArchiveReader;

    #[test]
    pub fn test_load_unknown_filetype() {
        let file_path = "target/test_load_unknown_filetype.file";
        let bytes = "this is a test".as_bytes().to_vec();

        let fs = ReadOnlyByteFsProvider::new(PathBuf::from(file_path), bytes);

        let result = SyncArchiveReader::read_manifest(file_path, &fs);

        match result {
            Ok(_) => panic!("Should be Err(UnsupportedFileType)"),
            Err(ArchiveError::UnsupportedFileType(_)) => {}
            Err(e) => panic!("Expected UnsupportedFileType but got {e}"),
        }
    }
}
