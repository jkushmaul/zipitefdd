use crate::{
    blocking::utils::fs::ArchiveFileReadSeekTraits,
    result::{ArchiveError, ArchiveResult},
    zip::{
        bocd::CentralDirectoryHead,
        zip32::{LocalFileHeader, Zip32CentralDirectoryTail},
    },
    ArchiveEntry,
};
use binrw::BinRead;
use std::io::SeekFrom;

/*
 * The goal of this is to be able to concurrently read zip entries.
 * One could simply just re-open the file and make a new ziparchive object every time
 * but there is expensive enough initialization that should be avoided by having a reusable object.
 *
 * One does not simply concurrently read from the same file handle.  So, to avoid this, the file
 * is not retained but the indexed contents are.
 * That would allow a file to be mutated between calls.  To avoid that, the retained object is a write-lock
 * on the file.
 * The file is then opened on-demand providing unique file handles at any point needed.
 *
 */

pub struct Zip32;

/// Minimum logic to support  trait
impl Zip32 {
    fn read_cd<R: ArchiveFileReadSeekTraits>(
        reader: &mut R,
        cd_tail: &Zip32CentralDirectoryTail,
    ) -> ArchiveResult<Vec<CentralDirectoryHead>> {
        reader.seek(SeekFrom::Start(cd_tail.central_directory_offset as u64))?;

        let mut v = Vec::new();
        for _ in 0..cd_tail.total_central_directory_records {
            let cd_head: CentralDirectoryHead = CentralDirectoryHead::read_le(reader)
                .map_err(|e| ArchiveError::InvalidData(format!("{e}")))?;
            v.push(cd_head);
        }

        Ok(v)
    }

    fn read_local_files<R: ArchiveFileReadSeekTraits>(
        reader: &mut R,
        directory: Vec<CentralDirectoryHead>,
    ) -> ArchiveResult<Vec<ArchiveEntry>> {
        let mut v = Vec::new();

        for (i, cd) in directory.into_iter().enumerate() {
            reader
                .seek(SeekFrom::Start(cd.local_file_header_offset as u64))
                .unwrap();
            let lf = LocalFileHeader::read_le(reader)
                .map_err(|e| ArchiveError::InvalidData(format!("{e}")))
                .unwrap_or_else(|e| {
                    panic!(
                        "Failed to read LocalFileHeader[{i}] starting at ofs {}: {}\nFrom CD {:#x?}",
                        cd.local_file_header_offset, e, cd
                    )
                });
            let offset = reader.stream_position()?;
            let entry = lf.into_archive_entry(offset, i, &cd).unwrap();
            v.push(entry);
        }

        Ok(v)
    }

    pub fn read_entries<R: ArchiveFileReadSeekTraits>(
        reader: &mut R,
    ) -> ArchiveResult<Vec<ArchiveEntry>> {
        let cd_tail = Zip32CentralDirectoryTail::read_le(reader)
            .map_err(|e| ArchiveError::InvalidData(format!("{e}")))
            .unwrap();

        let directory = Self::read_cd(reader, &cd_tail).unwrap();
        let entries = Self::read_local_files(reader, directory).unwrap();

        Ok(entries)
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Seek, SeekFrom};

    use crate::blocking::archive_file_types::zip::zip32::Zip32;

    #[test]
    pub fn loads_zip32() {
        let mut reader = crate::blocking::archive_file_types::zip::tests::zip32_binr();
        reader
            .seek(SeekFrom::Start(
                crate::test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD,
            ))
            .unwrap();

        let entries = Zip32::read_entries(&mut reader).unwrap();

        assert_eq!(entries.len(), 1);
        assert_eq!(entries[0].get_name(), "itwasjustatest.txt");
        assert_eq!(
            entries[0].get_metadata().len() as usize,
            crate::test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len()
        );
    }
}
