pub mod zip32;

use crate::{
    blocking::{
        streams::{ArchiveEntryStream, DecompressStream},
        utils::{fs::ArchiveFileReadSeekTraits, WindowedReader},
    },
    result::{ArchiveError, ArchiveResult},
    zip::{constants, ZipFileType},
    ArchiveEntry, ArchiveType,
};
use effitfs::FileType;
use std::{
    collections::HashSet,
    io::{BufRead, BufReader, Read, SeekFrom},
};
use zip32::Zip32;

pub struct ZipFile;

//read backwards by seeking earlier, filling buffer, finding
//expects reader to already be positioned at starting, er, um ending point. you know what I mean.
//(end of file usually)
//The left over stream position will be the last occurence
fn rfind_u32<R: ArchiveFileReadSeekTraits + BufRead>(
    reader: &mut R,
    targets: &[u32],
) -> ArchiveResult<Option<(usize, u64)>> {
    let mut byte_targets: Vec<[u8; 4]> = Vec::new();
    for x in targets {
        let b = x.to_le_bytes();
        let c = [b[0], b[1], b[2], b[3]];
        byte_targets.push(c)
    }

    let mut buf;

    let total_length = reader.seek(SeekFrom::End(0))? as usize;
    let mut buf_size: usize = 4096;
    let mut current_position = if buf_size > total_length {
        buf_size = total_length;
        buf = vec![0u8; buf_size];
        reader.seek(SeekFrom::Start(0))?
    } else {
        buf = vec![0u8; buf_size];
        reader.seek(SeekFrom::End(-(buf_size as i64)))?
    };

    loop {
        reader.read_exact(&mut buf)?;

        //search buffer
        if let Some((needle_index, buffer_offset)) = rindex_u32(byte_targets.as_slice(), &buf) {
            //align to the offset returned, which is forward from current
            current_position += buffer_offset as u64;
            reader.seek(SeekFrom::Start(current_position))?;
            return Ok(Some((needle_index, current_position)));
        }

        if current_position < 3 {
            break;
        }

        //Use padding to solve boundary issues
        let look_back: i64 = buf_size as i64 - 4;

        //if the current position is less than lookback, then this is the last pull.
        if current_position < look_back as u64 {
            //read exact only what's left.
            buf = vec![0u8; current_position as usize];
            reader.seek(SeekFrom::Start(0))?;
            current_position = 0;
        } else {
            //after the first read, only seek back buf_size - 3 to handle edge cases
            current_position = reader.seek(SeekFrom::Current(-look_back))?;
        }
    }
    Ok(None)
}

/**
 * Finds the first occurence of needle from tail.
 *
 * Returns Optionial pair of indexes, the first index is the index in needles that
 *  had the first (or last?) match.  The second is the starting offset in
 *  haystack.
 *
 *
 * I'd like to make this a bit more generic thanjust for 4 byte sizes.
 */
fn rindex_u32(needles: &[[u8; 4]], haystack: &[u8]) -> Option<(usize, usize)> {
    let mut i: usize = haystack.len();

    /*
     * Starting from the end, look at current haystack
     * and compare that against each of the needles last char.
     * on each match, fetch a slice of needle size back (so the last char of that is the char just checked)
     *  from haystack and compare it to needle.
     *
     * It's a race to the finish line and there can be only one.
     * If two match - the first one gets the prize.
     *
     */
    while i > 3 {
        i -= 1;

        //Only check last char of needle for current haystack, for each needle

        for (j, n) in needles.iter().enumerate() {
            //Name your favorite dinosaur
            if haystack[i] == n[3] {
                //Velociraptor!
                let target_offset = i - 3;
                let slice = &haystack[target_offset..i + 1];
                //Did we just become best friends!?
                if slice == n {
                    //YUP!
                    return Some((j, target_offset));
                }
            }
        }
    }

    None
}

impl ZipFile {
    pub fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadSeekTraits,
    {
        let mut buf = vec![0u8; constants::MAGIC_PKZIP.len()];
        reader.read_exact(&mut buf)?;

        Ok(buf == constants::MAGIC_PKZIP.as_slice())
    }

    pub fn load<R>(reader: &mut R) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadSeekTraits,
    {
        let mut reader = BufReader::new(reader);
        let reader = &mut reader;
        let (zip_file_type, _) = Self::find_eocd(reader)?;

        let mut entries = match zip_file_type {
            ZipFileType::Zip32 => Zip32::read_entries(reader)?,
            ZipFileType::Zip64 => {
                return Err(ArchiveError::UnsupportedFileType(
                    "Zip64 is not supported".to_string(),
                ))
            }
        };
        let possible_targets: HashSet<String> = entries
            .iter()
            .filter(|e| e.get_file_type() == FileType::File)
            .map(|e| e.get_name().to_owned())
            .collect();

        //TODO: Post process all entries with symnlink type, extract it's data and set the link target.
        for e in entries
            .iter_mut()
            .filter(|e| e.get_file_type() == FileType::SymLink)
        {
            let data = Self::static_extract_entry(reader, e)?;
            let link_name = String::from_utf8_lossy(&data).to_string();
            if possible_targets.contains(&link_name) {
                e.metadata_mut().set_link_target(Some(link_name));
            } else {
                e.metadata_mut().set_file_type(FileType::File);
            }
        }

        Ok((ArchiveType::Zip(zip_file_type), entries))
    }

    fn find_eocd<R>(reader: &mut R) -> ArchiveResult<(ZipFileType, u64)>
    where
        R: ArchiveFileReadSeekTraits + BufRead,
    {
        reader.seek(SeekFrom::End(0))?;

        //try each signature
        let needles = [constants::Z32_EOCD_SIG];

        let (_, offset) = match rfind_u32(reader, &needles)? {
            Some(a) => a,
            None => {
                return Err(ArchiveError::SignatureNotFound(
                    "Could not find Z32 EOCD".to_string(),
                ))
            }
        };
        //position should be offset, seek to it anyways
        reader.seek(SeekFrom::Start(offset))?;

        Ok((ZipFileType::Zip32, offset))
    }
    fn static_extract_entry<R>(reader: &mut R, dir_entry: &ArchiveEntry) -> ArchiveResult<Vec<u8>>
    where
        R: ArchiveFileReadSeekTraits,
    {
        reader.seek(SeekFrom::Start(dir_entry.get_compressed_offset()))?;
        let reader = WindowedReader::new(reader, dir_entry.get_compressed_size() as u64);

        let mut reader = DecompressStream::new(dir_entry.get_compression_codec(), reader);
        let mut buf = vec![0u8; dir_entry.get_uncompressed_size()];
        reader.read_exact(&mut buf)?;

        let crc = crc32fast::hash(&buf);

        if crc as u64 != dir_entry.get_uncompressed_crc() {
            return Err(ArchiveError::CrcMismatch(dir_entry.get_name().to_string()));
        }

        Ok(buf)
    }

    pub fn extract_entry<'a, R>(
        &self,
        reader: &'a mut R,
        dir_entry: &'a ArchiveEntry,
    ) -> ArchiveResult<Vec<u8>>
    where
        R: ArchiveFileReadSeekTraits,
    {
        Self::static_extract_entry(reader, dir_entry)
    }

    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        mut reader: R,
        dir_entry: &ArchiveEntry,
    ) -> ArchiveResult<ArchiveEntryStream<R>> {
        let compressed_offset = dir_entry.get_compressed_offset();
        let compression_codec = dir_entry.get_compression_codec();
        let compressed_size = dir_entry.get_compressed_size();

        reader.seek(SeekFrom::Start(compressed_offset))?;
        let reader = DecompressStream::new(compression_codec, reader);
        let reader = WindowedReader::new(reader, compressed_size as u64);
        let reader = ArchiveEntryStream::Zip(reader);

        Ok(reader)
    }
}

#[cfg(test)]
pub mod tests {
    use crate::{
        blocking::archive_file_types::zip::{zip32::Zip32, ZipFile},
        test_utils,
        zip::ZipFileType,
        ArchiveType,
    };
    use std::io::{Cursor, Seek, SeekFrom};

    pub fn zip32_binr<'a>() -> Cursor<&'a [u8]> {
        Cursor::new(crate::test_utils::zip32::IT_WAS_JUST_A_TEST_ZIP32)
    }

    #[test]
    fn test_find_first_sig_zip32() {
        let mut reader = zip32_binr();
        let result = ZipFile::find_eocd(&mut reader);

        assert!(result.is_ok());
        let result = result.unwrap();
        assert_eq!(result.0, ZipFileType::Zip32);
        assert_eq!(result.1, crate::test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD);
    }

    #[test]
    fn test_read_zip_type_zip32() {
        let mut reader = zip32_binr();

        let result = ZipFile::load(&mut reader);

        assert!(result.is_ok());
        let (zip_file_type, entries) = result.unwrap();

        assert_eq!(zip_file_type, ArchiveType::Zip(ZipFileType::Zip32));
        assert_eq!(1, entries.len());

        let entry = entries.first().unwrap();

        let result = ZipFile;

        let asset = result.extract_entry(&mut reader, entry).unwrap();
        assert_eq!(
            asset.len(),
            crate::test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len()
        );
    }

    #[test]
    fn loads_zip32_extracts_store() {
        let mut reader = zip32_binr();
        reader
            .seek(SeekFrom::Start(test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD))
            .unwrap();

        let entries = Zip32::read_entries(&mut reader).unwrap();
        let entry = entries.first().unwrap();
        let zip = ZipFile;
        let buf = match zip.extract_entry(&mut reader, entry) {
            Ok(buf) => buf,
            Err(e) => {
                panic!("Failed to extract entry: {}", e);
            }
        };

        assert_eq!(buf.len(), test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len());
        let actual = String::from_utf8_lossy(&buf).to_string();
        assert_eq!(actual, test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT);

        assert_eq!(entries.len(), 1);
        assert_eq!(
            entries[0].get_name(),
            test_utils::zip32::IT_WAS_JUST_A_TEST_FILE
        );
    }
}
