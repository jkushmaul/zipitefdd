use crate::blocking::streams::{ArchiveEntryStream, DecompressStream};
use crate::blocking::utils::fs::{ArchiveFileReadSeekTraits, ArchiveFileReadTraits};
use crate::result::{ArchiveError, ArchiveResult};
use crate::ArchiveType;
use crate::{ArchiveEntry, CompressionCodec};
use effitfs::FileType;
use std::io::{BufReader, Read, Seek};
use std::{io::SeekFrom, path::Path};

/// Compressed file needs to know the original file name - because there is no "Manifest", it's just
/// compressed bytes.  In order to have a single "ArchiveEntry", a name is needed.
pub struct CompressedFile {
    file_type: CompressionCodec,
}
impl From<CompressionCodec> for CompressedFile {
    fn from(file_type: CompressionCodec) -> Self {
        Self { file_type }
    }
}

impl CompressedFile {
    pub fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadSeekTraits,
    {
        if let CompressionCodec::STORE = Self::identify_compression(reader)? {
            return Ok(false);
        }
        Ok(true)
    }
    pub fn load<R>(
        file_name: &str,
        mut reader: R,
    ) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadSeekTraits,
    {
        let file_type = match Self::identify_compression(&mut reader)? {
            CompressionCodec::STORE => return Err(ArchiveError::UnidentifiedCompressionmethod),
            s => s,
        };

        let p = Path::new(file_name);
        let file_name = match p.file_name() {
            Some(s) => s.to_string_lossy().to_string(),
            None => {
                return Err(ArchiveError::Other(format!(
                    "Could not decode filename: {}",
                    file_name
                )));
            }
        };

        let mut entries = vec![];

        let entry = ArchiveEntry::new(
            file_name,
            0,
            // Say what?  I guess it's because the decompression is one layer out - the actual entry inside the compressed file, isn't compressed when this is used... but it is true, it does not make sense.
            CompressionCodec::STORE,
            0,
            0,
            0,
            0,
            0,
            FileType::File,
            None,
        );
        entries.push(entry);

        Ok((ArchiveType::Compressed(file_type), entries))
    }

    pub fn identify_compression<R>(reader: &mut R) -> ArchiveResult<CompressionCodec>
    where
        R: ArchiveFileReadTraits + Seek,
    {
        let mut buf = [0u8; 6];
        reader.read_exact(&mut buf)?;
        reader.seek(SeekFrom::Start(0))?;
        Ok(CompressionCodec::from(buf))
    }

    pub fn extract_entry<'a, R: ArchiveFileReadSeekTraits>(
        &'a self,
        reader: &'a mut R,
        _: &'a ArchiveEntry,
    ) -> ArchiveResult<Vec<u8>> {
        let file_type = self.file_type;

        let reader = BufReader::new(reader);
        let mut reader = DecompressStream::new(file_type, reader);
        let mut buf = vec![];
        reader.read_to_end(&mut buf)?;

        Ok(buf)
    }

    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        reader: R,
        _entry: &ArchiveEntry,
    ) -> ArchiveResult<ArchiveEntryStream<R>> {
        let file_type = self.file_type;
        let reader = DecompressStream::new(file_type, reader);
        let reader = ArchiveEntryStream::CompressedFile(reader);
        Ok(reader)
    }
}

#[cfg(test)]
mod test {
    use crate::{
        blocking::{
            archive_file_types::compressed::CompressedFile, tests::ReadOnlyByteFsProvider,
            utils::fs::FsFileProvider,
        },
        compressed::constants,
        result::ArchiveResult,
        ArchiveEntry, CompressionCodec,
    };
    use effitfs::FileType;
    use std::io::{BufReader, Cursor, Read};

    #[test]
    pub fn identify_compression() -> ArchiveResult<()> {
        let file_path = "test_files/tar/test.tar.gz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();

        let file_type = CompressedFile::identify_compression(&mut reader)?;
        if file_type == CompressionCodec::GZIP {
            return Ok(());
        };
        panic!("file_type was not Gz: {:?}", file_type);
    }

    #[test]
    pub fn test_extract_entry_uncompressed() {
        let expected = "test".as_bytes();
        let c = CompressedFile::from(CompressionCodec::STORE);
        let r = Cursor::new(&expected);
        let mut r = BufReader::new(r);
        let entry = ArchiveEntry::new(
            "test".to_owned(),
            0,
            CompressionCodec::STORE,
            0,
            1,
            2,
            3,
            4,
            FileType::File,
            None,
        );
        let mut r = c.open_entry(&mut r, &entry).unwrap();
        let mut v = vec![];
        r.read_to_end(&mut v).unwrap();

        assert_eq!(v.as_slice(), expected)
    }

    #[test]
    pub fn test_load_no_magic() {
        let expected = "test longer than six".as_bytes();
        let r = Cursor::new(&expected);
        let file_name = "";
        let result = CompressedFile::load(file_name, r);
        assert!(result.is_err());
    }

    #[test]
    pub fn test_load_gz_bad_filename() {
        let mut expected = vec![];
        for b in constants::MAGIC_GZ {
            expected.push(b);
        }
        for i in 0..16 {
            expected.push(i);
        }
        let r = Cursor::new(&expected);
        let file_name = "";
        let result = CompressedFile::load(file_name, r);
        assert!(result.is_err());
    }
}
