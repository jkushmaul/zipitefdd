pub mod reader;
pub mod xtheader;

use crate::{
    blocking::{
        streams::{ArchiveEntryStream, DecompressStream},
        utils::{
            fs::{ArchiveFileReadSeekTraits, ArchiveFileReadTraits},
            WindowedReader,
        },
    },
    result::ArchiveResult,
    tar, ArchiveEntry, ArchiveType, CompressionCodec,
};
use reader::TarFileReader;
use std::io::{BufRead, BufReader, Read, SeekFrom};

use super::compressed::CompressedFile;

pub struct TarFile {
    file_type: CompressionCodec,
}
impl From<CompressionCodec> for TarFile {
    fn from(file_type: CompressionCodec) -> Self {
        Self { file_type }
    }
}

// The use of 'static here concerns me - as in "I use globals for everything" in C or "I make everything a static field + init" in java
impl TarFile {
    pub fn load<R>(reader: &mut R) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadSeekTraits + BufRead,
    {
        let file_type = CompressedFile::identify_compression(reader)?;
        reader.seek(SeekFrom::Start(0))?;
        let mut container = TarFileReader::new(file_type, reader);
        let entries = container.read_entries()?;

        let archive_file_type = ArchiveType::Tar(file_type);
        Ok((archive_file_type, entries))
    }

    pub fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadSeekTraits + BufRead,
    {
        let file_type = CompressedFile::identify_compression(reader)?;
        reader.seek(SeekFrom::Start(0))?;

        let mut reader = DecompressStream::new(file_type, reader);

        let detection = Self::identify_tar(&mut reader)?;

        Ok(detection)
    }

    fn identify_tar<S>(reader: &mut S) -> ArchiveResult<bool>
    where
        S: Read,
    {
        // Seek is not possible due to possibly being wrapped in Gzip (And I have no seekable gzip impl to rely on)
        // So read forward this many, so that it's "Seeked" to the position of the magic
        let mut buf = [0u8; 257];

        match reader.read_exact(&mut buf) {
            Ok(_) => {}
            Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => {}
            Err(e) => return Err(e)?,
        };
        let mut buf = tar::constants::MAGIC_TAR_USTAR;

        match reader.read_exact(&mut buf) {
            Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => return Ok(false),
            Err(e) => return Err(e)?,
            Ok(()) => {}
        };

        Ok(buf == tar::constants::MAGIC_TAR_USTAR)
    }

    pub fn extract_entry<'a, R>(
        &'a self,
        reader: &'a mut R,
        entry: &'a ArchiveEntry,
    ) -> ArchiveResult<Vec<u8>>
    where
        R: ArchiveFileReadTraits,
    {
        let file_type = self.file_type;
        let compressed_size = entry.get_compressed_size();
        let compressed_offset = entry.get_compressed_offset();

        let reader = BufReader::new(reader);
        let mut reader = TarFileReader::new(file_type, reader);
        reader
            .get_block_reader_mut()
            .seek_to_offset(compressed_offset as usize)?;

        let mut v = vec![0u8; compressed_size];
        reader.get_block_reader_mut().read_bytes(&mut v)?;
        Ok(v)
    }

    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        reader: R,
        entry: &ArchiveEntry,
    ) -> ArchiveResult<ArchiveEntryStream<R>> {
        let file_type = self.file_type;
        let compressed_size = entry.get_compressed_size();
        let compressed_offset = entry.get_compressed_offset();

        let mut reader = TarFileReader::new(file_type, reader);
        reader
            .get_block_reader_mut()
            .seek_to_offset(compressed_offset as usize)?;
        let reader = reader.into_inner();
        let reader = reader.into_inner();

        // There has _GOT to be a better way to do this...
        let reader: WindowedReader<DecompressStream<R>> =
            WindowedReader::new(reader, compressed_size as u64);
        let reader = ArchiveEntryStream::Tar(reader);
        Ok(reader)
    }
}

#[cfg(test)]
mod test {
    use super::TarFile;
    use crate::{
        blocking::{tests::ReadOnlyByteFsProvider, utils::fs::FsFileProvider},
        ArchiveType, CompressionCodec,
    };

    #[test]
    pub fn test_detect_targz() {
        let file_path = "test_files/tar/test.tar.gz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();

        assert!(TarFile::detect_binary(&mut reader).unwrap());
    }

    #[test]
    pub fn test_detect_tarxz() {
        let file_path = "test_files/tar/test.tar.xz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();
        assert!(TarFile::detect_binary(&mut reader).unwrap());
    }

    #[test]
    pub fn test_detect_tgz() {
        let file_path = "test_files/tar/test.tgz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();
        assert!(TarFile::detect_binary(&mut reader).unwrap());
    }

    #[test]
    pub fn test_detect_targz_no_ext() {
        let file_path = "test_files/tar/testtargz_no_ext";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();
        assert!(TarFile::detect_binary(&mut reader).unwrap());
    }

    #[test]
    pub fn test_tar_file_load() {
        let file_path = "test_files/tar/test.tar.gz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let mut reader = fs.open_file(file_path).unwrap();
        let (archive_file_type, entries) = TarFile::load(&mut reader).unwrap();
        assert_eq!(archive_file_type, ArchiveType::Tar(CompressionCodec::GZIP));
        assert_eq!(entries.len(), 2);
        let entry = &entries[0];
        assert_eq!(entry.get_name(), "test/");
        let entry = &entries[1];
        assert_eq!(entry.get_name(), "test/test.txt");
    }
}
