use super::xtheader::AsyncXtHeader;
use crate::{
    blocking::{
        streams::DecompressStream,
        utils::{fs::ArchiveFileReadTraits, BlockReader},
    },
    result::{ArchiveError, ArchiveResult},
    tar::{
        self,
        block::Block,
        headers::{xheader::XtHeader, BlockResult},
        type_flags::TypeFlags,
    },
    ArchiveEntry, CompressionCodec,
};
use std::io::BufRead;

pub struct TarFileReader<R>
where
    R: ArchiveFileReadTraits,
{
    block_reader: BlockReader<DecompressStream<R>>,
}

impl<R> TarFileReader<R>
where
    R: ArchiveFileReadTraits,
{
    pub fn into_inner(self) -> BlockReader<DecompressStream<R>> {
        self.block_reader
    }
    pub fn new(file_type: CompressionCodec, reader: R) -> Self {
        let inner = DecompressStream::new(file_type, reader);
        let block_reader = BlockReader::new(tar::constants::BLOCK_SIZE, inner);
        Self { block_reader }
    }

    pub fn get_block_reader_mut(&mut self) -> &mut BlockReader<DecompressStream<R>> {
        &mut self.block_reader
    }

    pub fn read_entries(&mut self) -> ArchiveResult<Vec<ArchiveEntry>>
    where
        R: ArchiveFileReadTraits + BufRead,
    {
        let mut entries = vec![];

        let mut has_zero_entry = false;

        loop {
            match self.read_next_entry(entries.len())? {
                Some(e) => entries.push(e),
                None => {
                    if has_zero_entry {
                        break;
                    }
                    has_zero_entry = true
                }
            };
        }

        Ok(entries)
    }

    fn read_next_entry(&mut self, index: usize) -> ArchiveResult<Option<ArchiveEntry>> {
        // To do this, read blocks as headers
        // Accumulating information along the way until the header terminates
        // An extended block/header can exist before a non extended header - the data follows that final non extended header.

        let reader = self.get_block_reader_mut();

        let mut extended_header_tabs: Vec<(XtHeader, String)> = vec![];
        loop {
            let mut block = Block::default();

            match reader.read_block(block.bytes_mut()) {
                Ok(_) => {}
                Err(ArchiveError::Io(e)) if e.kind() == std::io::ErrorKind::UnexpectedEof => break,
                Err(err) => return Err(err),
            };

            let header = match block.decode_block() {
                Ok(BlockResult::End) => return Ok(None),
                Ok(BlockResult::Header(h)) => h,
                Err(e) => {
                    return Err(e);
                }
            };
            let data_offset = reader.current_offset();
            let typeflag = TypeFlags::parse(header.get_typeflag())?;

            let data_len: usize = match typeflag {
                TypeFlags::LinkType | TypeFlags::SymType => 0,
                _ => tar::parse::parse_digits(header.get_size(), false, 0..=i64::MAX)? as usize,
            };

            match typeflag {
                TypeFlags::XhdType => {
                    let xhv = AsyncXtHeader::parse(data_len, reader)?;
                    if !xhv.is_empty() {
                        for x in xhv {
                            extended_header_tabs.push(x);
                        }
                    }
                    continue;
                }
                TypeFlags::GnutypeLongname | TypeFlags::GnutypeLonglink => {
                    let s = reader.read_cstring(data_len)?;
                    extended_header_tabs.push((XtHeader::Path, s));
                    //I probably have this wrong for long link - this is probably the target of the link vs the link name itself which is what I have here...
                    continue;
                }
                // Anything else breaks through and is then processed as an entry
                _ => {}
            };
            if data_len > 0 {
                reader.skip_bytes(data_len)?;
            }

            let x =
                header.create_archive_entry(index, data_offset, data_len, &extended_header_tabs)?;

            return Ok(Some(x));
        }

        //I think this might actually be incorrect error status?
        Err(ArchiveError::InvalidData(
            "Reached end of file without a proper termination block".to_string(),
        ))
    }
}
