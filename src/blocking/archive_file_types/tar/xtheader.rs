use crate::{blocking::utils::BlockReader, result::ArchiveResult, tar::headers::xheader::XtHeader};
use std::io::Read;

pub struct AsyncXtHeader;

impl AsyncXtHeader {
    pub fn parse<R: Read>(
        data_len: usize,
        reader: &mut BlockReader<R>,
    ) -> ArchiveResult<Vec<(XtHeader, String)>> {
        //let xh_str = reader.read_cstring(data_len + reader.block_size()).await?;
        let xh_str = reader.read_to_zero(data_len)?;
        let xt_header = XtHeader::read(&xh_str)?;
        Ok(xt_header)
    }
}
