pub mod streams;
pub mod utils;

// mod archive_file;
mod archive_file_types;
pub use archive_file_types::SyncArchiveReader;

#[cfg(test)]
mod tests;
