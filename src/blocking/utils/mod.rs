pub mod fs;

mod block_reader;
mod windowed_reader;

pub use block_reader::BlockReader;
pub use windowed_reader::WindowedReader;

use crate::result::ArchiveResult;
use fs::ArchiveFileReadSeekTraits;

/// Used to parse a struct using Binr
pub trait BinrParser<R, T>
where
    R: ArchiveFileReadSeekTraits,
{
    /// The implementor will utilize binr and return self
    fn parse(parser: &mut R) -> ArchiveResult<T>;
}
