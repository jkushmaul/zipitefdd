//! Feels stupid to have to make this but I have found no other such object
//! In simplest terms:  "Read normally but return end of file after this many bytes read"
//! A slice of sorts - maybe that is a better name

use std::io::Read;

/// A struct that can be used to reduce a streams length
pub struct WindowedReader<R: Read> {
    inner: R,
    cursor: u64,
    length: u64,
}

impl<R: Read> WindowedReader<R> {
    /// Create a  new shadowed reader from existing, starting from current position with a max length
    pub fn new(inner: R, length: u64) -> Self {
        Self {
            inner,
            length,
            cursor: 0,
        }
    }
}
/*
impl Read for ShadowedReader<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> std::io::Result<usize> {
        if self.cursor >= self.length {
            return Err(std::io::Error::new(
                ErrorKind::UnexpectedEof,
                "failed to fill whole buffer",
            ));
        }

        let remaining = self.length - self.cursor;
        if buf.len() > remaining as usize {
            buf = &mut buf[0..remaining];
        }

        let s = self.inner.read(buf)?;
        self.cursor += s;
        return Ok(s);
    }
}
*/

impl<R: Read> Read for WindowedReader<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> std::io::Result<usize> {
        if self.cursor >= self.length {
            return Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "failed to fill whole buffer",
            ));
        }

        let remaining = (self.length - self.cursor) as usize;
        if buf.len() > remaining {
            buf = &mut buf[0..remaining];
        }
        self.inner.read(buf)
    }
}
