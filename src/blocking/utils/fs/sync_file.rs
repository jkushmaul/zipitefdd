use super::FsFileProvider;
use crate::result::{ArchiveError, ArchiveResult};
use effitfs::{FileType, Metadata, PathBuf};
use filetime_creation::FileTime;
use std::{fs::File, time::SystemTime};

/// A convenience implementation, by no means does it need to be used
#[derive(Default, Clone)]
pub struct SyncFsFileProvider {}

impl SyncFsFileProvider {
    fn convert_metadata(
        &self,
        path: &str,
        metadata: std::fs::Metadata,
    ) -> ArchiveResult<effitfs::Metadata> {
        let mut metadata: effitfs::Metadata = Metadata::from(metadata);

        let link_target = match metadata.file_type() {
            FileType::SymLink => {
                let path = std::fs::canonicalize(path)?;
                let path = PathBuf::from(path);
                Some(path.to_string())
            }
            _ => None,
        };

        metadata.set_link_target(link_target);

        Ok(metadata)
    }
}
/// A convenience type for using the async-fs provider
impl FsFileProvider for SyncFsFileProvider {
    type R = File;
    type W<'a> = File;

    fn get_symlink_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        let metadata: std::fs::Metadata = std::fs::symlink_metadata(&path)?;

        let m = self.convert_metadata(&path, metadata)?;

        Ok(m)
    }

    fn get_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        let metadata: std::fs::Metadata = std::fs::metadata(&path)?;
        let m = self.convert_metadata(&path, metadata)?;

        Ok(m)
    }

    fn open_file<'a>(&'a self, path: &'a str) -> ArchiveResult<Self::R>
    where
        Self: 'a,
    {
        let f = std::fs::File::open(path)?;
        Ok(f)
    }

    fn create_file(&mut self, path: String) -> ArchiveResult<Self::W<'_>> {
        let f = std::fs::File::create(path)?;
        Ok(f)
    }

    #[cfg(target_os = "windows")]
    fn chmod(&mut self, _path: String, _unix_mode: u32) -> ArchiveResult<()> {
        Ok(())
    }
    #[cfg(not(target_os = "windows"))]
    fn chmod<'a>(&'a mut self, path: String, unix_mode: u32) -> ArchiveResult<()> {
        use std::os::unix::fs::PermissionsExt;

        let path = std::path::PathBuf::from(&path);
        let path = path.canonicalize()?;
        let meta = path.metadata()?;
        let mut permissions = meta.permissions();
        permissions.set_mode(unix_mode);
        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn lchmod(&mut self, _path: String, _unix_mode: u32) -> ArchiveResult<()> {
        Ok(())
    }
    #[cfg(not(target_os = "windows"))]
    fn lchmod<'a>(&'a mut self, path: String, unix_mode: u32) -> ArchiveResult<()> {
        use std::os::unix::fs::PermissionsExt;

        let path = std::path::PathBuf::from(&path);
        let meta = path.symlink_metadata()?;
        meta.permissions().set_mode(unix_mode);

        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn chown(&mut self, _path: String, _uid: Option<u32>, _gid: Option<u32>) -> ArchiveResult<()> {
        Ok(())
    }

    #[cfg(not(target_os = "windows"))]
    fn chown<'a>(
        &'a mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> ArchiveResult<()> {
        std::os::unix::fs::chown(path, uid, gid)?;
        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn lchown(&mut self, _path: String, _uid: Option<u32>, _gid: Option<u32>) -> ArchiveResult<()> {
        Ok(())
    }
    #[cfg(not(target_os = "windows"))]
    fn lchown<'a>(
        &'a mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> ArchiveResult<()> {
        // Boooo
        std::os::unix::fs::lchown(path, uid, gid)?;
        Ok(())
    }

    fn ltouch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        let now = SystemTime::now();

        match filetime_creation::set_symlink_file_times(
            &path,
            FileTime::from_system_time(atime.unwrap_or(now)),
            FileTime::from_system_time(mtime.unwrap_or(now)),
            FileTime::from_system_time(ctime.unwrap_or(now)),
        ) {
            Ok(_) => {}
            Err(e) => {
                return Err(ArchiveError::Other(format!(
                    "Error setting file time: {}",
                    e
                )))
            }
        };
        Ok(())
    }

    fn touch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        let now = SystemTime::now();

        match filetime_creation::set_file_times(
            &path,
            FileTime::from_system_time(atime.unwrap_or(now)),
            FileTime::from_system_time(mtime.unwrap_or(now)),
            FileTime::from_system_time(ctime.unwrap_or(now)),
        ) {
            Ok(_) => {}
            Err(e) => {
                return Err(ArchiveError::Other(format!(
                    "Error setting file time: {}",
                    e
                )))
            }
        };
        Ok(())
    }

    fn rm_all(&mut self, path: String) -> ArchiveResult<()> {
        let path = std::path::PathBuf::from(path);
        if path.exists() {
            if path.is_dir() {
                std::fs::remove_dir_all(path)?;
            } else {
                std::fs::remove_file(path)?;
            }
        }

        Ok(())
    }

    fn mkdir_all(&mut self, path: String) -> ArchiveResult<()> {
        let path = std::path::PathBuf::from(path);

        if !path.exists() {
            std::fs::create_dir_all(path)?;
        }
        Ok(())
    }

    #[cfg(not(target_os = "windows"))]
    fn create_symlink<'a>(
        &'a mut self,
        link_target: String,
        target_path: String,
    ) -> ArchiveResult<()> {
        std::os::unix::fs::symlink(link_target, target_path)?;
        Ok(())
    }

    #[cfg(target_os = "windows")]
    fn create_symlink(&mut self, _link_target: String, _target_path: String) -> ArchiveResult<()> {
        Ok(())
    }

    fn read_dir(&self, path: String) -> ArchiveResult<Vec<effitfs::DirEntry>> {
        let mut v: Vec<effitfs::DirEntry> = vec![];
        let mut rd = std::fs::read_dir(path)?;

        while let Some(Ok(d)) = rd.next() {
            let path = effitfs::PathBuf::from(d.path());
            let metadata = d.metadata()?;
            let metadata = Metadata::from(metadata);
            let d = effitfs::DirEntry::new(path, metadata);
            v.push(d);
        }
        Ok(v)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        blocking::utils::fs::{FsFileProvider, SyncFsFileProvider},
        test_utils,
    };

    #[test]
    pub fn test_syncfs_file_provider() {
        let base_file_path = test_utils::workspace_relative_path("test_files/tar/test.tar.gz");

        let fs_provider = SyncFsFileProvider::default();

        let test_bytes = fs_provider.open_file(&base_file_path.to_string()).unwrap();
        let m = test_bytes.metadata().unwrap();
        assert_ne!(m.len(), 0);
    }
}
