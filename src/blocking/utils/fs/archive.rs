use super::FsFileProvider;
use crate::{
    blocking::{streams::ArchiveEntryStream, SyncArchiveReader},
    result::{ArchiveError, ArchiveResult},
    ArchiveManifest,
};
use effitfs::{DirEntry, Metadata};
use std::{
    io::{Cursor, Seek},
    time::SystemTime,
};

/// Read-only archive-file backed fs provider
#[derive(Clone)]
pub struct ArchiveFileReadOnlyFsProvider<FS: FsFileProvider>
where
    FS::R: Seek,
{
    archive_file: ArchiveManifest,
    underlying_fs: FS,
}

impl<FS: FsFileProvider> FsFileProvider for ArchiveFileReadOnlyFsProvider<FS>
where
    FS::R: Seek,
{
    type R = ArchiveEntryStream<FS::R>;

    type W<'a>
        = Cursor<&'a mut [u8]>
    where
        Self: 'a;

    fn open_file<'a>(&'a self, path: &str) -> ArchiveResult<Self::R>
    where
        Self: 'a,
    {
        let entry = self.archive_file.get_entry_by_name(path)?;
        let reader = SyncArchiveReader::new(&self.archive_file, &self.underlying_fs);
        let stream = reader.open_entry(entry)?;
        Ok(stream)
    }

    fn create_file(&mut self, _: String) -> ArchiveResult<Self::W<'_>> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn lchmod(&mut self, _path: String, _mode: u32) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn lchown(&mut self, _: String, _: Option<u32>, _: Option<u32>) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn chmod(&mut self, _path: String, _mode: u32) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn chown(&mut self, _: String, _: Option<u32>, _: Option<u32>) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn ltouch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn touch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn rm_all(&mut self, _: String) -> ArchiveResult<()> {
        let var_name = "ArchiveFileReadOnlyFsProvider does not support writes";
        Err(ArchiveError::Unsupported(var_name.to_string()))
    }

    fn mkdir_all(&mut self, _: String) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn create_symlink(&mut self, _: String, _: String) -> ArchiveResult<()> {
        Err(ArchiveError::Unsupported(
            "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
        ))
    }

    fn get_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        let path = path.to_owned();

        let entry = self.archive_file.get_entry_by_name(&path)?;
        let metadata = entry.get_metadata();
        Ok(metadata.clone())
    }

    fn get_symlink_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        self.get_metadata(path)
    }

    fn read_dir(&self, target_path: String) -> ArchiveResult<Vec<effitfs::DirEntry>> {
        let mut entries = vec![];
        let target_path = effitfs::PathBuf::from(target_path);
        for e in self.archive_file.get_entries() {
            let entry_path = effitfs::PathBuf::from(e.get_name());
            let entry_dir = entry_path.directory();
            if entry_dir != target_path {
                continue;
            }
            let metadata = e.get_metadata().clone();
            let entry = DirEntry::new(entry_path, metadata);
            entries.push(entry);
        }
        Ok(entries)
    }
}

#[cfg(test)]
mod tests {
    use std::io::Read;

    use crate::blocking::{
        tests::ReadOnlyByteFsProvider,
        utils::fs::{archive::ArchiveFileReadOnlyFsProvider, FsFileProvider},
        SyncArchiveReader,
    };

    #[test]
    pub fn test_readonly_archivefile_provider() {
        let file_path = "test_files/tar/test.tar.gz";
        let fs_provider = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        let archive_file = SyncArchiveReader::read_manifest(file_path, &fs_provider).unwrap();
        let archive_fs = ArchiveFileReadOnlyFsProvider {
            archive_file,
            underlying_fs: fs_provider,
        };

        let mut reader = archive_fs.open_file("test/test.txt").unwrap();
        let mut buf = vec![];
        reader.read_to_end(&mut buf).unwrap();
        assert_ne!(&buf, &[]);
    }
}
