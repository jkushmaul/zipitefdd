mod archive;
mod effitfs_provider;
mod sync_file;

pub use archive::ArchiveFileReadOnlyFsProvider;
pub use effitfs_provider::EffitFsProvider;
pub use sync_file::SyncFsFileProvider;

use crate::result::ArchiveResult;
use effitfs::{DirEntry, Metadata};
use std::io::{Read, Seek, Write};
use std::time::SystemTime;

/// A trait for provider to indicate its read type
pub trait ArchiveFileReadTraits: Read {}
impl<T> ArchiveFileReadTraits for T where T: Read {}

/// A trait for detecting archive file types
pub trait ArchiveFileReadSeekTraits: ArchiveFileReadTraits + Seek {}
impl<T> ArchiveFileReadSeekTraits for T where T: ArchiveFileReadTraits + Seek {}

/// A trait for provider to indicate its write type
pub trait ArchiveFileWriteTraits: Write {}
impl<T> ArchiveFileWriteTraits for T where T: Write + Send + Unpin {}

/// A trait used to interface with file system for sync calls
pub trait FsFileProvider
where
    Self: Clone + Sync,
{
    /// The type when opening a file
    type R: ArchiveFileReadTraits;

    /// The type when writing a file
    type W<'a>: ArchiveFileWriteTraits
    where
        Self: 'a;

    /// Open a file for reading
    fn open_file<'a>(&'a self, path: &'a str) -> ArchiveResult<Self::R>
    where
        Self: 'a;

    /// get metadata about path, following symlinks
    fn get_metadata(&self, path: String) -> ArchiveResult<Metadata>;

    /// Get metadata without dereferencing symlinks
    fn get_symlink_metadata(&self, path: String) -> ArchiveResult<Metadata>;

    /// Create and open a file for writing
    fn create_file(&mut self, path: String) -> ArchiveResult<Self::W<'_>>;

    /// Change file mode without dereferencing symbolic link
    fn lchmod(&mut self, path: String, mode: u32) -> ArchiveResult<()>;

    /// Change file mode
    fn chmod(&mut self, path: String, mode: u32) -> ArchiveResult<()>;

    /// Change file owner without dereferencing symbolic link
    fn lchown(&mut self, path: String, uid: Option<u32>, gid: Option<u32>) -> ArchiveResult<()>;

    /// Change file owner
    fn chown(&mut self, path: String, uid: Option<u32>, gid: Option<u32>) -> ArchiveResult<()>;

    /// Update file times without dereferencing symbolic link
    fn ltouch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()>;

    /// Update file times
    fn touch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()>;

    /// rm -rf
    fn rm_all(&mut self, path: String) -> ArchiveResult<()>;

    /// mkdir -p
    fn mkdir_all(&mut self, path: String) -> ArchiveResult<()>;

    /// ln -s
    fn create_symlink(&mut self, link_target: String, target_path: String) -> ArchiveResult<()>;

    /// ls -l
    fn read_dir(&self, path: String) -> ArchiveResult<Vec<DirEntry>>;
}
