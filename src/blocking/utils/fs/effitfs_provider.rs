use crate::result::ArchiveResult;
use effitfs::{DirEntry, Metadata, PathBuf};
use std::{io::Cursor, time::SystemTime};

use super::FsFileProvider;

/// An in memory file system
#[derive(Default)]
pub struct EffitFsProvider {
    fs: effitfs::Fs,
}

impl EffitFsProvider {
    fn import_recurse(
        fs: &mut effitfs::Fs,
        src_base_path: &PathBuf,
        relative_path: PathBuf,
    ) -> std::io::Result<()> {
        let src_path = src_base_path.clone();

        // And relative_path might be
        let src_path = src_path.resolve(&relative_path);

        let mut dir = std::fs::read_dir(src_path.to_string())?;
        while let Some(Ok(e)) = dir.next() {
            //I need to set the time based fields and permissions yet.
            //let metadata = e.metadata()?;
            //let permissions = effitfs::Permissions::from(metadata.permissions());
            let file_type = e.file_type()?;
            let name = match e.file_name().to_str() {
                Some(s) => s.to_string(),
                None => {
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        format!(
                            "Could not resolve filename for path: {:?}",
                            e.path().to_string_lossy()
                        ),
                    ));
                }
            };
            let mut src_path = src_path.clone();
            src_path.push_str(&name);
            let mut relative_path = relative_path.clone();
            relative_path.push_str(&name);

            if file_type.is_dir() {
                fs.create_dir(&relative_path.to_string())?;
                //fs.set_permissions(&src_path.to_string(), permissions)?;
                Self::import_recurse(fs, src_base_path, relative_path)?;
            } else if file_type.is_file() {
                let data = std::fs::read(src_path.to_string())?;
                fs.write(&relative_path.to_string(), &data)?;
                //fs.set_permissions(&src_path.to_string(), permissions)?;
            } else if file_type.is_symlink() {
                let path = std::fs::read_link(src_path.to_string())?;
                let path = path.to_string_lossy().to_string();
                fs.symlink(&relative_path.to_string(), &path)?;
                //fs.set_permissions(&src_path.to_string(), permissions)?;
            } else {
                panic!("I don't know what to do");
            }
        }
        Ok(())
    }

    /// Recurse through a directory and import it to CWD
    pub fn from_directory(src_path: &str) -> std::io::Result<Self> {
        let mut fs = effitfs::Fs::default();
        let mut src_base_path: effitfs::PathBuf = PathBuf::default();
        src_base_path.push_str(src_path);

        let relative_path = PathBuf::default();
        Self::import_recurse(&mut fs, &src_base_path, relative_path)?;
        Ok(Self { fs })
    }

    /// iterate contained files for given dir
    pub fn get_files(&self, dir: &str) -> ArchiveResult<Vec<effitfs::DirEntry>> {
        let v = self.fs.read_dir(dir)?;
        Ok(v)
    }
}

impl Clone for EffitFsProvider {
    fn clone(&self) -> Self {
        Self {
            fs: self.fs.clone(),
        }
    }
}

impl FsFileProvider for EffitFsProvider {
    type R = Cursor<Vec<u8>>;
    type W<'a> = Cursor<&'a mut Vec<u8>>;

    fn open_file<'a>(&'a self, path: &'a str) -> ArchiveResult<Self::R>
    where
        Self: 'a,
    {
        let f = self.fs.read(path)?;
        let f = Cursor::new(f);

        Ok(f)
    }

    fn create_file(&mut self, path: String) -> ArchiveResult<Self::W<'_>> {
        self.fs.write(&path, [])?;
        let f = self.fs.get_file_mut(&path)?;
        let data = f.get_data_mut();
        let writer = Cursor::new(data);
        Ok(writer)
    }

    fn lchmod(&mut self, path: String, mode: u32) -> ArchiveResult<()> {
        self.fs.lchmod_mut(&path, mode)?;
        Ok(())
    }

    fn lchown(&mut self, path: String, uid: Option<u32>, gid: Option<u32>) -> ArchiveResult<()> {
        self.fs.lchown_mut(&path, uid, gid)?;
        Ok(())
    }

    fn chmod(&mut self, path: String, mode: u32) -> ArchiveResult<()> {
        self.fs.chmod_mut(&path, mode)?;
        Ok(())
    }

    fn chown(&mut self, path: String, uid: Option<u32>, gid: Option<u32>) -> ArchiveResult<()> {
        self.fs.chown_mut(&path, uid, gid)?;
        Ok(())
    }
    fn ltouch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        self.fs.ltouch_mut(&path, atime, mtime, ctime)?;
        Ok(())
    }

    fn touch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> ArchiveResult<()> {
        self.fs.touch_mut(&path, atime, mtime, ctime)?;
        Ok(())
    }

    fn rm_all(&mut self, path: String) -> ArchiveResult<()> {
        self.fs.remove_dir_all(&path)?;
        Ok(())
    }

    fn mkdir_all(&mut self, path: String) -> ArchiveResult<()> {
        self.fs.create_dir_all(&path)?;
        Ok(())
    }

    fn create_symlink(&mut self, link_target: String, target_path: String) -> ArchiveResult<()> {
        self.fs.symlink(&target_path, &link_target)?;
        Ok(())
    }

    fn get_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        let m = self.fs.metadata(&path)?;
        Ok(m)
    }

    fn get_symlink_metadata(&self, path: String) -> ArchiveResult<Metadata> {
        let m = self.fs.symlink_metadata(&path)?;
        Ok(m)
    }

    fn read_dir(&self, path: String) -> ArchiveResult<Vec<DirEntry>> {
        let entries = self.fs.read_dir(&path)?;
        Ok(entries)
    }
}
