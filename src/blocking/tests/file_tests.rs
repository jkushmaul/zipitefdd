use crate::blocking::archive_file_types::SyncArchiveReader;
use crate::blocking::utils::fs::{EffitFsProvider, FsFileProvider};
use crate::{
    result::{ArchiveError, ArchiveResult},
    ExtractOptions,
};
use crate::{test_utils, ArchiveManifest};
use effitfs::{FileType, PathBuf};
use sha2::{Digest, Sha256};
use std::{collections::HashMap, io::Cursor, time::SystemTime};

use super::bytesfs_provider::ReadOnlyByteFsProvider;

// Blame rust for not having data driven tests and esoteric crates that try and make up for it
#[test]
fn test_compressed_compression_is_bzip2_bz2() {
    test_file_wrapper("type=compressed compression=bzip2.bz2");
}
#[test]
fn test_compressed_compression_is_gzip_gz() {
    test_file_wrapper("type=compressed compression=gzip.gz");
}
#[test]
fn test_compressed_compression_is_xz_xz() {
    test_file_wrapper("type=compressed compression=xz.xz");
}
#[test]
fn test_tar_compression_bzip2_type_tar_version_is_ustar_tar_bz2() {
    test_file_wrapper("type=tar compression=bzip2 type=tar version=ustar.tar.bz2");
}
#[test]
fn test_tar_compression_bzip2_version_is_default_tar_bz2() {
    test_file_wrapper("type=tar compression=bzip2 version=default.tar.bz2");
}
#[test]
fn test_tar_compression_bzip2_version_is_gnu_tar_bz2() {
    test_file_wrapper("type=tar compression=bzip2 version=gnu.tar.bz2");
}
#[test]
fn test_tar_compression_bzip2_version_is_oldgnu_tar_bz2() {
    test_file_wrapper("type=tar compression=bzip2 version=oldgnu.tar.bz2");
}
#[test]
fn test_tar_compression_bzip2_version_is_posix_tar_bz2() {
    test_file_wrapper("type=tar compression=bzip2 version=posix.tar.bz2");
}
#[test]
fn test_tar_compression_gzip_type_tar_version_is_ustar_tar_gz() {
    test_file_wrapper("type=tar compression=gzip type=tar version=ustar.tar.gz");
}
#[test]
fn test_tar_compression_gzip_version_is_default_tar_gz() {
    test_file_wrapper("type=tar compression=gzip version=default.tar.gz");
}
#[test]
fn test_tar_compression_gzip_version_is_gnu_tar_gz() {
    test_file_wrapper("type=tar compression=gzip version=gnu.tar.gz");
}
#[test]
fn test_tar_compression_gzip_version_is_oldgnu_tar_gz() {
    test_file_wrapper("type=tar compression=gzip version=oldgnu.tar.gz");
}
#[test]
fn test_tar_compression_gzip_version_is_posix_tar_gz() {
    test_file_wrapper("type=tar compression=gzip version=posix.tar.gz");
}
#[test]
fn test_tar_compression_none_type_tar_version_is_ustar_tar() {
    test_file_wrapper("type=tar compression=none type=tar version=ustar.tar");
}
#[test]
fn test_tar_compression_none_version_is_default_tar() {
    test_file_wrapper("type=tar compression=none version=default.tar");
}
#[test]
fn test_tar_compression_none_version_is_gnu_tar() {
    test_file_wrapper("type=tar compression=none version=gnu.tar");
}
#[test]
fn test_tar_compression_none_version_is_oldgnu_tar() {
    test_file_wrapper("type=tar compression=none version=oldgnu.tar");
}
#[test]
fn test_tar_compression_none_version_is_posix_tar() {
    test_file_wrapper("type=tar compression=none version=posix.tar");
}
#[test]
fn test_tar_compression_xz_type_tar_version_is_ustar_tar_xz() {
    test_file_wrapper("type=tar compression=xz type=tar version=ustar.tar.xz");
}
#[test]
fn test_tar_compression_xz_version_is_default_tar_xz() {
    test_file_wrapper("type=tar compression=xz version=default.tar.xz");
}
#[test]
fn test_tar_compression_xz_version_is_gnu_tar_xz() {
    test_file_wrapper("type=tar compression=xz version=gnu.tar.xz");
}
#[test]
fn test_tar_compression_xz_version_is_oldgnu_tar_xz() {
    test_file_wrapper("type=tar compression=xz version=oldgnu.tar.xz");
}
#[test]
fn test_tar_compression_xz_version_is_posix_tar_xz() {
    test_file_wrapper("type=tar compression=xz version=posix.tar.xz");
}
#[test]
fn test_zip_compression_is_bzip2_zip() {
    test_file_wrapper("type=zip compression=bzip2.zip");
}
#[test]
fn test_zip_compression_is_deflate_zip() {
    test_file_wrapper("type=zip compression=deflate.zip");
}
#[test]
fn test_zip_compression_is_store_zip() {
    test_file_wrapper("type=zip compression=store.zip");
}

pub fn test_file_wrapper(p: &str) {
    let p = test_utils::workspace_relative_path(&format!("test_files/datatest_files/{}", p));
    test_file(p).unwrap();
}

// This actually modifies the file system, not a fan.
pub fn test_file(archive_path: PathBuf) -> ArchiveResult<()> {
    let mut test_file = FileTest::new(archive_path)?;

    test_file.run_test()?;

    Ok(())
}

pub struct FileTest {
    archive_path: PathBuf,
    effit_fs_provider: EffitFsProvider,
    extraction_dir: PathBuf,
    extract_options: ExtractOptions,
}

impl FileTest {
    pub fn new(archive_path: PathBuf) -> ArchiveResult<Self> {
        let mut extraction_dir = PathBuf::default();
        extraction_dir.push_str("target");

        let mut effit_fs_provider = EffitFsProvider::default();
        effit_fs_provider.mkdir_all(extraction_dir.to_string())?;

        let extract_options = ExtractOptions {
            target_directory: Some(extraction_dir.to_string()),
            strip_prefix: None,
            use_permissions: true,
            use_ownership: false,
            use_timestamps: true,
        };
        Ok(Self {
            archive_path,
            effit_fs_provider,
            extract_options,
            extraction_dir,
        })
    }

    pub fn run_test(&mut self) -> ArchiveResult<()> {
        let archive_path_str = self.archive_path.to_string();
        let data = test_utils::read_file(&archive_path_str)?;

        let test_file_provider = ReadOnlyByteFsProvider::new(self.archive_path.clone(), data);
        let archive_file: ArchiveManifest =
            SyncArchiveReader::read_manifest(&archive_path_str, &test_file_provider)?;

        let archive_reader = SyncArchiveReader::new(&archive_file, &test_file_provider);

        //let archive_file = SyncArchiveReader::load(&archive_path_str, &test_file_provider)
        //    .unwrap_or_else(|e| panic!("Could not open archive file {archive_path_str}: {e}"));
        archive_reader
            .unpack_all(&mut self.effit_fs_provider, &self.extract_options)
            .unwrap_or_else(|e| {
                panic!(
                    "Could not unpack {} to {}: {e}",
                    self.archive_path, self.extraction_dir
                )
            });
        self.validate()?;
        Ok(())
    }
    fn validate(&self) -> ArchiveResult<()> {
        if self.archive_path.filename().contains("type=compressed") {
            self.verify_single_unpack()?;
        } else {
            self.verify_collection_unpack()?;
        }
        Ok(())
    }

    pub fn get_manifest(&self) -> ArchiveResult<HashMap<String, Manifest>> {
        let mut path = self.extraction_dir.clone();
        path.push_str("MANIFEST.txt");

        let manifest_str = self
            .effit_fs_provider
            .open_file(&path.to_string())?
            .into_inner();
        let manifest_str = String::from_utf8_lossy(&manifest_str).to_string();

        let manifests: HashMap<String, Manifest> = manifest_str
            .lines()
            .map(|s| Manifest::parse(s).unwrap())
            .map(|m| (m.name.to_owned(), m))
            .collect();
        Ok(manifests)
    }

    pub fn get_checksums(&self) -> ArchiveResult<HashMap<String, Checksum>> {
        let mut path = self.extraction_dir.clone();
        path.push_str("CHECKSUMS.txt");

        let checksum_str = self
            .effit_fs_provider
            .open_file(&path.to_string())?
            .into_inner();
        let checksum_str = String::from_utf8_lossy(&checksum_str).to_string();
        let checksums: HashMap<String, Checksum> = checksum_str
            .lines()
            .map(|s| Checksum::parse(s).unwrap())
            .map(|c| (c.name.to_owned(), c))
            .collect();

        Ok(checksums)
    }

    pub fn verify_single_unpack(&self) -> ArchiveResult<()> {
        let mut expected_path = self.extraction_dir.clone();
        let filename = self.archive_path.filename();
        expected_path.push_str(filename);
        let expected_path = expected_path.to_string();

        // Single files compressed will have the name of the directory
        let _ = self
            .effit_fs_provider
            .open_file(&expected_path)
            .unwrap_or_else(|_| panic!("Could not open file '{}'", expected_path));
        //Should probably validate the hash in some way...

        Ok(())
    }

    pub fn verify_collection_unpack(&self) -> ArchiveResult<()> {
        let manifests = self.get_manifest()?;
        let checksums = self.get_checksums()?;

        //now assert that each file in CHECKSUMs has same sha256 checksum
        //and that each listing in MANIFESTS matches attributes

        for (k, v) in &manifests {
            let mut path = self.extraction_dir.clone();
            path.push_str(k);
            self.verify_file_manifest(path, v)
                .unwrap_or_else(|_| panic!("Could not verify file manifest: {}", k));
        }

        for (k, v) in checksums {
            let mut path = self.extraction_dir.clone();
            path.push_str(&k);
            self.verify_file_checksum(&path, &v)
                .unwrap_or_else(|_| panic!("Could not verify file checksum: {}", k));
        }

        Ok(())
    }

    pub fn verify_file_checksum(&self, path: &PathBuf, v: &Checksum) -> ArchiveResult<()> {
        let path = path.to_string();
        let m = self.effit_fs_provider.get_metadata(path.clone())?;
        if let FileType::SymLink = m.file_type() {
            return Ok(());
        }

        let reader = self.effit_fs_provider.open_file(&path)?;
        let buf: Vec<u8> = reader.into_inner();
        let mut reader = Cursor::new(buf);

        let mut hasher = Sha256::new();
        std::io::copy(&mut reader, &mut hasher).unwrap();
        let hash_bytes = hasher.finalize().to_vec();

        if hash_bytes.as_slice() != v.sha256.as_slice() {
            return Err(ArchiveError::InvalidData("Hashes not equal".to_string()))?;
        }
        Ok(())
    }

    fn verify_file_manifest(&self, path: PathBuf, v: &Manifest) -> ArchiveResult<()> {
        if cfg!(target_os = "windows") && v.file_type == FileType::SymLink {
            return Ok(());
        }
        let path = path.to_string();
        let meta = self.effit_fs_provider.get_symlink_metadata(path.clone())?;
        let mtime_st = meta.modified().unwrap();
        let mtime = mtime_st
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs_f64()
            .floor() as u64;
        //SystemTime {dwLowDateTime=547327232 dwHighDateTime=31040158 } turns into 1675663602
        // +1 second from manifest, 1675663601 which is the time in the zip.

        if self.archive_path.filename().contains("type=zip") {
            if mtime < v.mtime - 1 || mtime > v.mtime + 1 {
                assert_eq!(
                    mtime, v.mtime,
                    "mtime for zip did not match manifest by +/- 1 for {}",
                    path,
                );
            }
        } else {
            assert_eq!(mtime, v.mtime, "mtime did not match manifest for {}", path,);
        };

        if cfg!(not(target_os = "windows")) {
            let f_mode = meta.permissions().get_mode();
            assert_eq!(
                f_mode, v.mode,
                "mode ({:o}) did not match manifest {:o} for {}",
                f_mode, v.mode, path,
            );
            /*
            // This is not easy to test - some refactor to allow file system mocking would be in order

            let uid = get_uid(&path)?;
            assert_eq2(
                uid,
                v.uid,
                format!(
                    "uid ({}) did not match manifest {} for {}",
                    uid, v.uid, pathstr
                ),
            );

            let gid = get_gid(&path)?;
            assert_eq2(
                gid,
                v.gid,
                format!(
                    "gid ({}) did not match manifest {} for {}",
                    gid, v.gid, pathstr
                ),
            );
            */
        }

        match v.file_type {
            FileType::File => {
                assert!(matches!(meta.file_type(), FileType::File));
                let data = self.effit_fs_provider.open_file(&path)?;
                assert_eq!(
                    data.into_inner().len(),
                    v.size as usize,
                    "File length did not match manifest for {}",
                    path,
                );
                assert_eq!(
                    meta.len(),
                    v.size as u64,
                    "File length did not match manifest for {}",
                    path,
                );
                return Ok(());
            }
            FileType::Dir => {
                assert!(matches!(meta.file_type(), FileType::Dir));
                self.effit_fs_provider.get_files(&path)?;
                return Ok(());
            }
            FileType::SymLink => {
                assert!(matches!(meta.file_type(), FileType::SymLink));
                assert!(meta.link_target().is_some());
            }
            FileType::Other(_) => panic!("Unknown file type..."),
        };

        //  target: Option<String>,
        //  uname: String,
        //  gname: String,
        //  uid: u32,
        //  gid: u32,
        //  mode: u32,

        // These are only carried in certain formats, need to filter based on name attributes

        //so lame.

        if cfg!(target_os = "windows") || v.file_type != FileType::SymLink {
            return Ok(());
        }

        let meta_target = meta.link_target().map(|s| s.as_str()).unwrap_or_default();

        // Manifest will be relative to extract dir
        let manifest_target = v.target.as_deref().unwrap_or_default();

        assert_eq!(manifest_target, meta_target);

        Ok(())
    }
}
#[derive(thiserror::Error, Debug)]
pub enum TestFailure {
    #[error("There was an error with the archive file: {0}")]
    ArchiveError(#[from] ArchiveError),
    #[error("Test failed: {0}")]
    AssertionFailure(String),
    #[error("IO Error")]
    Io(#[from] std::io::Error),
}

pub struct Checksum {
    name: String,
    sha256: [u8; 32],
}

impl Checksum {
    fn parse(line: &str) -> Result<Self, TestFailure> {
        let fields: Vec<_> = line.split_ascii_whitespace().collect();

        if fields.len() != 2 {
            return Err(TestFailure::AssertionFailure(format!(
                "fields.len(): {} should have equaled 2; from line : {}",
                fields.len(),
                line
            )))?;
        }

        let sha256 = fields[0];
        if sha256.len() != 64 {
            return Err(TestFailure::AssertionFailure(format!(
                "sha256.len(): {} should have equaled 64",
                sha256.len()
            )))?;
        }

        let v: Vec<u8> = sha256
            .as_bytes()
            .chunks(2)
            .map(|x| {
                let s = String::from_utf8_lossy(x).to_string();

                u8::from_str_radix(&s, 16).unwrap()
            })
            .collect();
        let mut sha256 = [0u8; 32];
        sha256.copy_from_slice(&v);

        Ok(Self {
            name: fields[1].to_owned(),
            sha256,
        })
    }
}

// mode=%a\tgid=%g\tgname=%G\tuid=%u\tuname=%U\tatime=%X\tctime=%W\tmtime=%Y\tsize=%B\tname=%N\n
pub struct Manifest {
    name: String,
    target: Option<String>,
    _uname: String,
    _gname: String,

    file_type: FileType,

    size: u32,
    _uid: u32,
    _gid: u32,
    mode: u32,
    mtime: u64,
    _ctime: u64,
    _atime: u64,
}

impl Manifest {
    fn parse(line: &str) -> ArchiveResult<Self> {
        let fields: HashMap<String, String> = line
            .split("\t")
            .map(|s| {
                let fields = s.split_once("=").unwrap();
                (fields.0.to_owned(), fields.1.to_owned())
            })
            .collect();

        let parse_time = |field_name: &str| -> u64 {
            let t = fields.get(field_name).unwrap();

            t.parse::<u64>().unwrap()
        };
        let parse_radix = |field_name: &str, radix: u32| -> u32 {
            let v = fields.get(field_name).unwrap();
            match u32::from_str_radix(v, radix) {
                Ok(s) => s,
                Err(e) => panic!(
                    "Failed to parse_radix('{}', {}) with value '{}': {}",
                    v, radix, field_name, e
                ),
            }
        };

        let file_type = fields.get("type").unwrap();
        let file_type = if file_type == "directory" {
            FileType::Dir
        } else if file_type == "symbolic link" {
            FileType::SymLink
        } else if file_type == "regular file" {
            FileType::File
        } else {
            panic!("Unknown file type: {:}", file_type);
        };

        let link_match = regex::Regex::new("'([^']+)'( -> '([^']+)')?").unwrap();
        let name = fields.get("name").unwrap().to_owned();
        let captures = link_match.captures(&name).unwrap();
        let name = captures.get(1).unwrap().as_str().to_owned();
        let target = captures.get(3).map(|s| s.as_str().to_owned());
        let mode = parse_radix("mode", 8);

        Ok(Self {
            name,
            target,
            file_type,
            size: parse_radix("size", 10),
            _uname: fields.get("uname").unwrap().to_owned(),
            _gname: fields.get("gname").unwrap().to_owned(),
            mode,
            _uid: parse_radix("uid", 10),
            _gid: parse_radix("gid", 10),
            mtime: parse_time("mtime"),
            _atime: parse_time("atime"),
            _ctime: parse_time("ctime"),
        })
    }
}
