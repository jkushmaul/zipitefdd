//! Just an enum to represent the various types of entry streams

use crate::blocking::utils::WindowedReader;

use super::DecompressStream;
use std::io::Read;

/// A type for all possible entry outputs
pub enum ArchiveEntryStream<R: Read> {
    Uncompressed(R),
    /// No entries, just a compressed (or uncomrpessed) file
    CompressedFile(DecompressStream<R>),
    /// A tar file, contained within a compressed file
    Tar(WindowedReader<DecompressStream<R>>),
    /// A zip file, with compressed entries
    Zip(WindowedReader<DecompressStream<R>>),
}

impl<R: Read> Read for ArchiveEntryStream<R> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match self {
            ArchiveEntryStream::CompressedFile(f) => f.read(buf),
            ArchiveEntryStream::Tar(f) => f.read(buf),
            ArchiveEntryStream::Zip(f) => f.read(buf),
            ArchiveEntryStream::Uncompressed(f) => f.read(buf),
        }
    }
}
