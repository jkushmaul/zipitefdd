use flate2::read::{DeflateDecoder, GzDecoder};
use std::io::Read;
use xz2::read::XzDecoder;

use crate::{blocking::utils::fs::ArchiveFileReadTraits, CompressionCodec};

/// all types of supported decompression
#[allow(clippy::large_enum_variant)]
pub enum DecompressStream<R: ArchiveFileReadTraits> {
    /// Gz
    Gz(GzDecoder<R>),
    /// Xz
    Xz(XzDecoder<R>),
    /// Bz2
    Bz2(bzip2_rs::DecoderReader<R>),
    /// Deflate
    Deflate(DeflateDecoder<R>),
    /// Uncompressed
    Uncompressed(R),
}

impl<R: Read> DecompressStream<R> {
    /// Construct a new DecompressStream
    pub fn new(file_type: CompressionCodec, reader: R) -> Self {
        match file_type {
            CompressionCodec::STORE => Self::Uncompressed(reader),
            CompressionCodec::GZIP => Self::Gz(GzDecoder::new(reader)),
            CompressionCodec::XZ => Self::Xz(XzDecoder::new(reader)),
            CompressionCodec::BZIP2 => Self::Bz2(bzip2_rs::DecoderReader::new(reader)),
            CompressionCodec::DEFLATE => Self::Deflate(DeflateDecoder::new(reader)),
        }
    }
}

impl<R: Read> Read for DecompressStream<R> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match self {
            DecompressStream::Gz(r) => r.read(buf),
            DecompressStream::Xz(r) => r.read(buf),
            DecompressStream::Bz2(r) => r.read(buf),

            DecompressStream::Deflate(r) => r.read(buf),
            DecompressStream::Uncompressed(r) => r.read(buf),
        }
    }
}
