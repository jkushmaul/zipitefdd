pub mod reader;
pub mod xtheader;

use crate::non_blocking::streams::{ArchiveEntryStream, DecompressStream};
use crate::non_blocking::utils::fs::ArchiveFileReadTraits;
use crate::non_blocking::{
    archive_file_types::compressed::CompressedFile,
    utils::{fs::ArchiveFileReadSeekTraits, windowed_reader::WindowedReader},
};
use crate::result::ArchiveResult;
use crate::{tar, ArchiveEntry, ArchiveType, CompressionCodec};
use futures_util::{
    future::BoxFuture, io::BufReader, AsyncBufRead, AsyncReadExt, AsyncSeekExt, FutureExt,
};
use reader::TarFileReader;
use std::io::SeekFrom;

pub struct TarFile {
    file_type: CompressionCodec,
}
impl From<CompressionCodec> for TarFile {
    fn from(file_type: CompressionCodec) -> Self {
        Self { file_type }
    }
}

// The use of 'static here concerns me - as in "I use globals for everything" in C or "I make everything a static field + init" in java
impl TarFile {
    pub async fn load<R>(reader: &mut R) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadSeekTraits + AsyncBufRead,
    {
        let file_type = CompressedFile::identify_compression(reader).await?;
        reader.seek(SeekFrom::Start(0)).await?;
        let mut container = TarFileReader::new(file_type, reader);
        let entries = container.read_entries().await?;

        let archive_file_type = ArchiveType::Tar(file_type);
        Ok((archive_file_type, entries))
    }

    pub async fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadSeekTraits + AsyncBufRead,
    {
        let file_type = CompressedFile::identify_compression(reader).await?;
        reader.seek(SeekFrom::Start(0)).await?;

        let mut reader = DecompressStream::new(file_type, reader);

        let detection = identify_tar(&mut reader).await?;

        Ok(detection)
    }

    pub fn extract_entry<'a, R>(
        &'a self,
        reader: &'a mut R,
        entry: &'a ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<Vec<u8>>>
    where
        R: ArchiveFileReadTraits,
    {
        let file_type = self.file_type;
        let compressed_size = entry.get_compressed_size();
        let compressed_offset = entry.get_compressed_offset();

        async move {
            let reader = BufReader::new(reader);
            let mut reader = TarFileReader::new(file_type, reader);
            reader
                .get_block_reader_mut()
                .seek_to_offset(compressed_offset as usize)
                .await?;

            let mut v = vec![0u8; compressed_size];
            reader.get_block_reader_mut().read_bytes(&mut v).await?;
            Ok(v)
        }
        .boxed()
    }

    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        reader: R,
        entry: &ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<ArchiveEntryStream<R>>> {
        let file_type = self.file_type;
        let compressed_size = entry.get_compressed_size();
        let compressed_offset = entry.get_compressed_offset();
        async move {
            let reader = BufReader::new(reader);
            let mut reader = TarFileReader::new(file_type, reader);
            reader
                .get_block_reader_mut()
                .seek_to_offset(compressed_offset as usize)
                .await?;
            let reader = reader.into_inner();
            let reader = reader.into_inner();

            // There has _GOT to be a better way to do this...
            let reader = WindowedReader::new(reader, compressed_size as u64);
            let reader = ArchiveEntryStream::Tar(reader);
            Ok(reader)
        }
        .boxed()
    }
}

async fn identify_tar<S>(reader: &mut S) -> ArchiveResult<bool>
where
    S: ArchiveFileReadTraits,
{
    // Seek is not possible due to possibly being wrapped in Gzip (And I have no seekable gzip impl to rely on)
    // So read forward this many, so that it's "Seeked" to the position of the magic
    let mut buf = [0u8; 257];

    match reader.read_exact(&mut buf).await {
        Ok(_) => {}
        Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => {}
        Err(e) => return Err(e)?,
    };
    let mut buf = tar::constants::MAGIC_TAR_USTAR;

    match reader.read_exact(&mut buf).await {
        Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => return Ok(false),
        Err(e) => return Err(e)?,
        Ok(()) => {}
    };

    Ok(buf == tar::constants::MAGIC_TAR_USTAR)
}

#[cfg(test)]
mod test {
    use super::TarFile;
    use crate::non_blocking::{
        tests::bytesfs_provider::ReadOnlyByteFsProvider, utils::fs::FsFileProvider,
    };
    use crate::{ArchiveType, CompressionCodec};

    #[test]
    pub fn test_detect_targz() {
        let file_path = "test_files/tar/test.tar.gz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        futures_executor::block_on(async move {
            let mut reader = fs.open_file(file_path).await.unwrap();

            assert!(TarFile::detect_binary(&mut reader).await.unwrap());
        });
    }

    #[test]
    pub fn test_detect_tarxz() {
        let file_path = "test_files/tar/test.tar.xz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

        futures_executor::block_on(async move {
            let mut reader = fs.open_file(file_path).await.unwrap();
            assert!(TarFile::detect_binary(&mut reader).await.unwrap());
        });
    }

    #[test]
    pub fn test_detect_tgz() {
        let file_path = "test_files/tar/test.tgz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();
        futures_executor::block_on(async move {
            let mut reader = fs.open_file(file_path).await.unwrap();
            assert!(TarFile::detect_binary(&mut reader).await.unwrap());
        });
    }

    #[test]
    pub fn test_detect_targz_no_ext() {
        let file_path = "test_files/tar/testtargz_no_ext";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();
        futures_executor::block_on(async move {
            let mut reader = fs.open_file(file_path).await.unwrap();
            assert!(TarFile::detect_binary(&mut reader).await.unwrap());
        });
    }

    #[test]
    pub fn test_tar_file_load() {
        let file_path = "test_files/tar/test.tar.gz";
        let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();
        futures_executor::block_on(async move {
            let mut reader = fs.open_file(file_path).await.unwrap();
            let (archive_file_type, entries) = TarFile::load(&mut reader).await.unwrap();
            assert_eq!(archive_file_type, ArchiveType::Tar(CompressionCodec::GZIP));
            assert_eq!(entries.len(), 2);
            let entry = &entries[0];
            assert_eq!(entry.get_name(), "test/");
            let entry = &entries[1];
            assert_eq!(entry.get_name(), "test/test.txt");
        });
    }
}
