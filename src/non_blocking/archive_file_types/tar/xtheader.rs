use crate::non_blocking::utils::{binr::BlockReader, fs::ArchiveFileReadTraits};
use crate::{result::ArchiveResult, tar::headers::xheader::XtHeader};

pub struct AsyncXtHeader;

impl AsyncXtHeader {
    pub async fn parse<R: ArchiveFileReadTraits>(
        data_len: usize,
        reader: &mut BlockReader<R>,
    ) -> ArchiveResult<Vec<(XtHeader, String)>> {
        //let xh_str = reader.read_cstring(data_len + reader.block_size()).await?;
        let xh_str = reader.read_to_zero(data_len).await?;
        let xt_header = XtHeader::read(&xh_str)?;
        Ok(xt_header)
    }
}
