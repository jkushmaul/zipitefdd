use crate::non_blocking::streams::ArchiveEntryStream;
use crate::non_blocking::streams::DecompressStream;
use crate::non_blocking::utils::fs::ArchiveFileReadSeekTraits;
use crate::non_blocking::utils::fs::ArchiveFileReadTraits;
use crate::result::ArchiveError;
use crate::result::ArchiveResult;
use crate::ArchiveType;
use crate::{ArchiveEntry, CompressionCodec};
use effitfs::FileType;
use futures_util::{
    future::BoxFuture, io::BufReader, AsyncReadExt, AsyncSeek, AsyncSeekExt, FutureExt,
};
use std::{io::SeekFrom, path::Path};

/// Compressed file needs to know the original file name - because there is no "Manifest", it's just
/// compressed bytes.  In order to have a single "ArchiveEntry", a name is needed.
pub struct CompressedFile {
    file_type: CompressionCodec,
}
impl From<CompressionCodec> for CompressedFile {
    fn from(file_type: CompressionCodec) -> Self {
        Self { file_type }
    }
}

impl CompressedFile {
    pub async fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadTraits + AsyncSeek,
    {
        if let CompressionCodec::STORE = Self::identify_compression(reader).await? {
            return Ok(false);
        }
        Ok(true)
    }
    pub async fn load<R>(
        file_name: &str,
        mut reader: R,
    ) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadTraits + AsyncSeek,
    {
        let file_type = match Self::identify_compression(&mut reader).await? {
            CompressionCodec::STORE => return Err(ArchiveError::UnidentifiedCompressionmethod),
            s => s,
        };

        let p = Path::new(file_name);
        let file_name = match p.file_name() {
            Some(s) => s.to_string_lossy().to_string(),
            None => {
                return Err(ArchiveError::Other(format!(
                    "Could not decode filename: {}",
                    file_name
                )));
            }
        };

        let mut entries = vec![];

        let entry = ArchiveEntry::new(
            file_name,
            0,
            // Say what?  I guess it's because the decompression is one layer out - the actual entry inside the compressed file, isn't compressed when this is used... but it is true, it does not make sense.
            CompressionCodec::STORE,
            0,
            0,
            0,
            0,
            0,
            FileType::File,
            None,
        );
        entries.push(entry);

        Ok((ArchiveType::Compressed(file_type), entries))
    }

    pub async fn identify_compression<R>(reader: &mut R) -> ArchiveResult<CompressionCodec>
    where
        R: ArchiveFileReadTraits + AsyncSeek,
    {
        let mut buf = [0u8; 6];
        reader.read_exact(&mut buf).await?;
        reader.seek(SeekFrom::Start(0)).await?;
        Ok(CompressionCodec::from(buf))
    }

    pub fn extract_entry<'a, R: ArchiveFileReadSeekTraits>(
        &'a self,
        reader: &'a mut R,
        _: &'a ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<Vec<u8>>> {
        let file_type = self.file_type;
        async move {
            let reader = BufReader::new(reader);
            let mut reader = DecompressStream::new(file_type, reader);
            let mut buf = vec![];
            reader.read_to_end(&mut buf).await?;

            Ok(buf)
        }
        .boxed()
    }
    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        reader: R,
        _entry: &ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<ArchiveEntryStream<R>>> {
        let file_type = self.file_type;
        let reader = BufReader::new(reader);
        let reader = DecompressStream::new(file_type, reader);
        let reader = ArchiveEntryStream::CompressedFile(reader);
        async move { Ok(reader) }.boxed()
    }
}

#[cfg(test)]
mod test {

    use crate::{
        compressed,
        non_blocking::{
            archive_file_types::compressed::CompressedFile,
            tests::bytesfs_provider::ReadOnlyByteFsProvider, utils::fs::FsFileProvider,
        },
        result::ArchiveResult,
        ArchiveEntry, CompressionCodec,
    };
    use effitfs::FileType;
    use futures_util::io::{BufReader, Cursor};

    #[test]
    pub fn identify_compression() -> ArchiveResult<()> {
        futures_executor::block_on(async move {
            let file_path = "test_files/tar/test.tar.gz";
            let fs = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

            let mut reader = fs.open_file(file_path).await.unwrap();

            let file_type = CompressedFile::identify_compression(&mut reader).await?;
            if file_type == CompressionCodec::GZIP {
                return Ok(());
            };
            panic!("file_type was not Gz: {:?}", file_type);
        })
    }

    #[test]
    pub fn test_extract_entry_uncompressed() {
        let expected = "test".as_bytes();
        let c = CompressedFile::from(CompressionCodec::STORE);
        let r = Cursor::new(&expected);
        let mut r = BufReader::new(r);
        let entry = ArchiveEntry::new(
            "test".to_owned(),
            0,
            CompressionCodec::STORE,
            0,
            1,
            2,
            3,
            4,
            FileType::File,
            None,
        );
        let v =
            futures_executor::block_on(
                async move { c.extract_entry(&mut r, &entry).await.unwrap() },
            );

        assert_eq!(v.as_slice(), expected)
    }

    #[test]
    pub fn test_load_no_magic() {
        let expected = "test longer than six".as_bytes();
        let r = Cursor::new(&expected);
        let file_name = "";
        let result =
            futures_executor::block_on(async move { CompressedFile::load(file_name, r).await });
        assert!(result.is_err());
    }

    #[test]
    pub fn test_load_gz_bad_filename() {
        let mut expected = vec![];
        for b in compressed::constants::MAGIC_GZ {
            expected.push(b);
        }
        for i in 0..16 {
            expected.push(i);
        }
        let r = Cursor::new(&expected);
        let file_name = "";
        let result =
            futures_executor::block_on(async move { CompressedFile::load(file_name, r).await });
        assert!(result.is_err());
    }
}
