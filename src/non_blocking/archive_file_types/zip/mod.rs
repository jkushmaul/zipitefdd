pub mod bocd;
pub mod zip32;

use crate::non_blocking::{
    streams::{ArchiveEntryStream, DecompressStream},
    utils::{
        binr::{Binr, BinrSeekr, ByteOrder},
        fs::{ArchiveFileReadSeekTraits, ArchiveFileReadTraits},
        windowed_reader::WindowedReader,
    },
};
use crate::{
    result::{ArchiveError, ArchiveResult},
    zip::{constants, ZipFileType},
    ArchiveEntry, ArchiveType, CompressionCodec,
};
use async_compression::futures::bufread::{BzDecoder, DeflateDecoder};
use effitfs::FileType;
use futures_util::{
    future::BoxFuture,
    io::{BufReader, Cursor},
    AsyncReadExt, AsyncSeekExt, FutureExt,
};
use std::{collections::HashSet, io::SeekFrom};
use zip32::Zip32;

pub struct ZipFile;

impl ZipFile {
    pub async fn detect_binary<R>(reader: &mut R) -> ArchiveResult<bool>
    where
        R: ArchiveFileReadTraits,
    {
        let mut buf = vec![0u8; constants::MAGIC_PKZIP.len()];
        reader.read_exact(&mut buf).await?;

        Ok(buf == constants::MAGIC_PKZIP.as_slice())
    }

    pub async fn load<R>(reader: &mut R) -> ArchiveResult<(ArchiveType, Vec<ArchiveEntry>)>
    where
        R: ArchiveFileReadSeekTraits,
    {
        let mut reader = Binr::new(ByteOrder::Little, reader);
        let (zip_file_type, _) = Self::find_eocd(&mut reader).await?;

        let mut entries = match zip_file_type {
            ZipFileType::Zip32 => Zip32::read_entries(&mut reader).await?,
            ZipFileType::Zip64 => {
                return Err(ArchiveError::UnsupportedFileType(
                    "Zip64 is not supported".to_string(),
                ))
            }
        };
        let possible_targets: HashSet<String> = entries
            .iter()
            .filter(|e| e.get_file_type() == FileType::File)
            .map(|e| e.get_name().to_owned())
            .collect();

        //TODO: Post process all entries with symnlink type, extract it's data and set the link target.
        for e in entries
            .iter_mut()
            .filter(|e| e.get_file_type() == FileType::SymLink)
        {
            let data = Self::static_extract_entry(reader.get_inner_mut(), e).await?;
            let link_name = String::from_utf8_lossy(&data).to_string();
            if possible_targets.contains(&link_name) {
                e.metadata_mut().set_link_target(Some(link_name));
            } else {
                e.metadata_mut().set_file_type(FileType::File);
            }
        }

        Ok((ArchiveType::Zip(zip_file_type), entries))
    }

    async fn find_eocd<R>(reader: &mut Binr<R>) -> ArchiveResult<(ZipFileType, u64)>
    where
        R: ArchiveFileReadSeekTraits,
    {
        reader.seek(SeekFrom::End(0)).await?;

        //try each signature
        let needles = [constants::Z32_EOCD_SIG];

        let (_, offset) = match reader.rfind_u32(&needles).await? {
            Some(a) => a,
            None => {
                return Err(ArchiveError::SignatureNotFound(
                    "Could not find Z32 EOCD".to_string(),
                ))
            }
        };
        //position should be offset, seek to it anyways
        reader.seek(SeekFrom::Start(offset)).await?;

        Ok((ZipFileType::Zip32, offset))
    }
    async fn static_extract_entry<R>(
        reader: &mut R,
        dir_entry: &ArchiveEntry,
    ) -> ArchiveResult<Vec<u8>>
    where
        R: ArchiveFileReadSeekTraits,
    {
        reader
            .seek(SeekFrom::Start(dir_entry.get_compressed_offset()))
            .await?;

        let mut compressed_buffer = vec![0u8; dir_entry.get_compressed_size()];
        reader.read_exact(&mut compressed_buffer).await?;

        let mut decompressed_buffer = vec![0u8; dir_entry.get_uncompressed_size()];

        match dir_entry.get_compression_codec() {
            CompressionCodec::STORE => {
                decompressed_buffer = compressed_buffer;
            }
            CompressionCodec::BZIP2 => {
                let reader = Cursor::new(compressed_buffer);
                let mut decoder = BzDecoder::new(reader);
                decoder.read_exact(&mut decompressed_buffer).await?;
            }
            CompressionCodec::DEFLATE => {
                let reader = Cursor::new(compressed_buffer);
                let mut decoder = DeflateDecoder::new(reader);
                decoder.read_exact(&mut decompressed_buffer).await?;
            }
            _ => return Err(ArchiveError::UnidentifiedCompressionmethod),
        };

        let crc = crc32fast::hash(&decompressed_buffer);

        if crc as u64 != dir_entry.get_uncompressed_crc() {
            return Err(ArchiveError::CrcMismatch(dir_entry.get_name().to_string()));
        }

        Ok(decompressed_buffer)
    }

    pub fn extract_entry<'a, R>(
        &self,
        reader: &'a mut R,
        dir_entry: &'a ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<Vec<u8>>>
    where
        R: ArchiveFileReadSeekTraits,
    {
        async move {
            reader
                .seek(SeekFrom::Start(dir_entry.get_compressed_offset()))
                .await?;

            let mut compressed_buffer = vec![0u8; dir_entry.get_compressed_size()];
            reader.read_exact(&mut compressed_buffer).await?;

            let mut decompressed_buffer = vec![0u8; dir_entry.get_uncompressed_size()];

            match dir_entry.get_compression_codec() {
                CompressionCodec::STORE => {
                    decompressed_buffer = compressed_buffer;
                }
                CompressionCodec::BZIP2 => {
                    let reader = Cursor::new(compressed_buffer);
                    let mut decoder = BzDecoder::new(reader);
                    decoder.read_exact(&mut decompressed_buffer).await?;
                }
                CompressionCodec::DEFLATE => {
                    let reader = Cursor::new(compressed_buffer);
                    let mut decoder = DeflateDecoder::new(reader);
                    decoder.read_exact(&mut decompressed_buffer).await?;
                }
                _ => return Err(ArchiveError::UnidentifiedCompressionmethod),
            };

            let crc = crc32fast::hash(&decompressed_buffer);

            if crc as u64 != dir_entry.get_uncompressed_crc() {
                return Err(ArchiveError::CrcMismatch(dir_entry.get_name().to_string()));
            }

            Ok(decompressed_buffer)
        }
        .boxed()
    }

    pub fn open_entry<'a, R: ArchiveFileReadSeekTraits + 'a>(
        &self,
        mut reader: R,
        dir_entry: &ArchiveEntry,
    ) -> BoxFuture<'a, ArchiveResult<ArchiveEntryStream<R>>> {
        let compressed_offset = dir_entry.get_compressed_offset();
        let compression_codec = dir_entry.get_compression_codec();
        let compressed_size = dir_entry.get_compressed_size();

        async move {
            reader.seek(SeekFrom::Start(compressed_offset)).await?;
            let reader = BufReader::new(reader);
            let reader = DecompressStream::new(compression_codec, reader);
            let reader = WindowedReader::new(reader, compressed_size as u64);
            let reader = ArchiveEntryStream::Zip(reader);

            Ok(reader)
        }
        .boxed()
    }
}

#[cfg(test)]
pub mod tests {
    use crate::non_blocking::utils::binr::{Binr, ByteOrder};
    use crate::non_blocking::{
        archive_file_types::zip::{zip32::Zip32, ZipFile},
        utils::binr::BinrSeekr,
    };
    use crate::test_utils;
    use crate::{zip::ZipFileType, ArchiveType};
    use futures_util::io::Cursor;
    use std::io::SeekFrom;

    pub fn zip32_binr<'a>() -> Binr<Cursor<&'a [u8]>> {
        let reader = Cursor::new(test_utils::zip32::IT_WAS_JUST_A_TEST_ZIP32);

        Binr::new(ByteOrder::Little, reader)
    }

    #[test]
    fn test_find_first_sig_zip32() {
        futures_executor::block_on(async move {
            let mut reader = zip32_binr();
            let result = ZipFile::find_eocd(&mut reader).await;

            assert!(result.is_ok());
            let result = result.unwrap();
            assert_eq!(result.0, ZipFileType::Zip32);
            assert_eq!(result.1, test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD);
        });
    }

    #[test]
    fn test_read_zip_type_zip32() {
        futures_executor::block_on(async move {
            let reader = zip32_binr();
            let mut reader = reader.into_inner();

            let result = ZipFile::load(&mut reader).await;

            assert!(result.is_ok());
            let (zip_file_type, entries) = result.unwrap();

            assert_eq!(zip_file_type, ArchiveType::Zip(ZipFileType::Zip32));
            assert_eq!(1, entries.len());

            let entry = entries.first().unwrap();

            let mut reader = zip32_binr().into_inner();

            let result = ZipFile;

            let asset = result.extract_entry(&mut reader, entry).await.unwrap();
            assert_eq!(
                asset.len(),
                test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len()
            );
        });
    }

    #[test]
    fn loads_zip32_extracts_store() {
        futures_executor::block_on(async move {
            let mut reader = zip32_binr();
            reader
                .seek(SeekFrom::Start(test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD))
                .await
                .unwrap();

            let entries = Zip32::read_entries(&mut reader).await.unwrap();
            let entry = entries.first().unwrap();
            let mut reader = reader.into_inner();
            let zip = ZipFile;
            let buf = match zip.extract_entry(&mut reader, entry).await {
                Ok(buf) => buf,
                Err(e) => {
                    panic!("Failed to extract entry: {}", e);
                }
            };

            assert_eq!(buf.len(), test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len());
            let actual = String::from_utf8_lossy(&buf).to_string();
            assert_eq!(actual, test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT);

            assert_eq!(entries.len(), 1);
            assert_eq!(
                entries[0].get_name(),
                test_utils::zip32::IT_WAS_JUST_A_TEST_FILE
            );
        });
    }
}
