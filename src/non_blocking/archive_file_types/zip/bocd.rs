// https://en.wikipedia.org/wiki/ZIP_(file_format)

use crate::non_blocking::utils::{
    binr::{Binr, BinrParser},
    fs::ArchiveFileReadTraits,
};
use crate::{
    result::{ArchiveError, ArchiveResult},
    zip::bocd::CentralDirectoryHead,
};
use futures_util::{future::BoxFuture, AsyncSeek, FutureExt};

/// Minimum logic to support async trait
pub struct AsyncCentralDirectoryHead;

impl<R> BinrParser<R, CentralDirectoryHead> for AsyncCentralDirectoryHead
where
    R: ArchiveFileReadTraits + AsyncSeek,
{
    fn parse(parser: &mut Binr<R>) -> BoxFuture<'_, ArchiveResult<CentralDirectoryHead>>
    where
        R: ArchiveFileReadTraits,
    {
        async move {
            let signature: u32 = parser.read_u32().await?; //0x02014b50
            if signature != CentralDirectoryHead::SIGNATURE {
                return Err(ArchiveError::InvalidSignature(format!(
                    "Expected signature {} but found {}",
                    CentralDirectoryHead::SIGNATURE,
                    signature
                )));
            }
            let version_created: u16 = parser.read_u16().await?;
            let version_target: u16 = parser.read_u16().await?;
            let general_purpose: u16 = parser.read_u16().await?;
            let compression_method: u16 = parser.read_u16().await?;
            let last_mod_time: u16 = parser.read_u16().await?;
            let last_mod_date: u16 = parser.read_u16().await?;
            let crc32_uncompressed: u32 = parser.read_u32().await?;
            let compressed_size: u32 = parser.read_u32().await?; // or 0xffffffff for zip64
            let uncompressed_size: u32 = parser.read_u32().await?; // same
            let filename_length: u16 = parser.read_u16().await?;
            let extra_field_length: u16 = parser.read_u16().await?;
            let file_comment_length: u16 = parser.read_u16().await?;
            let disk_number: u16 = parser.read_u16().await?; // 0xffff for zip64
            let internal_file_attributes: u16 = parser.read_u16().await?;
            let external_file_attributes: u32 = parser.read_u32().await?;
            let local_file_header_offset: u32 = parser.read_u32().await?; //  0xffffffff for zip64

            let filename = parser.read_string(filename_length as usize).await?;
            let extra_field = parser.read_bytes(extra_field_length as usize).await?;
            let file_comment = parser.read_bytes(file_comment_length as usize).await?;

            Ok(CentralDirectoryHead {
                signature,
                version_created,
                version_target,
                general_purpose,
                compression_method,
                last_mod_time,
                last_mod_date,
                crc32_uncompressed,
                compressed_size,
                uncompressed_size,
                filename_length,
                extra_field_length,
                file_comment_length,
                disk_number,
                internal_file_attributes,
                external_file_attributes,
                local_file_header_offset,
                filename,
                extra_field,
                file_comment,
            })
        }
        .boxed()
    }
}
