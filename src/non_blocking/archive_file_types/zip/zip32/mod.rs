pub mod cd_tail;
pub mod local_file;

use super::bocd::AsyncCentralDirectoryHead;
use crate::{
    non_blocking::utils::{
        binr::{Binr, BinrParser, BinrSeekr},
        fs::ArchiveFileReadSeekTraits,
    },
    result::ArchiveResult,
    zip::{bocd::CentralDirectoryHead, zip32::Zip32CentralDirectoryTail},
    ArchiveEntry,
};
use cd_tail::AsyncZip32CentralDirectoryTail;
use local_file::AsyncLocalFileHeader;
use std::io::SeekFrom;

/*
 * The goal of this is to be able to concurrently read zip entries.
 * One could simply just re-open the file and make a new ziparchive object every time
 * but there is expensive enough initialization that should be avoided by having a reusable object.
 *
 * One does not simply concurrently read from the same file handle.  So, to avoid this, the file
 * is not retained but the indexed contents are.
 * That would allow a file to be mutated between calls.  To avoid that, the retained object is a write-lock
 * on the file.
 * The file is then opened on-demand providing unique file handles at any point needed.
 *
 */

pub struct Zip32;

/// Minimum logic to support async trait
impl Zip32 {
    async fn read_cd<R: ArchiveFileReadSeekTraits>(
        parser: &mut Binr<R>,
        cd_tail: &Zip32CentralDirectoryTail,
    ) -> ArchiveResult<Vec<CentralDirectoryHead>> {
        parser
            .seek(SeekFrom::Start(cd_tail.central_directory_offset as u64))
            .await?;

        let mut v = Vec::new();
        for _ in 0..cd_tail.total_central_directory_records {
            let cd_head = AsyncCentralDirectoryHead::parse(parser).await?;
            v.push(cd_head);
        }

        Ok(v)
    }

    async fn read_local_files<R: ArchiveFileReadSeekTraits>(
        parser: &mut Binr<R>,
        directory: Vec<CentralDirectoryHead>,
    ) -> ArchiveResult<Vec<ArchiveEntry>> {
        let mut v = Vec::new();

        for (i, cd) in directory.into_iter().enumerate() {
            parser
                .seek(SeekFrom::Start(cd.local_file_header_offset as u64))
                .await?;
            let lf = AsyncLocalFileHeader::parse(parser).await?;
            let offset = parser.stream_position().await?;
            let entry = lf.into_archive_entry(offset, i, &cd)?;
            v.push(entry);
        }

        Ok(v)
    }

    pub async fn read_entries<R: ArchiveFileReadSeekTraits>(
        parser: &mut Binr<R>,
    ) -> ArchiveResult<Vec<ArchiveEntry>> {
        let cd_tail = AsyncZip32CentralDirectoryTail::parse(parser).await?;
        let directory = Self::read_cd(parser, &cd_tail).await?;
        let entries = Self::read_local_files(parser, directory).await?;

        Ok(entries)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        non_blocking::{
            archive_file_types::zip::{self, zip32::Zip32},
            utils::binr::BinrSeekr,
        },
        test_utils,
    };
    use std::io::SeekFrom;

    #[test]
    pub fn loads_zip32() {
        futures_executor::block_on(async move {
            let mut reader = zip::tests::zip32_binr();
            reader
                .seek(SeekFrom::Start(test_utils::zip32::IT_WAS_JUST_A_TEST_EOCD))
                .await
                .unwrap();

            let entries = Zip32::read_entries(&mut reader).await.unwrap();

            assert_eq!(entries.len(), 1);
            assert_eq!(entries[0].get_name(), "itwasjustatest.txt");
            assert_eq!(
                entries[0].get_metadata().len() as usize,
                test_utils::zip32::IT_WAS_JUST_A_TEST_TEXT.len()
            );
        });
    }
}
