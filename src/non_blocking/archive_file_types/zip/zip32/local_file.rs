use crate::non_blocking::utils::{
    binr::{Binr, BinrParser},
    fs::ArchiveFileReadTraits,
};
use crate::{
    result::{ArchiveError, ArchiveResult},
    zip::zip32::LocalFileHeader,
};
use futures_util::{future::BoxFuture, FutureExt};

/// Minimum logic to support async trait
pub struct AsyncLocalFileHeader;

impl<R: ArchiveFileReadTraits> BinrParser<R, LocalFileHeader> for AsyncLocalFileHeader {
    fn parse(parser: &mut Binr<R>) -> BoxFuture<'_, ArchiveResult<LocalFileHeader>> {
        async move {
            let signature: u32 = parser.read_u32().await?; //0x04034b50
            if signature != LocalFileHeader::SIGNATURE {
                return Err(ArchiveError::InvalidSignature(format!(
                    "Expected signature {} but found {}",
                    LocalFileHeader::SIGNATURE,
                    signature
                )));
            }
            let min_version: u16 = parser.read_u16().await?;
            let general_purpose_flags: u16 = parser.read_u16().await?;
            let compression_method: u16 = parser.read_u16().await?;
            // Given an archive with last mod: 2023-06-16 20:03:27.000000000  (@1686960207)
            // Per pkware APPNOTE 6.3.5 date and time encoded in standard ms-dos format (yay!)
            // file_last_mod_time: 41070
            // file_last_mod_date: 22224

            let file_last_mod_time: u16 = parser.read_u16().await?;
            let file_last_mod_date: u16 = parser.read_u16().await?;

            let crc32_uncompressed: u32 = parser.read_u32().await?;
            let compressed_size: u32 = parser.read_u32().await?; // or zip64 0xffffffff
            let uncompressed_size: u32 = parser.read_u32().await?; // same
            let filename_length: u16 = parser.read_u16().await?;
            let extra_field_length: u16 = parser.read_u16().await?;

            let filename: String = parser.read_string(filename_length as usize).await?;
            let extra_field: Vec<u8> = parser.read_bytes(extra_field_length as usize).await?;

            Ok(LocalFileHeader {
                signature,
                min_version,
                general_purpose_flags,
                compression_method,
                file_last_mod_time,
                file_last_mod_date,
                crc32_uncompressed,
                compressed_size,
                uncompressed_size,
                filename_length,
                extra_field_length,
                filename,
                extra_field,
            })
        }
        .boxed()
    }
}
