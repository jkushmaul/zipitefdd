use crate::non_blocking::utils::{
    binr::{Binr, BinrParser},
    fs::ArchiveFileReadTraits,
};
use crate::{
    result::{ArchiveError, ArchiveResult},
    zip::zip32::Zip32CentralDirectoryTail,
};
use futures_util::{future::BoxFuture, FutureExt};

/// Minimum logic to support async trait
pub struct AsyncZip32CentralDirectoryTail;

impl<R: ArchiveFileReadTraits> BinrParser<R, Zip32CentralDirectoryTail>
    for AsyncZip32CentralDirectoryTail
where
    R: ArchiveFileReadTraits,
{
    fn parse(parser: &mut Binr<R>) -> BoxFuture<'_, ArchiveResult<Zip32CentralDirectoryTail>> {
        async move {
            let signature: u32 = parser.read_u32().await?; //0x06054b50
            if signature != Zip32CentralDirectoryTail::SIGNATURE {
                return Err(ArchiveError::InvalidSignature(format!(
                    "Expected signature {} but found {}",
                    Zip32CentralDirectoryTail::SIGNATURE,
                    signature
                )));
            }
            let disk_index: u16 = parser.read_u16().await?; // or 0xffff for zip64
            let central_directory_disk_start: u16 = parser.read_u16().await?; //same
            let central_directory_records_this_disk: u16 = parser.read_u16().await?; //same
            let total_central_directory_records: u16 = parser.read_u16().await?; //same
            let central_directory_length: u32 = parser.read_u32().await?; //     0xffffffff for zip64
            let central_directory_offset: u32 = parser.read_u32().await?; // same
            let comment_length: u16 = parser.read_u16().await?; //
                                                                //let comment: Vec<u8> = parser.read_bytes(comment_length as usize).await?;

            Ok(Zip32CentralDirectoryTail {
                signature,
                disk_index,
                central_directory_disk_start,
                central_directory_records_this_disk,
                total_central_directory_records,
                central_directory_length,
                central_directory_offset,
                comment_length,
                //comment,
            })
        }
        .boxed()
    }
}
