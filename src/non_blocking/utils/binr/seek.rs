use crate::result::ArchiveResult;
use futures_util::{future::BoxFuture, AsyncSeek, AsyncSeekExt, FutureExt};

use super::Binr;
use crate::non_blocking::utils::fs::ArchiveFileReadTraits;
use std::io::SeekFrom;

/// Needed to support async seek when needed without requiring it in main impl
pub trait BinrSeekr<R: ArchiveFileReadTraits + AsyncSeek> {
    /// Same as std seek
    fn seek(&mut self, seek_from: SeekFrom) -> BoxFuture<ArchiveResult<u64>>;
    /// Same as std stream_position
    fn stream_position(&mut self) -> BoxFuture<ArchiveResult<u64>>;
    /// Given a list of 4 byte targets, find them in from the tail end
    fn rfind_u32<'a>(
        &'a mut self,
        targets: &'a [u32],
    ) -> BoxFuture<'a, ArchiveResult<Option<(usize, u64)>>>;
}

impl<R: ArchiveFileReadTraits + AsyncSeek> BinrSeekr<R> for Binr<R> {
    fn seek(&mut self, seek_from: SeekFrom) -> BoxFuture<ArchiveResult<u64>> {
        async move {
            let pos = self.get_inner_mut().seek(seek_from).await?;
            Ok(pos)
        }
        .boxed()
    }

    fn stream_position(&mut self) -> BoxFuture<ArchiveResult<u64>> {
        async move {
            let offset = self.get_inner_mut().stream_position().await?;
            Ok(offset)
        }
        .boxed()
    }

    //read backwards by seeking earlier, filling buffer, finding
    //expects reader to already be positioned at starting, er, um ending point. you know what I mean.
    //(end of file usually)
    //The left over stream position will be the last occurence
    fn rfind_u32<'a>(
        &'a mut self,
        targets: &'a [u32],
    ) -> BoxFuture<'a, ArchiveResult<Option<(usize, u64)>>> {
        async move {
            let mut byte_targets: Vec<[u8; 4]> = Vec::new();
            for x in targets {
                let b = x.to_le_bytes();
                let c = [b[0], b[1], b[2], b[3]];
                byte_targets.push(c)
            }

            let mut buf;

            let total_length = self.seek(SeekFrom::End(0)).await? as usize;
            let mut buf_size: usize = 4096;
            let mut current_position = if buf_size > total_length {
                buf_size = total_length;
                buf = vec![0u8; buf_size];
                self.seek(SeekFrom::Start(0)).await?
            } else {
                buf = vec![0u8; buf_size];
                self.seek(SeekFrom::End(-(buf_size as i64))).await?
            };

            loop {
                self.read_exact(&mut buf).await?;

                //search buffer
                if let Some((needle_index, buffer_offset)) =
                    rindex_u32(byte_targets.as_slice(), &buf)
                {
                    //align to the offset returned, which is forward from current
                    current_position += buffer_offset as u64;
                    self.seek(SeekFrom::Start(current_position)).await?;
                    return Ok(Some((needle_index, current_position)));
                }

                if current_position < 3 {
                    break;
                }

                //Use padding to solve boundary issues
                let look_back: i64 = buf_size as i64 - 4;

                //if the current position is less than lookback, then this is the last pull.
                if current_position < look_back as u64 {
                    //read exact only what's left.
                    buf = vec![0u8; current_position as usize];
                    self.seek(SeekFrom::Start(0)).await?;
                    current_position = 0;
                } else {
                    //after the first read, only seek back buf_size - 3 to handle edge cases
                    current_position = self.seek(SeekFrom::Current(-look_back)).await?;
                }
            }
            Ok(None)
        }
        .boxed()
    }
}

/**
 * Finds the first occurence of needle from tail.
 *
 * Returns Optionial pair of indexes, the first index is the index in needles that
 *  had the first (or last?) match.  The second is the starting offset in
 *  haystack.
 *
 *
 * I'd like to make this a bit more generic thanjust for 4 byte sizes.
 */
fn rindex_u32(needles: &[[u8; 4]], haystack: &[u8]) -> Option<(usize, usize)> {
    let mut i: usize = haystack.len();

    /*
     * Starting from the end, look at current haystack
     * and compare that against each of the needles last char.
     * on each match, fetch a slice of needle size back (so the last char of that is the char just checked)
     *  from haystack and compare it to needle.
     *
     * It's a race to the finish line and there can be only one.
     * If two match - the first one gets the prize.
     *
     */
    while i > 3 {
        i -= 1;

        //Only check last char of needle for current haystack, for each needle

        for (j, n) in needles.iter().enumerate() {
            //Name your favorite dinosaur
            if haystack[i] == n[3] {
                //Velociraptor!
                let target_offset = i - 3;
                let slice = &haystack[target_offset..i + 1];
                //Did we just become best friends!?
                if slice == n {
                    //YUP!
                    return Some((j, target_offset));
                }
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use crate::non_blocking::utils::binr::seek::rindex_u32;

    #[test]
    pub fn rfind_u32() {}

    #[test]
    pub fn test_slice_sanity() {
        //a reality check for me
        let needle: [u8; 4] = [3, 4, 5, 6];
        let haystack: [u8; 10] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        let a = needle.as_slice();
        let _haystack = haystack.as_slice();

        let b = &_haystack[3..7];
        assert!(a == b);
    }

    #[test]
    pub fn test_rindex_u32_start() {
        let needles = [[0, 1, 2, 3], [6, 0, 1, 2]];
        let haystack = [0, 1, 2, 3, 1, 2, 3, 4, 5, 6, 5, 6, 7, 8, 0, 9, 8, 7, 6];
        let (needle_index, haystack_offset) = rindex_u32(&needles, &haystack).unwrap();
        assert_eq!(needle_index, 0);
        assert_eq!(haystack_offset, 0);
    }

    #[test]
    pub fn test_rindex_u32_mid() {
        let needles = [[0, 1, 2, 3], [6, 0, 1, 2]];
        let haystack = [1, 2, 3, 4, 5, 6, 0, 1, 2, 3, 5, 6, 7, 8, 0, 9, 8, 7, 6];
        let (needle_index, haystack_offset) = rindex_u32(&needles, &haystack).unwrap();
        assert_eq!(needle_index, 0);
        assert_eq!(haystack_offset, 6);
    }

    #[test]
    pub fn test_rindex_u32_tail() {
        let needles = [[0, 1, 2, 3], [6, 0, 1, 2]];
        let haystack = [1, 2, 3, 4, 5, 6, 5, 6, 7, 8, 0, 9, 8, 7, 6, 0, 1, 2, 3];
        let (needle_index, haystack_offset) = rindex_u32(&needles, &haystack).unwrap();
        assert_eq!(needle_index, 0);
        assert_eq!(haystack_offset, haystack.len() - 4);
    }

    #[test]
    pub fn test_rindex_u32_none() {
        let needles = [[3, 2, 1, 0], [6, 5, 4, 3]];
        let haystack = [1, 2, 3, 4, 5, 6, 5, 6, 7, 8, 0, 9, 8, 7, 6, 0, 1, 2, 3];
        assert!(rindex_u32(&needles, &haystack).is_none());
    }

    #[test]
    pub fn test_rfind_u32_lessthan3() {
        let needles = [[3, 2, 1, 0]];
        let haystack = vec![0u8; 4099];
        assert!(rindex_u32(&needles, &haystack).is_none());
    }
}
