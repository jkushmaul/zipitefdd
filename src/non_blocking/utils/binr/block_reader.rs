use crate::result::{ArchiveError, ArchiveResult};
use futures_util::AsyncReadExt;

use crate::non_blocking::utils::fs::ArchiveFileReadTraits;
use std::io::{Cursor, Write};

/// Used to read in blocks a binr without forcing it on impl
pub struct BlockReader<R: ArchiveFileReadTraits> {
    block_size: usize,
    inner: R,
    offset: usize,
}
impl<R: ArchiveFileReadTraits> BlockReader<R> {
    /// construct a new block reader
    pub fn new(block_size: usize, inner: R) -> Self {
        Self {
            inner,
            offset: 0,
            block_size,
        }
    }

    /// Each block will be read into one of these
    pub fn alloc_block(&self) -> Vec<u8> {
        vec![0u8; self.block_size]
    }

    /// current offset
    pub fn current_offset(&self) -> usize {
        self.offset
    }

    /// the constructed block size
    pub fn block_size(&self) -> usize {
        self.block_size
    }

    /// Reads an entire block or fails
    pub async fn read_block(&mut self, buf: &mut [u8]) -> ArchiveResult<()> {
        self.inner.read_exact(buf).await?;
        self.offset += self.block_size;
        Ok(())
    }

    /// Useful to skip over large amounts of bytes to next block
    pub async fn skip_bytes(&mut self, len: usize) -> ArchiveResult<()> {
        let target_offset = self.offset + len;

        let mut buf = self.alloc_block();

        while self.offset < target_offset {
            self.read_block(&mut buf).await?;
        }

        Ok(())
    }

    /// Similar to skip bytes, in fact, I'm not sure the difference right now
    pub async fn seek_to_offset(&mut self, offset: usize) -> ArchiveResult<()> {
        if self.current_offset() > offset {
            return Err(ArchiveError::Other(
                "Out of order offset, cannot seek backwards".to_owned(),
            ));
        }

        let delta = offset - self.current_offset();
        if delta > 0 {
            self.skip_bytes(delta).await?;
        }

        Ok(())
    }

    /// Reads len bytes intoa  string
    pub async fn read_string(&mut self, len: usize) -> ArchiveResult<String> {
        let mut buf = vec![0u8; len];
        self.read_bytes(&mut buf).await?;

        Ok(String::from_utf8_lossy(&buf).to_string())
    }

    /// Reads a all of len, then loads a string from all bytes until first zero or end
    pub async fn read_to_zero(&mut self, len: usize) -> ArchiveResult<Vec<u8>> {
        let mut buf = vec![0u8; len];
        self.read_bytes(&mut buf).await?;

        let first_zero = buf
            .iter()
            .enumerate()
            .find(|(_, c)| **c == 0)
            .map_or(buf.len(), |(i, _)| i);

        let bytes = &buf.as_slice()[0..first_zero];
        Ok(bytes.to_vec())
    }

    /// Reads a all of len, then loads a string from all bytes until first zero or end
    pub async fn read_cstring(&mut self, len: usize) -> ArchiveResult<String> {
        let mut buf = vec![0u8; len];
        self.read_bytes(&mut buf).await?;

        let first_zero = buf
            .iter()
            .enumerate()
            .find(|(_, c)| **c == 0)
            .map_or(buf.len(), |(i, _)| i);

        let chars = &buf.as_slice()[0..first_zero];
        Ok(String::from_utf8_lossy(chars).to_string())
    }

    /// Read a slice of bytes
    pub async fn read_bytes(&mut self, buf: &mut [u8]) -> ArchiveResult<()> {
        let mut block = vec![0u8; self.block_size];

        let total_len: usize = buf.len();
        let start_offset = self.offset;
        let target_offset = start_offset + total_len;

        let mut c = Cursor::new(buf);

        while self.offset < target_offset {
            self.read_block(&mut block).await?;
            if self.offset > target_offset {
                let current_len = c.position() as usize;
                c.write_all(&block[0..total_len - current_len])?;
                break;
            } else {
                c.write_all(&block)?;
            }
        }

        Ok(())
    }

    /// Releases inner object back
    pub fn into_inner(self) -> R {
        self.inner
    }
}

#[cfg(test)]
mod test {
    use futures_util::io::Cursor;

    use super::BlockReader;

    const BLOCK_SIZE: usize = 7;

    fn make_reader() -> BlockReader<Cursor<Vec<u8>>> {
        let mut buf = vec![0u8; 0xff];

        for i in 0..0xff_u8 {
            buf[i as usize] = i;
        }
        let reader = Cursor::new(buf);

        BlockReader::new(BLOCK_SIZE, reader)
    }

    fn make_reader_from_buf(buf: Vec<u8>) -> BlockReader<Cursor<Vec<u8>>> {
        let reader = Cursor::new(buf);

        BlockReader::new(BLOCK_SIZE, reader)
    }

    #[test]
    pub fn test_new_current_offset() {
        let reader = make_reader();
        assert_eq!(0, reader.current_offset());
    }

    #[test]
    pub fn test_new_block_size() {
        let reader = make_reader();
        assert_eq!(BLOCK_SIZE, reader.block_size());
    }

    #[test]
    pub fn test_alloc_block() {
        let reader = make_reader();
        let block = reader.alloc_block();
        assert_eq!(block.capacity(), BLOCK_SIZE);
    }

    #[test]
    pub fn test_read_block() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();
            let mut buf = reader.alloc_block();

            reader.read_block(&mut buf).await.unwrap();

            const EXPECTED: [u8; BLOCK_SIZE] = [0, 1, 2, 3, 4, 5, 6];
            assert_eq!(buf.as_slice(), &EXPECTED);
            assert_eq!(reader.current_offset(), BLOCK_SIZE);
        });
    }

    #[test]
    pub fn test_read_bytes() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();

            let mut buf = vec![0u8; 8];

            reader.read_bytes(&mut buf).await.unwrap();

            const EXPECTED: [u8; BLOCK_SIZE + 1] = [0, 1, 2, 3, 4, 5, 6, 7];
            assert_eq!(buf.as_slice(), &EXPECTED);
            assert_eq!(reader.current_offset(), BLOCK_SIZE * 2);
        });
    }

    #[test]
    pub fn test_read_string() {
        futures_executor::block_on(async move {
            let mut buf = "test".as_bytes().to_vec();
            for x in 0..BLOCK_SIZE as u8 {
                buf.push(x);
            }

            let mut reader = make_reader_from_buf(buf);

            let result = reader.read_string(4).await.unwrap();

            const EXPECTED: &str = "test";
            assert_eq!(result.as_str(), EXPECTED);
        });
    }

    #[test]
    pub fn test_read_cstring_exact() {
        futures_executor::block_on(async move {
            let mut buf = "test".as_bytes().to_vec();
            buf.push(0);
            for x in 0..BLOCK_SIZE as u8 {
                buf.push(x);
            }
            let mut reader = make_reader_from_buf(buf);

            let result = reader.read_cstring(5).await.unwrap();

            const EXPECTED: &str = "test";
            assert_eq!(result.as_str(), EXPECTED);
        });
    }

    #[test]
    pub fn test_read_cstring_truncated() {
        futures_executor::block_on(async move {
            let mut buf = "test".as_bytes().to_vec();
            buf.push(0);

            for x in "again".as_bytes() {
                buf.push(*x);
            }
            let mut reader = make_reader_from_buf(buf);

            let result = reader.read_cstring(6).await.unwrap();

            const EXPECTED: &str = "test";
            assert_eq!(result.as_str(), EXPECTED);
        });
    }

    #[test]
    pub fn test_skip_bytes() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();

            reader.skip_bytes(BLOCK_SIZE + 1).await.unwrap();

            assert_eq!(reader.current_offset(), BLOCK_SIZE * 2);
        });
    }

    #[test]
    pub fn test_seek_to_offset_ok() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();

            reader.seek_to_offset(BLOCK_SIZE + 1).await.unwrap();

            assert_eq!(reader.current_offset(), BLOCK_SIZE * 2);
        });
    }

    #[test]
    pub fn test_seek_to_offset_zero_ok() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();

            reader.seek_to_offset(0).await.unwrap();

            assert_eq!(reader.current_offset(), 0);
        });
    }

    #[test]
    pub fn test_seek_to_offset_backwards_fails() {
        futures_executor::block_on(async move {
            let mut reader = make_reader();

            reader.seek_to_offset(1).await.unwrap();
            let result = reader.seek_to_offset(0).await;
            assert!(result.is_err());
        });
    }
}
