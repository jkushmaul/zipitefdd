//! Binr - async binary reader - more than likely something better

mod block_reader;
mod seek;

use super::fs::ArchiveFileReadTraits;
pub use block_reader::BlockReader;
use futures_util::{future::BoxFuture, AsyncReadExt, FutureExt};
pub use seek::BinrSeekr;
use crate::result::ArchiveResult;

/// Used to identify byte order for reader
pub enum ByteOrder {
    /// Little endian
    Little,
    /// Big endian
    Big,
}

/// Wraps R to allow binary reads against it
pub struct Binr<R>
where
    R: ArchiveFileReadTraits,
{
    byte_order: ByteOrder,
    inner: R,
}

/// Used to parse a struct using Binr
pub trait BinrParser<R, T>
where
    R: ArchiveFileReadTraits,
{
    /// The implementor will utilize binr and return self
    fn parse(parser: &mut Binr<R>) -> BoxFuture<'_, ArchiveResult<T>>;
}

impl<R: ArchiveFileReadTraits> Binr<R> {
    /// Wrap R as a binr
    pub fn new(byte_order: ByteOrder, inner: R) -> Self {
        Self { byte_order, inner }
    }

    /// get the wrapped R
    pub fn get_inner_mut(&mut self) -> &mut R {
        &mut self.inner
    }

    /// Forget self and go back to R
    pub fn into_inner(self) -> R {
        self.inner
    }

    /// Read exactly the size of buf or fail
    pub fn read_exact<'a, 'b: 'a>(
        &'a mut self,
        buf: &'b mut [u8],
    ) -> BoxFuture<'a, ArchiveResult<()>> {
        async move {
            self.inner.read_exact(buf).await?;
            Ok(())
        }
        .boxed()
    }

    /// Read the exact size of bytes specified by len
    pub fn read_bytes(&mut self, len: usize) -> BoxFuture<ArchiveResult<Vec<u8>>> {
        async move {
            let mut buf = vec![0u8; len];
            self.read_exact(&mut buf).await?;
            Ok(buf)
        }
        .boxed()
    }

    /// Load len bytes into a string
    pub fn read_string(&mut self, len: usize) -> BoxFuture<ArchiveResult<String>> {
        async move {
            let buf = self.read_bytes(len).await?;

            let result = String::from_utf8_lossy(&buf).to_string();
            Ok(result)
        }
        .boxed()
    }

    /// read 4 bytes as little endian
    pub fn read_u32(&mut self) -> BoxFuture<ArchiveResult<u32>> {
        async move {
            let mut buf: [u8; 4] = [0u8; 4];
            self.read_exact(&mut buf).await?;

            let v = match self.byte_order {
                ByteOrder::Little => u32::from_le_bytes(buf),
                ByteOrder::Big => u32::from_be_bytes(buf),
            };

            Ok(v)
        }
        .boxed()
    }

    /// read 8 bytes as little endian
    pub fn read_u64(&mut self) -> BoxFuture<ArchiveResult<u64>> {
        async move {
            let mut buf = [0u8; 8];
            self.read_exact(&mut buf).await?;
            let v = match self.byte_order {
                ByteOrder::Little => u64::from_le_bytes(buf),
                ByteOrder::Big => u64::from_be_bytes(buf),
            };

            Ok(v)
        }
        .boxed()
    }

    /// read 2 bytes as little endian
    pub fn read_u16(&mut self) -> BoxFuture<ArchiveResult<u16>> {
        async move {
            let mut buf: [u8; 2] = [0, 0];
            self.read_exact(&mut buf).await?;

            let v = match self.byte_order {
                ByteOrder::Little => u16::from_le_bytes(buf),
                ByteOrder::Big => u16::from_be_bytes(buf),
            };

            Ok(v)
        }
        .boxed()
    }
}

#[cfg(test)]
mod test {

    use futures_util::io::Cursor;

    use super::{Binr, ByteOrder};

    #[test]
    pub fn test_read_le_u16() {
        let expected: u16 = 0x0102;
        let reader = Cursor::new(expected.to_le_bytes().to_vec());
        let mut reader = Binr::new(ByteOrder::Little, reader);
        futures_executor::block_on(async move {
            let actual = reader.read_u16().await.unwrap();
            assert_eq!(actual, expected);
        });
    }
    #[test]
    pub fn test_read_le_u32() {
        let expected: u32 = 0x01020304;
        let reader = Cursor::new(expected.to_le_bytes().to_vec());
        let mut reader = Binr::new(ByteOrder::Little, reader);
        futures_executor::block_on(async move {
            let actual = reader.read_u32().await.unwrap();
            assert_eq!(actual, expected);
        });
    }
    #[test]
    pub fn test_read_le_u64() {
        let expected: u64 = 0x01020304060708;
        let reader = Cursor::new(expected.to_le_bytes().to_vec());
        let mut reader = Binr::new(ByteOrder::Little, reader);
        futures_executor::block_on(async move {
            let actual = reader.read_u64().await.unwrap();
            assert_eq!(actual, expected);
        });
    }

    #[test]
    pub fn test_read_string_exact() {
        let expected = "test";
        let mut buf = expected.as_bytes().to_vec();
        for x in 0..8 {
            buf.push(x);
        }

        let reader = Cursor::new(buf);
        let mut reader = Binr::new(ByteOrder::Little, reader);
        futures_executor::block_on(async move {
            let actual = reader.read_string(4).await.unwrap();
            assert_eq!(actual, expected);
        });
    }
}
