//! Feels stupid to have to make this but I have found no other such object
//! In simplest terms:  "Read normally but return end of file after this many bytes read"
//! A slice of sorts - maybe that is a better name

use super::fs::ArchiveFileReadTraits;
use futures_util::AsyncRead;
use std::pin::Pin;

/// A struct that can be used to reduce a streams length
pub struct WindowedReader<R: ArchiveFileReadTraits> {
    inner: R,
    cursor: u64,
    length: u64,
}

impl<R: ArchiveFileReadTraits> WindowedReader<R> {
    /// Create a  new shadowed reader from existing, starting from current position with a max length
    pub fn new(inner: R, length: u64) -> Self {
        Self {
            inner,
            length,
            cursor: 0,
        }
    }
}
/*
impl Read for ShadowedReader<R> {
    fn read(&mut self, mut buf: &mut [u8]) -> std::io::Result<usize> {
        if self.cursor >= self.length {
            return Err(std::io::Error::new(
                ErrorKind::UnexpectedEof,
                "failed to fill whole buffer",
            ));
        }

        let remaining = self.length - self.cursor;
        if buf.len() > remaining as usize {
            buf = &mut buf[0..remaining];
        }

        let s = self.inner.read(buf)?;
        self.cursor += s;
        return Ok(s);
    }
}
*/

impl<R: ArchiveFileReadTraits> AsyncRead for WindowedReader<R> {
    fn poll_read(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        mut buf: &mut [u8],
    ) -> std::task::Poll<std::io::Result<usize>> {
        if self.cursor >= self.length {
            return std::task::Poll::Ready(Err(std::io::Error::new(
                std::io::ErrorKind::UnexpectedEof,
                "failed to fill whole buffer",
            )));
        }

        let remaining = (self.length - self.cursor) as usize;
        if buf.len() > remaining {
            buf = &mut buf[0..remaining];
        }

        let inner = Pin::new(&mut self.get_mut().inner);
        AsyncRead::poll_read(inner, cx, buf)
    }
}
