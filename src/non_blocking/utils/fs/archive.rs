use super::FsFileProvider;
use crate::{
    non_blocking::{streams::ArchiveEntryStream, AsyncArchiveReader},
    result::{ArchiveError, ArchiveResult},
    ArchiveManifest,
};
use effitfs::{DirEntry, Metadata};
use futures_util::{future::BoxFuture, io::Cursor, AsyncSeek, FutureExt};
use std::time::SystemTime;

/// Read-only archive-file backed fs provider
#[derive(Clone)]
pub struct ArchiveFileReadOnlyFsProvider<FS>
where
    FS: FsFileProvider,
    FS::R: AsyncSeek,
{
    archive_file: ArchiveManifest,
    underlying_fs: FS,
}

impl<FS> ArchiveFileReadOnlyFsProvider<FS>
where
    FS: FsFileProvider,
    FS::R: AsyncSeek,
{
    /// Createa  new archive file fs provider
    pub fn new(archive_file: ArchiveManifest, fs: FS) -> Self {
        Self {
            archive_file,
            underlying_fs: fs,
        }
    }
}

impl<FS> FsFileProvider for ArchiveFileReadOnlyFsProvider<FS>
where
    FS: FsFileProvider,
    FS::R: AsyncSeek,
{
    type R = ArchiveEntryStream<FS::R>;
    type W<'a>
        = Cursor<&'a mut [u8]>
    where
        Self: 'a;

    fn open_file<'a>(&'a self, path: &str) -> BoxFuture<'a, ArchiveResult<Self::R>>
    where
        Self: 'a,
    {
        let entry = self.archive_file.get_entry_by_name(path);
        async move {
            let entry = entry?;
            let reader = AsyncArchiveReader::new(&self.archive_file, &self.underlying_fs);
            let reader = reader.open_entry(entry).await?;
            Ok(reader)
        }
        .boxed()
    }

    fn create_file(&mut self, _: String) -> BoxFuture<'_, ArchiveResult<Self::W<'_>>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn lchmod(&mut self, _path: String, _mode: u32) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn lchown(
        &mut self,
        _: String,
        _: Option<u32>,
        _: Option<u32>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn chmod(&mut self, _path: String, _mode: u32) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn chown(&mut self, _: String, _: Option<u32>, _: Option<u32>) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn ltouch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn touch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn rm_all(&mut self, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            let var_name = "ArchiveFileReadOnlyFsProvider does not support writes";
            Err(ArchiveError::Unsupported(var_name.to_string()))
        }
        .boxed()
    }

    fn mkdir_all(&mut self, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn create_symlink(&mut self, _: String, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ArchiveFileReadOnlyFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn get_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>> {
        let path = path.to_owned();
        async move {
            let entry = self.archive_file.get_entry_by_name(&path)?;
            let metadata = entry.get_metadata();
            Ok(metadata.clone())
        }
        .boxed()
    }

    fn get_symlink_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>> {
        self.get_metadata(path)
    }

    fn read_dir(&self, target_path: String) -> BoxFuture<ArchiveResult<Vec<effitfs::DirEntry>>> {
        async move {
            let mut entries = vec![];
            let target_path = effitfs::PathBuf::from(target_path);
            for e in self.archive_file.get_entries() {
                let entry_path = effitfs::PathBuf::from(e.get_name());
                let entry_dir = entry_path.directory();
                if entry_dir != target_path {
                    continue;
                }
                let metadata = e.get_metadata().clone();
                let entry = DirEntry::new(entry_path, metadata);
                entries.push(entry);
            }
            Ok(entries)
        }
        .boxed()
    }
}

#[cfg(test)]
mod tests {

    use futures_util::AsyncReadExt;

    use crate::non_blocking::{
        tests::bytesfs_provider::ReadOnlyByteFsProvider,
        utils::fs::{ArchiveFileReadOnlyFsProvider, FsFileProvider},
        AsyncArchiveReader,
    };

    #[test]
    pub fn test_readonly_archivefile_provider() {
        futures_executor::block_on(async move {
            let file_path = "test_files/tar/test.tar.gz";
            let fs_provider = ReadOnlyByteFsProvider::load_file(file_path).unwrap();

            let base_archive = AsyncArchiveReader::read_manifest(file_path, &fs_provider)
                .await
                .unwrap();
            let archive_fs = ArchiveFileReadOnlyFsProvider::new(base_archive, fs_provider);

            let mut reader = archive_fs.open_file("test/test.txt").await.unwrap();
            let mut buf = vec![];
            reader.read_to_end(&mut buf).await.unwrap();
            assert_ne!(&buf, &[]);
        });
    }
}
