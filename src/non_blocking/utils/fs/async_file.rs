use super::FsFileProvider;
use crate::result::{ArchiveError, ArchiveResult};
use effitfs::{Metadata, PathBuf};
use filetime_creation::FileTime;
use futures_util::{future::BoxFuture, FutureExt, StreamExt};
use std::time::SystemTime;

/// A convenience implementation, by no means does it need to be used
#[derive(Default, Clone)]
pub struct AsyncFsFileProvider {}

impl AsyncFsFileProvider {
    async fn convert_metadata(
        &self,
        path: &str,
        metadata: std::fs::Metadata,
    ) -> ArchiveResult<effitfs::Metadata> {
        let link_target = if metadata.is_symlink() {
            async_fs::read_link(path)
                .await
                .ok()
                .map(|s| PathBuf::from(s).to_string())
        } else {
            None
        };
        let mut metadata: effitfs::Metadata = Metadata::from(metadata);
        metadata.set_link_target(link_target);

        Ok(metadata)
    }
}

/// A convenience type for using the async-fs provider
impl FsFileProvider for AsyncFsFileProvider {
    type R = async_fs::File;
    type W<'a> = async_fs::File;

    fn get_symlink_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>> {
        async move {
            let metadata: std::fs::Metadata = async_fs::symlink_metadata(&path).await?;
            let m = self.convert_metadata(&path, metadata).await?;

            Ok(m)
        }
        .boxed()
    }

    fn get_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>> {
        async move {
            let metadata: std::fs::Metadata = async_fs::metadata(&path).await?;
            let m = self.convert_metadata(&path, metadata).await?;

            Ok(m)
        }
        .boxed()
    }

    fn open_file<'a>(&'a self, path: &'a str) -> BoxFuture<'a, ArchiveResult<Self::R>>
    where
        Self: 'a,
    {
        let path = path.to_owned();
        async move {
            let f = async_fs::File::open(path).await?;
            Ok(f)
        }
        .boxed()
    }

    fn create_file(&mut self, path: String) -> BoxFuture<'_, ArchiveResult<Self::W<'_>>> {
        let path = path.to_owned();
        async move {
            let f = async_fs::File::create(path).await?;
            Ok(f)
        }
        .boxed()
    }

    #[cfg(target_os = "windows")]
    fn chmod(&mut self, _path: String, _unix_mode: u32) -> BoxFuture<'_, ArchiveResult<()>> {
        async move { Ok(()) }.boxed()
    }

    #[cfg(target_os = "windows")]
    fn lchmod(&mut self, _path: String, _unix_mode: u32) -> BoxFuture<'_, ArchiveResult<()>> {
        async move { Ok(()) }.boxed()
    }

    #[cfg(not(target_os = "windows"))]
    fn lchmod<'a>(&'a mut self, path: String, unix_mode: u32) -> BoxFuture<'a, ArchiveResult<()>> {
        use async_fs::unix::PermissionsExt;

        async move {
            let path = std::path::PathBuf::from(path);
            let meta = async_fs::symlink_metadata(path).await?;
            let mut perms = meta.permissions();
            perms.set_mode(unix_mode);
            Ok(())
        }
        .boxed()
    }
    #[cfg(not(target_os = "windows"))]
    fn chmod<'a>(&'a mut self, path: String, unix_mode: u32) -> BoxFuture<'a, ArchiveResult<()>> {
        use async_fs::unix::PermissionsExt;

        async move {
            let path = std::path::PathBuf::from(path);
            let meta = async_fs::metadata(path).await?;

            //no way this works...
            let mut perms = meta.permissions();
            perms.set_mode(unix_mode);
            Ok(())
        }
        .boxed()
    }

    #[cfg(target_os = "windows")]
    fn lchown(
        &mut self,
        _path: String,
        _uid: Option<u32>,
        _gid: Option<u32>,
    ) -> BoxFuture<'_, ArchiveResult<()>> {
        async move { Ok(()) }.boxed()
    }
    #[cfg(target_os = "windows")]
    fn chown(
        &mut self,
        _path: String,
        _uid: Option<u32>,
        _gid: Option<u32>,
    ) -> BoxFuture<'_, ArchiveResult<()>> {
        async move { Ok(()) }.boxed()
    }
    #[cfg(not(target_os = "windows"))]
    fn lchown<'a>(
        &'a mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> BoxFuture<'a, ArchiveResult<()>> {
        async move {
            // Boooo
            std::os::unix::fs::lchown(path, uid, gid)?;
            Ok(())
        }
        .boxed()
    }

    #[cfg(not(target_os = "windows"))]
    fn chown<'a>(
        &'a mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> BoxFuture<'a, ArchiveResult<()>> {
        async move {
            std::os::unix::fs::chown(path, uid, gid)?;
            Ok(())
        }
        .boxed()
    }

    fn ltouch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> BoxFuture<'_, ArchiveResult<()>> {
        async move {
            let path = path;
            let now = SystemTime::now();

            match filetime_creation::set_symlink_file_times(
                &path,
                FileTime::from_system_time(atime.unwrap_or(now)),
                FileTime::from_system_time(mtime.unwrap_or(now)),
                FileTime::from_system_time(ctime.unwrap_or(now)),
            ) {
                Ok(_) => {}
                Err(e) => {
                    return Err(ArchiveError::Other(format!(
                        "Error setting file time: {}",
                        e
                    )))
                }
            };
            Ok(())
        }
        .boxed()
    }

    fn touch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> BoxFuture<'_, ArchiveResult<()>> {
        async move {
            let path = path;
            let now = SystemTime::now();

            match filetime_creation::set_file_times(
                &path,
                FileTime::from_system_time(atime.unwrap_or(now)),
                FileTime::from_system_time(mtime.unwrap_or(now)),
                FileTime::from_system_time(ctime.unwrap_or(now)),
            ) {
                Ok(_) => {}
                Err(e) => {
                    return Err(ArchiveError::Other(format!(
                        "Error setting file time: {}",
                        e
                    )))
                }
            };
            Ok(())
        }
        .boxed()
    }

    fn rm_all(&mut self, path: String) -> BoxFuture<'_, ArchiveResult<()>> {
        async move {
            let path = std::path::PathBuf::from(path);
            if path.exists() {
                if path.is_dir() {
                    std::fs::remove_dir_all(path)?;
                } else {
                    std::fs::remove_file(path)?;
                }
            }

            Ok(())
        }
        .boxed()
    }

    fn mkdir_all(&mut self, path: String) -> BoxFuture<'_, ArchiveResult<()>> {
        async move {
            let path = std::path::PathBuf::from(path);

            if !path.exists() {
                std::fs::create_dir_all(path)?;
            }
            Ok(())
        }
        .boxed()
    }

    #[cfg(not(target_os = "windows"))]
    fn create_symlink<'a>(
        &'a mut self,
        link_target: String,
        target_path: String,
    ) -> BoxFuture<'a, ArchiveResult<()>> {
        async move {
            async_fs::unix::symlink(link_target, target_path).await?;

            Ok(())
        }
        .boxed()
    }

    #[cfg(target_os = "windows")]
    fn create_symlink(
        &mut self,
        _link_target: String,
        _target_path: String,
    ) -> BoxFuture<'_, ArchiveResult<()>> {
        async move { Ok(()) }.boxed()
    }

    fn read_dir(&self, path: String) -> BoxFuture<ArchiveResult<Vec<effitfs::DirEntry>>> {
        async move {
            let mut v: Vec<effitfs::DirEntry> = vec![];
            let mut rd = async_fs::read_dir(path).await?;
            while let Some(Ok(d)) = rd.next().await {
                let path = effitfs::PathBuf::from(d.path());
                let metadata = d.metadata().await?;
                let metadata = Metadata::from(metadata);
                let d = effitfs::DirEntry::new(path, metadata);
                v.push(d);
            }
            Ok(v)
        }
        .boxed()
    }
}

#[cfg(test)]
pub mod tests {
    use crate::{
        non_blocking::utils::fs::{AsyncFsFileProvider, FsFileProvider},
        test_utils,
    };
    use effitfs::PathBuf;

    pub fn init_tests_dir(test_name: &str) -> PathBuf {
        let mut tests_path = test_utils::workspace_relative_path("target");
        tests_path.push_str("async_file_tests");
        tests_path.push_str(test_name);
        let std_path = std::path::PathBuf::from(&tests_path);
        if std_path.exists() {
            std::fs::remove_dir_all(&std_path).unwrap();
        }
        std::fs::create_dir_all(&std_path).unwrap();
        tests_path
    }

    #[cfg(not(target_os = "windows"))]
    #[test]
    pub fn test_symlink() {
        let mut symlink = init_tests_dir("create_symlink");
        let mut link_target = symlink.clone();

        link_target.push_str("file");
        symlink.push_str("symlink");

        std::fs::File::create(link_target.to_string()).unwrap();

        let mut fs = AsyncFsFileProvider::default();
        futures_executor::block_on(async {
            fs.create_symlink(link_target.to_string(), symlink.to_string())
                .await
                .unwrap();
        });

        // Make sure a symlink was actually created
        let std_symlink = std::path::PathBuf::from(&symlink);
        let std_link_target = std::path::PathBuf::from(&link_target);
        assert!(std_symlink.exists());
        assert!(std_symlink.is_symlink());

        // Make sure the link points to the actual link target
        let actual_path = std_symlink.canonicalize().unwrap();
        let expected_path = std_link_target.canonicalize().unwrap();
        assert_eq!(actual_path, expected_path);

        // Now test get_symlink_metadata because the support files already exist
        futures_executor::block_on(async {
            let a = fs.get_symlink_metadata(symlink.to_string()).await.unwrap();
            assert!(a.is_symlink());
            assert_eq!(a.link_target(), Some(&link_target.to_string()));
        });
    }

    #[test]
    pub fn test_create_file() {
        let mut target_file = init_tests_dir("create_file");
        target_file.push_str("test_create_file.file");

        let mut fs_provider = AsyncFsFileProvider::default();
        futures_executor::block_on(async {
            fs_provider
                .create_file(target_file.to_string())
                .await
                .unwrap();
        });

        let path = std::path::PathBuf::from(&target_file);
        assert!(path.exists());
        assert!(path.is_file());
    }

    #[test]
    pub fn test_open_file() {
        futures_executor::block_on(async move {
            let base_file_path = test_utils::workspace_relative_path("test_files/tar/test.tar.gz");

            let fs_provider = AsyncFsFileProvider::default();

            let test_bytes = fs_provider
                .open_file(&base_file_path.to_string())
                .await
                .unwrap();
            let m = test_bytes.metadata().await.unwrap();
            assert_ne!(m.len(), 0);
        });
    }
}
