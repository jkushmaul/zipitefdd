//! Utilities for fs

mod archive;
mod async_file;
mod effitfs_provider;

pub use archive::ArchiveFileReadOnlyFsProvider;
pub use async_file::AsyncFsFileProvider;
pub use effitfs_provider::EffitFsProvider;

use crate::result::ArchiveResult;
use effitfs::{DirEntry, Metadata};
use futures_util::{future::BoxFuture, AsyncRead, AsyncSeek, AsyncWrite};
use std::time::SystemTime;

/// A trait for provider to indicate its read type
pub trait ArchiveFileReadTraits: AsyncRead + Send + Unpin {}
impl<T> ArchiveFileReadTraits for T where T: AsyncRead + Send + Unpin {}

/// A trait for detecting archive file types
pub trait ArchiveFileReadSeekTraits: ArchiveFileReadTraits + AsyncSeek {}
impl<T> ArchiveFileReadSeekTraits for T where T: ArchiveFileReadTraits + AsyncSeek {}

/// A trait for provider to indicate its write type
pub trait ArchiveFileWriteTraits: AsyncWrite + Send + Unpin {}
impl<T> ArchiveFileWriteTraits for T where T: AsyncWrite + Send + Unpin {}

/// A trait used to interface with file system
pub trait FsFileProvider
where
    Self: Clone + Sync,
{
    /// The type when opening a file
    type R: ArchiveFileReadTraits;
    /// The type when writing a file
    type W<'a>: ArchiveFileWriteTraits
    where
        Self: 'a;

    /// Open a file for reading
    fn open_file<'a>(&'a self, path: &'a str) -> BoxFuture<'a, ArchiveResult<Self::R>>
    where
        Self: 'a;

    /// get metadata about path, following symlinks
    fn get_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>>;

    /// Get metadata without dereferencing symlinks
    fn get_symlink_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>>;

    /// Create and open a file for writing
    fn create_file(&mut self, path: String) -> BoxFuture<'_, ArchiveResult<Self::W<'_>>>;

    /// Change file mode without dereferencing symbolic link
    fn lchmod(&mut self, path: String, mode: u32) -> BoxFuture<ArchiveResult<()>>;

    /// Change file mode
    fn chmod(&mut self, path: String, mode: u32) -> BoxFuture<ArchiveResult<()>>;

    /// Change file owner without dereferencing symbolic link
    fn lchown(
        &mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> BoxFuture<ArchiveResult<()>>;

    /// Change file owner
    fn chown(
        &mut self,
        path: String,
        uid: Option<u32>,
        gid: Option<u32>,
    ) -> BoxFuture<ArchiveResult<()>>;

    /// Update file times without dereferencing symbolic link
    fn ltouch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>>;

    /// Update file times
    fn touch(
        &mut self,
        path: String,
        atime: Option<SystemTime>,
        mtime: Option<SystemTime>,
        ctime: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>>;

    /// rm -rf
    fn rm_all(&mut self, path: String) -> BoxFuture<ArchiveResult<()>>;

    /// mkdir -p
    fn mkdir_all(&mut self, path: String) -> BoxFuture<ArchiveResult<()>>;

    /// ln -s
    fn create_symlink(
        &mut self,
        link_target: String,
        target_path: String,
    ) -> BoxFuture<ArchiveResult<()>>;

    /// ls -l
    fn read_dir(&self, path: String) -> BoxFuture<ArchiveResult<Vec<DirEntry>>>;
}
