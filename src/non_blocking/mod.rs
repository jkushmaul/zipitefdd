#![warn(rustdoc::broken_intra_doc_links)]
#![warn(missing_docs)]
#![warn(rustdoc::missing_crate_level_docs)]
#![warn(rustdoc::private_doc_tests)]
#![warn(rustdoc::invalid_codeblock_attributes)]
#![warn(rustdoc::bare_urls)]
#![warn(rustdoc::invalid_html_tags)]

//! Zipity F DooDah
//! Async archive file library
pub mod streams;
pub mod utils;

mod archive_file_types;
pub use archive_file_types::AsyncArchiveReader;

#[cfg(test)]
mod tests;
