use crate::{non_blocking::utils::fs::ArchiveFileReadTraits, CompressionCodec};
use async_compression::futures::bufread::{BzDecoder, DeflateDecoder, GzipDecoder, XzDecoder};
use futures_util::{AsyncBufRead, AsyncRead};
use std::pin::Pin;

/// all types of supported decompression
pub enum DecompressStream<R: ArchiveFileReadTraits + AsyncBufRead> {
    /// Gz
    Gz(GzipDecoder<R>),
    /// Xz
    Xz(XzDecoder<R>),
    /// Bz2
    Bz2(BzDecoder<R>),
    /// Deflate
    Deflate(DeflateDecoder<R>),
    /// Uncompressed
    Uncompressed(R),
}

impl<R: ArchiveFileReadTraits + AsyncBufRead> DecompressStream<R> {
    /// Construct a new DecompressStream
    pub fn new(file_type: CompressionCodec, reader: R) -> Self {
        match file_type {
            CompressionCodec::STORE => Self::Uncompressed(reader),
            CompressionCodec::GZIP => Self::Gz(GzipDecoder::new(reader)),
            CompressionCodec::XZ => Self::Xz(XzDecoder::new(reader)),
            CompressionCodec::BZIP2 => Self::Bz2(BzDecoder::new(reader)),
            CompressionCodec::DEFLATE => Self::Deflate(DeflateDecoder::new(reader)),
        }
    }
}

impl<R: ArchiveFileReadTraits + AsyncBufRead> AsyncRead for DecompressStream<R> {
    /// I feel dumb.  There has got to be a more idomatic way to do this.
    /// "All these enums - they are all AsyncRead - so "propagate" to them
    fn poll_read(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut [u8],
    ) -> std::task::Poll<std::io::Result<usize>> {
        match self.get_mut() {
            DecompressStream::Gz(r) => {
                let p = Pin::new(r);
                p.poll_read(cx, buf)
            }
            DecompressStream::Xz(r) => {
                let p = Pin::new(r);
                p.poll_read(cx, buf)
            }
            DecompressStream::Bz2(r) => {
                let p = Pin::new(r);
                p.poll_read(cx, buf)
            }

            DecompressStream::Deflate(r) => {
                let p = Pin::new(r);
                p.poll_read(cx, buf)
            }
            DecompressStream::Uncompressed(r) => {
                let p = Pin::new(r);
                p.poll_read(cx, buf)
            }
        }
    }
}
