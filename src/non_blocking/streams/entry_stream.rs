//! Just an enum to represent entry streams

use super::DecompressStream;
use crate::non_blocking::utils::{fs::ArchiveFileReadTraits, windowed_reader::WindowedReader};
use futures_util::{io::BufReader, AsyncRead};
use std::pin::Pin;

/// A type for all possible entry outputs
pub enum ArchiveEntryStream<R: ArchiveFileReadTraits> {
    /// Just a raw file
    Uncompressed(R),
    /// No entries, just a compressed (or uncomrpessed) file
    CompressedFile(DecompressStream<BufReader<R>>),
    /// A tar file, contained within a compressed file
    Tar(WindowedReader<DecompressStream<BufReader<R>>>),
    /// A zip file, with compressed entries
    Zip(WindowedReader<DecompressStream<BufReader<R>>>),
}

impl<R: ArchiveFileReadTraits> AsyncRead for ArchiveEntryStream<R> {
    fn poll_read(
        self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut [u8],
    ) -> std::task::Poll<std::io::Result<usize>> {
        match self.get_mut() {
            ArchiveEntryStream::CompressedFile(f) => {
                let f = Pin::new(f);
                f.poll_read(cx, buf)
            }
            ArchiveEntryStream::Tar(f) => {
                let f = Pin::new(f);
                f.poll_read(cx, buf)
            }
            ArchiveEntryStream::Zip(f) => {
                let f = Pin::new(f);
                f.poll_read(cx, buf)
            }
            ArchiveEntryStream::Uncompressed(f) => {
                let f = Pin::new(f);
                f.poll_read(cx, buf)
            }
        }
    }
}
