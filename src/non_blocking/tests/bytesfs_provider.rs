use crate::non_blocking::utils::fs::FsFileProvider;
use crate::result::{ArchiveError, ArchiveResult};
use crate::test_utils;
use effitfs::{DirEntry, Metadata, PathBuf, Permissions};
use futures_util::{future::BoxFuture, io::Cursor, AsyncReadExt, FutureExt};
use std::{io, time::SystemTime};

/// Convenience provider for raw bytes
/// Originally this would allow sharing but made it difficult to use a fn to return one when it creates the vec.
#[derive(Clone)]
pub struct ReadOnlyByteFsProvider {
    path: PathBuf,
    data: Vec<u8>,
    metadata: Metadata,
}

impl ReadOnlyByteFsProvider {
    /// Loads a file into memory
    pub fn load_file(value: &str) -> ArchiveResult<Self> {
        let file = test_utils::workspace_relative_path(value);
        let data = test_utils::read_file(&file.to_string())?;
        let fs = ReadOnlyByteFsProvider::new(file, data);
        Ok(fs)
    }

    /// Construct a new fs provider
    pub fn new(file_path: PathBuf, data: Vec<u8>) -> Self {
        let metadata = Metadata::new(
            effitfs::FileType::File,
            None,
            None,
            None,
            Permissions::default(),
            0,
            None,
        );
        Self {
            path: file_path,
            data,
            metadata,
        }
    }

    /// Pathn ot found
    pub fn not_found(path: PathBuf) -> io::Error {
        io::Error::new(io::ErrorKind::NotFound, path.to_string())
    }
}

impl FsFileProvider for ReadOnlyByteFsProvider {
    type R = Cursor<Vec<u8>>;
    type W<'a>
        = Cursor<&'a mut [u8]>
    where
        Self: 'a;

    fn open_file<'a>(&'a self, _: &'a str) -> BoxFuture<'a, ArchiveResult<Self::R>>
    where
        Self: 'a,
    {
        async move { Ok(Cursor::new(self.data.clone())) }.boxed()
    }

    fn create_file(&mut self, _: String) -> BoxFuture<'_, ArchiveResult<Self::W<'_>>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn chmod(&mut self, _path: String, _mode: u32) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn chown(&mut self, _: String, _: Option<u32>, _: Option<u32>) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }
    fn lchmod(&mut self, _path: String, _mode: u32) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn lchown(
        &mut self,
        _: String,
        _: Option<u32>,
        _: Option<u32>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }
    fn ltouch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }
    fn touch(
        &mut self,
        _: String,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
        _: Option<SystemTime>,
    ) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn rm_all(&mut self, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn mkdir_all(&mut self, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn create_symlink(&mut self, _: String, _: String) -> BoxFuture<ArchiveResult<()>> {
        async move {
            Err(ArchiveError::Unsupported(
                "ReadOnlyByteFsProvider does not support writes".to_string(),
            ))
        }
        .boxed()
    }

    fn get_metadata(&self, _: String) -> BoxFuture<ArchiveResult<Metadata>> {
        async move { Ok(self.metadata.clone()) }.boxed()
    }

    fn get_symlink_metadata(&self, path: String) -> BoxFuture<ArchiveResult<Metadata>> {
        self.get_metadata(path)
    }

    fn read_dir(&self, path: String) -> BoxFuture<ArchiveResult<Vec<effitfs::DirEntry>>> {
        async move {
            let path = PathBuf::from(path);
            if self.path.directory() != path {
                return Err(Self::not_found(path))?;
            }
            let metadata = self.metadata.clone();
            let entry = DirEntry::new(path, metadata);
            Ok(vec![entry])
        }
        .boxed()
    }
}

#[test]
fn test_readonly_bytes_provider() {
    futures_executor::block_on(async move {
        let file_path = "test_files/tar/test.tar.gz";
        let file_path = test_utils::workspace_relative_path(file_path);

        let buf = test_utils::read_file(&file_path.to_string()).unwrap();

        let bytes_fs = ReadOnlyByteFsProvider::new(file_path.clone(), buf.clone());

        let mut reader = bytes_fs.open_file(&file_path.to_string()).await.unwrap();
        let mut newbuf: Vec<u8> = vec![];
        reader.read_to_end(&mut newbuf).await.unwrap();
        assert_eq!(buf, newbuf);
    });
}
