use crate::CompressionCodec;
use effitfs::{FileType, Metadata, PathBuf, Permissions};
use std::time::{Duration, SystemTime};

/// Represents a single entry in an archive, it's metadata, and extraction data.
#[derive(Clone, Debug, PartialEq)]
pub struct ArchiveEntry {
    entry_name: String,
    entry_index: usize,
    compression_codec: CompressionCodec,
    compressed_offset: u64,
    compressed_size: usize,
    uncompressed_crc: u64,
    metadata: Metadata,
}

impl ArchiveEntry {
    /// Construct  a new archive entry
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        entry_name: String,
        entry_index: usize,
        compression_codec: CompressionCodec,
        compressed_offset: u64,
        compressed_size: usize,
        uncompressed_size: usize,
        uncompressed_crc: u64,
        unix_mode: u32,
        file_type: FileType,
        link_target: Option<String>,
    ) -> Self {
        let metadata = Metadata::new(
            file_type,
            None,
            None,
            None,
            Permissions::new(None, None, unix_mode),
            uncompressed_size as u64,
            link_target,
        );

        Self {
            entry_name,
            entry_index,
            compression_codec,
            compressed_offset,
            compressed_size,
            uncompressed_crc,
            metadata,
        }
    }

    /// Set access time
    pub fn set_atime(&mut self, t: Option<i64>) {
        let t = t.map(|t| SystemTime::UNIX_EPOCH + Duration::from_secs(t as u64));

        self.metadata.set_accessed(t);
    }

    /// Set creation time
    pub fn set_ctime(&mut self, t: Option<i64>) {
        let t = t.map(|t| SystemTime::UNIX_EPOCH + Duration::from_secs(t as u64));
        self.metadata.set_created(t);
    }

    /// Set modification time
    pub fn set_mtime(&mut self, t: Option<i64>) {
        let t = t.map(|t| SystemTime::UNIX_EPOCH + Duration::from_secs(t as u64));
        self.metadata.set_modified(t);
    }

    /// Set user id
    pub fn set_uid(&mut self, u: Option<u32>) {
        let mut permissions = self.metadata.permissions();
        permissions.set_uid(u);
        self.metadata.set_permissions(permissions);
    }

    /// Set group id
    pub fn set_gid(&mut self, g: Option<u32>) {
        let mut permissions = self.metadata.permissions();
        permissions.set_gid(g);
        self.metadata.set_permissions(permissions);
    }

    /// Appends a path to the link target
    pub fn append_linkpath(&mut self, n: &str) {
        match self.metadata.link_target() {
            Some(s) => {
                let mut path = PathBuf::from(s.as_str());
                path.push_str(n);
                self.metadata.set_link_target(Some(path.to_string()));
            }
            None => self.metadata.set_link_target(Some(n.to_owned())),
        };
    }

    /// Appends to the name
    pub fn append_name(&mut self, n: &str) {
        self.entry_name.push_str(n);
    }

    /// Get the uncompressed size
    pub fn get_uncompressed_size(&self) -> usize {
        self.metadata.len() as usize
    }

    /// Get the file type
    pub fn get_file_type(&self) -> FileType {
        self.metadata.file_type()
    }

    /// Get the name
    pub fn get_name(&self) -> &str {
        &self.entry_name
    }
    /// Get the index
    pub fn get_index(&self) -> usize {
        self.entry_index
    }
    /// Get metadata
    pub fn get_metadata(&self) -> &Metadata {
        &self.metadata
    }
    /// get metadata mut
    pub fn metadata_mut(&mut self) -> &mut Metadata {
        &mut self.metadata
    }

    /// Get compressed size
    pub fn get_compressed_size(&self) -> usize {
        self.compressed_size
    }

    /// Get compressed offset
    pub fn get_compressed_offset(&self) -> u64 {
        self.compressed_offset
    }

    /// Get uncompressed crc
    pub fn get_uncompressed_crc(&self) -> u64 {
        self.uncompressed_crc
    }

    /// Get compression codec
    pub fn get_compression_codec(&self) -> CompressionCodec {
        self.compression_codec
    }
}

#[cfg(test)]
mod test {

    use super::ArchiveEntry;
    use crate::CompressionCodec;
    use effitfs::FileType;

    #[test]
    pub fn test_clone() {
        let a = ArchiveEntry::new(
            String::from("entry_name"),
            1,
            CompressionCodec::BZIP2,
            2,
            3,
            4,
            5,
            6,
            FileType::Dir,
            Some(String::from("test")),
        );
        let b = a.clone();
        assert_eq!(a, b);
    }
}
