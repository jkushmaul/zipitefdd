use std::string::FromUtf8Error;
use thiserror::Error;

/// Shortcut for typical archive result
pub type ArchiveResult<T> = std::result::Result<T, ArchiveError>;

/// Standard error types for archive operations
#[derive(Debug, Error)]
pub enum ArchiveError {
    /// error working with the files
    #[error(transparent)]
    Io(#[from] std::io::Error),
    /// All other error types
    #[error("Error: {0}")]
    Other(String),
    /// Mismatches in deserialization
    #[error("Invalid Data: {0}")]
    InvalidData(String),
    /// Signatures not found
    #[error("Signature not found: {0}")]
    SignatureNotFound(String),
    /// Invalid signature
    #[error("Invalid signature: {0}")]
    InvalidSignature(String),
    /// Invalid entry index
    #[error("Invalid entry index: {0}")]
    InvalidEntryIndex(usize),
    /// Invalid entry name
    #[error("Invalid entry: {0}")]
    InvalidEntryName(String),
    /// A link entry does not reference a valid regular/dir entry name
    #[error("Invalid link target: {0}")]
    InvalidLinkTarget(String),
    /// An entry has an invalid compression method
    #[error("Compression type not detected")]
    UnidentifiedCompressionmethod,
    #[error("Unsupported file type: {0}")]
    /// An archive file type is not supported
    UnsupportedFileType(String),
    #[error("CRC mismatch: {0}")]
    /// CRC of an entry was invalid
    CrcMismatch(String),
    /// Feature is not supported
    #[error("Unsupported: {0}")]
    Unsupported(String),
    /// Error encoding a string
    #[error("UTF8 Error: {0}")]
    FromUtf8Error(#[from] FromUtf8Error),
    /// Something bad with SystemTime
    #[error("System Time Error: {0}")]
    SystemTimeError(#[from] std::time::SystemTimeError),
    /// Dir,File(regular), and SymLink are the only types supported
    #[error("The entry has unsupported file type bits: {0:#o}")]
    UnsupportedEntryFileType(u16),
    #[error("Error in data structure")]
    BinRw(#[from] binrw::Error),
}
