pub mod zip32;

use effitfs::PathBuf;
use std::io::Read;

use super::result::ArchiveResult;

pub fn workspace_relative_path(cargo_manifest_relative: &str) -> PathBuf {
    let s = env!("CARGO_MANIFEST_DIR");
    // the crates/<crate> root
    let mut manifest_dir = PathBuf::from(s);
    // the path relative to workspace dir
    manifest_dir.push_str(cargo_manifest_relative);

    manifest_dir
}

pub fn read_file(p: &str) -> ArchiveResult<Vec<u8>> {
    let mut f = std::fs::File::open(p)?;
    let mut buf: Vec<u8> = vec![];
    f.read_to_end(&mut buf)?;
    Ok(buf)
}
