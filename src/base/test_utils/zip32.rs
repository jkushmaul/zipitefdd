//! In memory fs - load files from HashMap<String, &[u8]>  Probably only useful in testing single files

/*
On linux:

echo "it was just a test" > itwasjustatest.txt && zip itwasjustatest.zip itwasjustatest.txt && zipinfo itwasjustatest.zip && xxd -i itwasjustatest.zip

--
updating: itwasjustatest.txt (stored 0%)

Archive:  itwasjustatest.zip
Zip file size: 205 bytes, number of entries: 1
-rw-r--r--  3.0 unx       19 tx stor 24-Feb-17 13:02 itwasjustatest.txt
1 file, 19 bytes uncompressed, 19 bytes compressed:  0.0%

unsigned char itwasjustatest_zip[] = {
  0x50, 0x4b, 0x03, 0x04, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x68,
  0x51, 0x58, 0x46, 0x83, 0x2f, 0xac, 0x13, 0x00, 0x00, 0x00, 0x13, 0x00,
  0x00, 0x00, 0x12, 0x00, 0x1c, 0x00, 0x69, 0x74, 0x77, 0x61, 0x73, 0x6a,
  0x75, 0x73, 0x74, 0x61, 0x74, 0x65, 0x73, 0x74, 0x2e, 0x74, 0x78, 0x74,
  0x55, 0x54, 0x09, 0x00, 0x03, 0xb9, 0xf4, 0xd0, 0x65, 0xb6, 0xf4, 0xd0,
  0x65, 0x75, 0x78, 0x0b, 0x00, 0x01, 0x04, 0xe8, 0x03, 0x00, 0x00, 0x04,
  0xe8, 0x03, 0x00, 0x00, 0x69, 0x74, 0x20, 0x77, 0x61, 0x73, 0x20, 0x6a,
  0x75, 0x73, 0x74, 0x20, 0x61, 0x20, 0x74, 0x65, 0x73, 0x74, 0x0a, 0x50,
  0x4b, 0x01, 0x02, 0x1e, 0x03, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51,
  0x68, 0x51, 0x58, 0x46, 0x83, 0x2f, 0xac, 0x13, 0x00, 0x00, 0x00, 0x13,
  0x00, 0x00, 0x00, 0x12, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
  0x00, 0x00, 0x00, 0xa4, 0x81, 0x00, 0x00, 0x00, 0x00, 0x69, 0x74, 0x77,
  0x61, 0x73, 0x6a, 0x75, 0x73, 0x74, 0x61, 0x74, 0x65, 0x73, 0x74, 0x2e,
  0x74, 0x78, 0x74, 0x55, 0x54, 0x05, 0x00, 0x03, 0xb9, 0xf4, 0xd0, 0x65,
  0x75, 0x78, 0x0b, 0x00, 0x01, 0x04, 0xe8, 0x03, 0x00, 0x00, 0x04, 0xe8,
  0x03, 0x00, 0x00, 0x50, 0x4b, 0x05, 0x06, 0x00, 0x00, 0x00, 0x00, 0x01,
  0x00, 0x01, 0x00, 0x58, 0x00, 0x00, 0x00, 0x5f, 0x00, 0x00, 0x00, 0x00,
  0x00
};

*/
pub const IT_WAS_JUST_A_TEST_ZIP32: &[u8] = &[
    0x50, 0x4b, 0x03, 0x04, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x68, 0x51, 0x58, 0x46, 0x83,
    0x2f, 0xac, 0x13, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x12, 0x00, 0x1c, 0x00, 0x69, 0x74,
    0x77, 0x61, 0x73, 0x6a, 0x75, 0x73, 0x74, 0x61, 0x74, 0x65, 0x73, 0x74, 0x2e, 0x74, 0x78, 0x74,
    0x55, 0x54, 0x09, 0x00, 0x03, 0xb9, 0xf4, 0xd0, 0x65, 0xb6, 0xf4, 0xd0, 0x65, 0x75, 0x78, 0x0b,
    0x00, 0x01, 0x04, 0xe8, 0x03, 0x00, 0x00, 0x04, 0xe8, 0x03, 0x00, 0x00, 0x69, 0x74, 0x20, 0x77,
    0x61, 0x73, 0x20, 0x6a, 0x75, 0x73, 0x74, 0x20, 0x61, 0x20, 0x74, 0x65, 0x73, 0x74, 0x0a, 0x50,
    0x4b, 0x01, 0x02, 0x1e, 0x03, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x51, 0x68, 0x51, 0x58, 0x46,
    0x83, 0x2f, 0xac, 0x13, 0x00, 0x00, 0x00, 0x13, 0x00, 0x00, 0x00, 0x12, 0x00, 0x18, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0xa4, 0x81, 0x00, 0x00, 0x00, 0x00, 0x69, 0x74, 0x77,
    0x61, 0x73, 0x6a, 0x75, 0x73, 0x74, 0x61, 0x74, 0x65, 0x73, 0x74, 0x2e, 0x74, 0x78, 0x74, 0x55,
    0x54, 0x05, 0x00, 0x03, 0xb9, 0xf4, 0xd0, 0x65, 0x75, 0x78, 0x0b, 0x00, 0x01, 0x04, 0xe8, 0x03,
    0x00, 0x00, 0x04, 0xe8, 0x03, 0x00, 0x00, 0x50, 0x4b, 0x05, 0x06, 0x00, 0x00, 0x00, 0x00, 0x01,
    0x00, 0x01, 0x00, 0x58, 0x00, 0x00, 0x00, 0x5f, 0x00, 0x00, 0x00, 0x00, 0x00,
];
pub const IT_WAS_JUST_A_TEST_EOCD: u64 = 0xb7; // the last 'PK' in the bytes above... I could probably just rfind this.
pub const IT_WAS_JUST_A_TEST_TEXT: &str = "it was just a test\n";
pub const IT_WAS_JUST_A_TEST_FILE: &str = "itwasjustatest.txt";

pub const TEST_EXPECTED: [u8; 5] = [b't', b'e', b's', b't', b'\n'];
