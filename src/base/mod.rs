#[cfg(test)]
pub mod test_utils;

mod archive_entry;
pub mod compressed;
mod compression_codec;
mod extract_options;

pub mod result;
pub mod tar;
pub mod utils;
pub mod zip;

pub use archive_entry::*;
pub use compression_codec::*;
pub use extract_options::*;

use indexmap::IndexMap;
use result::{ArchiveError, ArchiveResult};
use zip::ZipFileType;

/// Used to identify types of archive files
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ArchiveType {
    /// Pk Zip file
    Zip(ZipFileType),
    /// Tar file, possibly wrapped itself as a Compressed
    Tar(CompressionCodec),
    /// single file compressed, gz, xz, bz2
    Compressed(CompressionCodec),
}

/// An ArchiveManifest will be indexed once and re-opened for each extraction
pub struct ArchiveManifest {
    file_path: String,
    archive_type: ArchiveType,
    entries: IndexMap<String, ArchiveEntry>,
}

impl Clone for ArchiveManifest {
    fn clone(&self) -> Self {
        Self {
            file_path: self.file_path.clone(),
            archive_type: self.archive_type,
            entries: self.entries.clone(),
        }
    }
}

//where
//    F: FsFileProvider,

impl ArchiveManifest {
    /// Create a new archive file with predefined entries
    pub fn new(
        file_path: String,
        archive_type: ArchiveType,
        raw_entries: Vec<ArchiveEntry>,
    ) -> Self {
        let mut entries = IndexMap::new();
        for e in raw_entries {
            entries.insert(e.get_name().to_string(), e);
        }
        Self {
            file_path,
            archive_type,
            entries,
        }
    }

    /// list entries
    pub fn get_entries(&self) -> impl Iterator<Item = &'_ ArchiveEntry> {
        self.entries.iter().map(|(_, v)| v)
    }

    /// get an entry by it's name
    pub fn get_entry_by_name(&self, name: &str) -> ArchiveResult<&ArchiveEntry> {
        match self.entries.get(name) {
            Some(s) => Ok(s),
            None => Err(ArchiveError::InvalidEntryName(name.to_owned())),
        }
    }

    /// get an entry by it's index
    pub fn get_entry_by_index(&self, index: usize) -> ArchiveResult<&ArchiveEntry> {
        match self.entries.get_index(index) {
            Some((_, v)) => Ok(v),
            None => Err(ArchiveError::InvalidEntryIndex(index)),
        }
    }

    /// Get the file path
    pub fn get_file_path(&self) -> &str {
        &self.file_path
    }

    /// Get the archive file type
    pub fn get_type(&self) -> ArchiveType {
        self.archive_type
    }
}

#[cfg(test)]
mod tests {
    use effitfs::FileType;

    use crate::{
        result::ArchiveError, ArchiveEntry, ArchiveManifest, ArchiveType, CompressionCodec,
    };

    #[test]
    pub fn test_get_entry_by_name_invalid() {
        let entry = ArchiveEntry::new(
            String::from("test"),
            0,
            CompressionCodec::STORE,
            1,
            2,
            3,
            4,
            5,
            FileType::File,
            None,
        );

        let entries = vec![entry];

        let a = ArchiveManifest::new(
            String::from("test"),
            ArchiveType::Compressed(CompressionCodec::STORE),
            entries,
        );

        let result = a.get_entry_by_name("test");
        assert!(result.is_ok());
        let result = a.get_entry_by_name("not_exist");
        let e = match result {
            Ok(_) => panic!("Should have failed"),
            Err(e) => e,
        };
        match e {
            ArchiveError::InvalidEntryName(_) => {}
            _ => panic!("Incorrect error"),
        }
    }
}
