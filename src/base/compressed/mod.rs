use constants::ZLIB_CINFO_32K_WINDOW;

use crate::CompressionCodec;

pub mod constants {
    pub const SUPPORTED_FILE_EXTENSIONS: &[&str] = &["gz", "xz", "bz2"];
    pub const MAGIC_GZ: [u8; 2] = [0x1F, 0x8B];
    pub const MAGIC_XZ: [u8; 6] = [0xFD, 0x37, 0x7A, 0x58, 0x5A, 0x00];
    pub const MAGIC_BZ2: [u8; 2] = [0x42, 0x5a];

    pub const ZLIB_CM_DEFLATE: u8 = 8;
    pub const ZLIB_CINFO_32K_WINDOW: u8 = 7;
}

impl From<[u8; 6]> for CompressionCodec {
    fn from(buf: [u8; 6]) -> Self {
        if buf.as_slice()[0..2] == constants::MAGIC_GZ {
            return Self::GZIP;
        } else if buf.as_slice()[0..6] == constants::MAGIC_XZ {
            return Self::XZ;
        } else if buf.as_slice()[0..2] == constants::MAGIC_BZ2 {
            return Self::BZIP2;
        }
        //Deflate, you nasty booger.. Couldnt' afford 4 bytes easy magic or something
        let cmf = buf[0];
        let flag = buf[1];
        let cm = cmf & 0x7;
        let cinfo = cmf & 0xF8 >> 0x7;

        let check_be = (cmf as u16) << 8 | (flag as u16);
        if check_be % 31 == 0 && cm == constants::ZLIB_CM_DEFLATE && cinfo == ZLIB_CINFO_32K_WINDOW
        {
            //  The FCHECK value must be such that CMF and FLG, when viewed as
            // a 16-bit unsigned integer stored in MSB order (CMF*256 + FLG),
            // is a multiple of 31.
            return CompressionCodec::DEFLATE;
        }

        CompressionCodec::STORE
    }
}
