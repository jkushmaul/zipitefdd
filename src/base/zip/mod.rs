pub mod bocd;
pub mod zip32;

// https://en.wikipedia.org/wiki/ZIP_(file_format)

use crate::CompressionCodec;
use strum::FromRepr;

use super::result::ArchiveError;

pub mod constants {
    pub const Z32_EOCD_SIG: u32 = 0x06054b50;
    pub const LOCALFILE_SIG: u32 = 0x04034b50;
    pub const BOCD_SIG: u32 = 0x02014b50;

    pub const SUPPORTED_FILE_EXTENSIONS: &[&str] = &["zip"];
    pub const MAGIC_PKZIP: [u8; 4] = [0x50, 0x4b, 0x03, 0x04];
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ZipFileType {
    Zip64,
    Zip32,
}

/// StatFlags which I don't know why I can't get this from effitfs
#[derive(Copy, Clone)]
#[repr(u16)]
#[allow(non_camel_case_types)]
pub enum StatFlags {
    /// Directory
    S_IFDIR = 0o040000, //16384, 0x4000
    /// Regular file
    S_IFREG = 0o100000, //32768, 0x8000
    /// A link
    S_IFLNK = 0o120000, //40960, 0xA000
}

impl StatFlags {
    const S_IFMT: u16 = 0o170000; //61440, 0xF000

    /// file_type_bits
    pub fn is_in(&self, file_type_bits: u16) -> bool {
        let s = *self as u16;
        (file_type_bits & Self::S_IFMT) == s
    }
}

/// File modes
#[repr(u16)]
pub enum ModeFlags {
    /// Set UID on execution
    SUID = 0o4000,
    /// Set GID on execution
    SGID = 0o2000,
    /// Reserved
    SVTX = 0o1000,
    /// Read for user
    RUSR = 0o0400,
    /// Write for user
    WUSR = 0o0200,
    /// Execute for user
    XUSR = 0o0100,
    /// Read for group
    RGRP = 0o0040,
    /// Write for group
    WGRP = 0o0020,
    /// Execute for group
    XGRP = 0o0010,
    /// Read for all
    ROTH = 0o0004,
    /// Write for all
    WOTH = 0o0002,
    /// Execute for all
    XOTH = 0o0001,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(FromRepr, Debug, PartialEq)]
#[repr(u16)]
pub enum ZipCompressionMethod {
    STORE = 0,
    LZW = 1,
    REDUCE1 = 2,
    REDUCE2 = 3,
    REDUCE3 = 4,
    REDUCE5 = 5,
    IMPLODE = 6,
    DEFLATE = 8,
    DEFLATE64 = 9,
    BZIP2 = 12,
    LZMA = 14,
    ZSTD = 93,
    XZ = 95,
    WAVPACK = 97,
    PPMD = 98,
    LZ77 = 99,
    /*

            0 - The file is stored (no compression)
            1 - The file is Shrunk
            2 - The file is Reduced with compression factor 1
            3 - The file is Reduced with compression factor 2
            4 - The file is Reduced with compression factor 3
            5 - The file is Reduced with compression factor 4
            6 - The file is Imploded
            7 - Reserved for Tokenizing compression algorithm
            8 - The file is Deflated
            9 - Enhanced Deflating using Deflate64(tm)
           10 - PKWARE Data Compression Library Imploding (old IBM TERSE)
           11 - Reserved by PKWARE
           12 - File is compressed using BZIP2 algorithm
           13 - Reserved by PKWARE
           14 - LZMA
           15 - Reserved by PKWARE
           16 - IBM z/OS CMPSC Compression
           17 - Reserved by PKWARE
           18 - File is compressed using IBM TERSE (new)
           19 - IBM LZ77 z Architecture
           20 - deprecated (use method 93 for zstd)
           93 - Zstandard (zstd) Compression
           94 - MP3 Compression
           95 - XZ Compression
           96 - JPEG variant
           97 - WavPack compressed data
           98 - PPMd version I, Rev 1
           99 - AE-x encryption marker (see APPENDIX E)
    */
}

impl TryFrom<ZipCompressionMethod> for CompressionCodec {
    type Error = ArchiveError;

    fn try_from(value: ZipCompressionMethod) -> Result<Self, Self::Error> {
        let compression_method = match value {
            ZipCompressionMethod::STORE => CompressionCodec::STORE,
            ZipCompressionMethod::DEFLATE => CompressionCodec::DEFLATE,
            ZipCompressionMethod::BZIP2 => CompressionCodec::BZIP2,
            _ => return Err(ArchiveError::UnidentifiedCompressionmethod),
        };

        Ok(compression_method)
    }
}

impl TryFrom<u16> for ZipCompressionMethod {
    type Error = ArchiveError;

    fn try_from(value: u16) -> Result<Self, Self::Error> {
        match ZipCompressionMethod::from_repr(value) {
            Some(a) => Ok(a),
            None => Err(ArchiveError::UnidentifiedCompressionmethod),
        }
    }
}
