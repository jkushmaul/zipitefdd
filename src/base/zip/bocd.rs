use binrw::{meta::ReadEndian, BinRead, BinReaderExt, Endian};

#[derive(Debug, Clone)]
pub struct CentralDirectoryHead {
    pub signature: u32, //0x02014b50
    pub version_created: u16,
    pub version_target: u16,
    pub general_purpose: u16,
    pub compression_method: u16,
    pub last_mod_time: u16,
    pub last_mod_date: u16,
    pub crc32_uncompressed: u32,
    pub compressed_size: u32,   // or 0xffffffff for zip64
    pub uncompressed_size: u32, // same
    pub filename_length: u16,
    pub extra_field_length: u16,
    pub file_comment_length: u16,
    pub disk_number: u16, // 0xffff for zip64
    pub internal_file_attributes: u16,
    pub external_file_attributes: u32,
    pub local_file_header_offset: u32, //  0xffffffff for zip64

    // of length: filename_length
    pub filename: String,

    // of length: extra_field_length
    pub extra_field: Vec<u8>,
    // of length: file_comment_length
    pub file_comment: Vec<u8>,
}

impl CentralDirectoryHead {
    pub const SIGNATURE: u32 = 0x02014b50;
}

impl ReadEndian for CentralDirectoryHead {
    const ENDIAN: binrw::meta::EndianKind = binrw::meta::EndianKind::Endian(Endian::Little);
}

impl BinRead for CentralDirectoryHead {
    type Args<'a> = ();

    fn read_options<R: std::io::Read + std::io::Seek>(
        parser: &mut R,
        _: Endian,
        _: Self::Args<'_>,
    ) -> binrw::BinResult<Self> {
        let signature: u32 = parser.read_le()?; //0x02014b50
        if signature != CentralDirectoryHead::SIGNATURE {
            let pos = parser.stream_position()?;
            return Err(binrw::Error::BadMagic {
                pos,
                found: Box::new(format!(
                    "Expected signature {} but found {}",
                    CentralDirectoryHead::SIGNATURE,
                    signature
                )),
            });
        }
        // I can't figure out how to use constants in derives - probably because, the constant doesn't exist yet... But that is kind of silly because it's just a macro...  More like the macro just written to expect
        // a literal, and didn't consider constants.

        let version_created: u16 = parser.read_le()?;
        let version_target: u16 = parser.read_le()?;
        let general_purpose: u16 = parser.read_le()?;
        let compression_method: u16 = parser.read_le()?;
        let last_mod_time: u16 = parser.read_le()?;
        let last_mod_date: u16 = parser.read_le()?;
        let crc32_uncompressed: u32 = parser.read_le()?;
        let compressed_size: u32 = parser.read_le()?; // or 0xffffffff for zip64
        let uncompressed_size: u32 = parser.read_le()?; // same
        let filename_length: u16 = parser.read_le()?;
        let extra_field_length: u16 = parser.read_le()?;
        let file_comment_length: u16 = parser.read_le()?;
        let disk_number: u16 = parser.read_le()?; // 0xffff for zip64
        let internal_file_attributes: u16 = parser.read_le()?;
        let external_file_attributes: u32 = parser.read_le()?;
        let local_file_header_offset: u32 = parser.read_le()?; //  0xffffffff for zip64

        let mut filename = vec![0u8; filename_length as usize];
        let mut extra_field: Vec<u8> = vec![0u8; extra_field_length as usize];
        let mut file_comment = vec![0u8; file_comment_length as usize];

        parser.read_exact(&mut filename)?;
        let i = filename
            .iter()
            .enumerate()
            .find(|(_, &b)| b == 0)
            .map(|a| a.0)
            .unwrap_or(filename_length as usize);
        parser.read_exact(&mut extra_field)?;
        parser.read_exact(&mut file_comment)?;

        let filename = String::from_utf8_lossy(&filename[0..i]).to_string();

        Ok(CentralDirectoryHead {
            signature,
            version_created,
            version_target,
            general_purpose,
            compression_method,
            last_mod_time,
            last_mod_date,
            crc32_uncompressed,
            compressed_size,
            uncompressed_size,
            filename_length,
            extra_field_length,
            file_comment_length,
            disk_number,
            internal_file_attributes,
            external_file_attributes,
            local_file_header_offset,
            filename,
            extra_field,
            file_comment,
        })
    }
}
