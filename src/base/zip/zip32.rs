use super::constants;
use super::{bocd::CentralDirectoryHead, StatFlags, ZipCompressionMethod};
use crate::result::{ArchiveError, ArchiveResult};
use crate::{utils::msdos::MsdosDateTime, ArchiveEntry, CompressionCodec};
use binrw::BinReaderExt;
use binrw::{meta::ReadEndian, BinRead, Endian};
use effitfs::FileType;

#[derive(BinRead, Clone)]
#[br(little)]
pub struct Zip32CentralDirectoryTail {
    //#[br(magic = 0x06054b50_u32)]
    pub signature: u32,                           //0x06054b50
    pub disk_index: u16,                          // or 0xffff for zip64
    pub central_directory_disk_start: u16,        //same
    pub central_directory_records_this_disk: u16, //same
    pub total_central_directory_records: u16,     //same
    pub central_directory_length: u32,            //     0xffffffff for zip64
    pub central_directory_offset: u32,            // same
    pub comment_length: u16,                      //

                                                  //#[br(count = usize::from(comment_length))]
                                                  //pub comment: Vec<u8>, // of length: comment_length
}
impl Zip32CentralDirectoryTail {
    pub const SIGNATURE: u32 = 0x06054b50;
}

pub struct LocalFileHeader {
    pub signature: u32, //0x04034b50
    pub min_version: u16,
    pub general_purpose_flags: u16,
    pub compression_method: u16,
    pub file_last_mod_time: u16,
    pub file_last_mod_date: u16,
    pub crc32_uncompressed: u32,
    pub compressed_size: u32,   // or zip64 0xffffffff
    pub uncompressed_size: u32, // same
    pub filename_length: u16,
    pub extra_field_length: u16,
    pub filename: String,
    pub extra_field: Vec<u8>,
}
impl ReadEndian for LocalFileHeader {
    const ENDIAN: binrw::meta::EndianKind = binrw::meta::EndianKind::Endian(Endian::Little);
}
impl BinRead for LocalFileHeader {
    type Args<'a> = ();

    fn read_options<R: std::io::Read + std::io::Seek>(
        parser: &mut R,
        _: Endian,
        _: Self::Args<'_>,
    ) -> binrw::BinResult<Self> {
        let signature: u32 = parser.read_le()?; //0x04034b50
        if signature != constants::LOCALFILE_SIG {
            let pos = parser.stream_position()?;
            return Err(binrw::error::Error::BadMagic {
                pos,
                found: Box::new(format!(
                    "Expected magic {:#x?}, but found {:#x?}",
                    constants::LOCALFILE_SIG,
                    signature
                )),
            });
        }
        let min_version: u16 = parser.read_le()?;
        let general_purpose_flags: u16 = parser.read_le()?;
        let compression_method: u16 = parser.read_le()?;
        // Given an archive with last mod: 2023-06-16 20:03:27.000000000  (@1686960207)
        // Per pkware APPNOTE 6.3.5 date and time encoded in standard ms-dos format (yay!)
        // file_last_mod_time: 41070
        // file_last_mod_date: 22224

        let file_last_mod_time: u16 = parser.read_le()?;
        let file_last_mod_date: u16 = parser.read_le()?;

        let crc32_uncompressed: u32 = parser.read_le()?;
        let compressed_size: u32 = parser.read_le()?; // or zip64 0xffffffff
        let uncompressed_size: u32 = parser.read_le()?; // same
        let filename_length: u16 = parser.read_le()?;
        let extra_field_length: u16 = parser.read_le()?;

        let mut filename = vec![0u8; filename_length as usize];
        parser.read_exact(&mut filename)?;

        let i = filename
            .iter()
            .enumerate()
            .find(|(_, &b)| b == 0)
            .map(|a| a.0)
            .unwrap_or(filename_length as usize);

        let filename = String::from_utf8_lossy(&filename[0..i]).to_string();

        let mut extra_field: Vec<u8> = vec![0u8; extra_field_length as usize];
        parser.read_exact(&mut extra_field)?;

        Ok(LocalFileHeader {
            signature,
            min_version,
            general_purpose_flags,
            compression_method,
            file_last_mod_time,
            file_last_mod_date,
            crc32_uncompressed,
            compressed_size,
            uncompressed_size,
            filename_length,
            extra_field_length,
            filename,
            extra_field,
        })
    }
}

impl LocalFileHeader {
    pub const SIGNATURE: u32 = 0x04034b50;

    pub fn into_archive_entry(
        self,
        offset: u64,
        index: usize,
        cd: &CentralDirectoryHead,
    ) -> ArchiveResult<ArchiveEntry> {
        let compression_codec = ZipCompressionMethod::try_from(self.compression_method)?;
        let compression_codec = CompressionCodec::try_from(compression_codec)?;

        /*
          the mode will be 0xffff & !(S_ISGID|S_ISUID|S_ISVTX)
        */

        let bits: u16 = (cd.external_file_attributes >> 16) as u16;
        let unix_mode_bits = bits;
        let file_type_bits = bits;

        let unix_mode = unix_mode_bits & 0o777;

        //What is 0x120000?
        let file_type = if StatFlags::S_IFLNK.is_in(file_type_bits) {
            FileType::SymLink
            //Need to read the entry data for this to get the actual link path and it doesn't fit with the way this was written...
        } else if StatFlags::S_IFDIR.is_in(file_type_bits)
            || self.filename.ends_with("/")
            || self.filename.ends_with("\\")
        {
            FileType::Dir
        } else if StatFlags::S_IFREG.is_in(file_type_bits) || file_type_bits == 0 {
            // Msdos file attributes 20 hex seen in wild, with no external attributes so I guess that means - if it isn't a dir.  and it isn't a symlink, it's a file.
            FileType::File
        } else {
            return Err(ArchiveError::UnsupportedEntryFileType(file_type_bits));
        };
        let msdos_date = MsdosDateTime::new(self.file_last_mod_date, self.file_last_mod_time);
        let last_mod = msdos_date.unix_epoch()?;

        let mut entry: ArchiveEntry = ArchiveEntry::new(
            self.filename,
            index,
            compression_codec,
            offset,
            self.compressed_size as usize,
            self.uncompressed_size as usize,
            self.crc32_uncompressed as u64,
            unix_mode as u32,
            file_type,
            None,
        );
        entry.set_mtime(Some(last_mod));

        Ok(entry)
    }
}
