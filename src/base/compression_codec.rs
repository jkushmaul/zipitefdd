/// Compression codecs used for files
#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Default)]
pub enum CompressionCodec {
    /// No compression
    #[default]
    STORE,
    /// Deflate
    DEFLATE,
    /// Bz2
    BZIP2,
    /// Gzip
    GZIP,
    /// XZ
    XZ,
}
