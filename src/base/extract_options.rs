use effitfs::PathBuf;

use super::result::ArchiveResult;

/// Used for extraction to specify the target directory and how many prefixes to strip
#[derive(Clone, Debug, Default)]
pub struct ExtractOptions {
    /// The target directory, defaults to working dir
    pub target_directory: Option<String>,

    /// Strip a prefix from extracted files
    pub strip_prefix: Option<u8>,

    /// Use permissions contained in tar.
    /// default: false; makes no attempt to sync permissions from entry attributes
    pub use_permissions: bool,

    /// Use ownership contained in tar.
    /// default: false; makes no attempt to sync uid/gid from entry attributes
    pub use_ownership: bool,

    /// Use timestamps contained in tar.
    /// default: false; makes no attempt to sync mtim, atime, ctime from entry attributes
    pub use_timestamps: bool,
}
impl ExtractOptions {
    /// Safely creates a relative path onto extract options target directory
    pub fn build_entry_path(&self, entry_path: &str) -> ArchiveResult<PathBuf> {
        //Removes all ../. etc
        let relative_path = PathBuf::from(entry_path);
        //Removes any root
        let relative_path = relative_path.make_relative();
        //It's now a relative path

        let target_path = match &self.target_directory {
            Some(s) => s.as_ref(),
            None => ".",
        };
        let mut target_path = PathBuf::from(target_path);
        target_path.push_str(relative_path.to_string());

        Ok(target_path.normalize())
    }
}

#[cfg(test)]
mod tests {
    use crate::ExtractOptions;

    #[test]
    pub fn test_build_entry_path_default() {
        let options = ExtractOptions {
            target_directory: None,
            strip_prefix: None,
            ..Default::default()
        };
        let p = options.build_entry_path("test").unwrap();
        assert_eq!(p.to_string().as_str(), "test");
    }
}
