use crate::tar::{block::Block, version::VersionHeader};

#[derive(Debug)]
pub struct V7Header {
    block: Block,
}
impl V7Header {
    pub fn new(block: Block) -> Self {
        Self { block }
    }
}

// Apparently just matches posix header layout?
impl VersionHeader for V7Header {
    fn take_block(self) -> Block {
        self.block
    }
    fn get_block(&self) -> &Block {
        &self.block
    }
    fn get_prefix(&self) -> &[u8] {
        self.get_block().get_bytes(345, 155)
    }

    fn get_atime(&self) -> &[u8] {
        &[]
    }

    fn get_ctime(&self) -> &[u8] {
        &[]
    }
}
