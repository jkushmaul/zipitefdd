use crate::tar::{block::Block, version::VersionHeader};

#[derive(Debug)]
pub struct OldGnuHeader {
    block: Block,
}

/* The old GNU format header conflicts with POSIX format in such a way that
POSIX archives may fool old GNU tar's, and POSIX tar's might well be
fooled by old GNU tar archives.  An old GNU format header uses the space
used by the prefix field in a POSIX header, and cumulates information
normally found in a GNU extra header.  With an old GNU tar header, we
never see any POSIX header nor GNU extra header.  Supplementary sparse
headers are allowed, however.  */
impl VersionHeader for OldGnuHeader {
    fn take_block(self) -> Block {
        self.block
    }
    fn get_block(&self) -> &Block {
        &self.block
    }

    fn get_prefix(&self) -> &[u8] {
        &[]
    }

    fn get_atime(&self) -> &[u8] {
        self.get_block().get_bytes(476, 12)
    }

    fn get_ctime(&self) -> &[u8] {
        self.get_block().get_bytes(488, 12)
    }
}
