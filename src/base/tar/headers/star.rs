use crate::tar::{block::Block, version::VersionHeader};

#[derive(Debug)]
pub struct StarHeader {
    block: Block,
}
impl StarHeader {
    pub fn new(block: Block) -> Self {
        Self { block }
    }
}

impl VersionHeader for StarHeader {
    fn take_block(self) -> Block {
        self.block
    }
    fn get_block(&self) -> &Block {
        &self.block
    }
    /*
    // Cuts room out of prefix, for atime and time
    char prefix[131]; /* 345 */
    char atime[12]; /* 476 */
    char ctime[12]; /* 488 */
    */
    fn get_prefix(&self) -> &[u8] {
        self.get_block().get_bytes(345, 131)
    }
    fn get_atime(&self) -> &[u8] {
        self.get_block().get_bytes(476, 12)
    }
    fn get_ctime(&self) -> &[u8] {
        self.get_block().get_bytes(488, 12)
    }
}
