use crate::tar::{block::Block, version::VersionHeader};

#[derive(Debug)]
pub struct PosixHeader {
    block: Block,
}

impl PosixHeader {
    pub fn new(block: Block) -> Self {
        Self { block }
    }
}

impl VersionHeader for PosixHeader {
    fn take_block(self) -> Block {
        self.block
    }
    fn get_block(&self) -> &Block {
        &self.block
    }
    fn get_prefix(&self) -> &[u8] {
        self.get_block().get_bytes(345, 155)
    }

    fn get_atime(&self) -> &[u8] {
        &[]
    }

    fn get_ctime(&self) -> &[u8] {
        &[]
    }
}
