use std::str::FromStr;

use crate::{
    result::{ArchiveError, ArchiveResult},
    ArchiveEntry,
};
use strum::{Display, EnumString};

#[derive(Display, EnumString)]
#[strum(serialize_all = "lowercase")]
pub enum XtHeader {
    Atime,
    Comment,
    Charset,
    Ctime,
    Gid,
    Gname,
    Linkpath,
    Mtime,
    Path,
    Size,
    Uid,
    Uname,

    #[strum(serialize = "GNU.sparse.name")]
    GnuSparseName,
    #[strum(serialize = "GNU.sparse.major")]
    GnuSparseMajor,
    #[strum(serialize = "GNU.sparse.minor")]
    GnuSparseMinor,
    #[strum(serialize = "GNU.sparse.realsize")]
    GnuSparseRealSize,
    #[strum(serialize = "GNU.sparse.numblocks")]
    GnuSparseNumBlocks,
    #[strum(serialize = "GNU.sparse.size")]
    GnuSparseSize,
    #[strum(serialize = "GNU.sparse.offset,")]
    GnuSparseOffset,
    #[strum(serialize = "GNU.sparse.numbytes,")]
    GnuSparseNumBytes,
    #[strum(serialize = "GNU.sparse.map,")]
    GnuSparseMap,
    #[strum(serialize = "GNU.dumpdir")]
    GnuDumpDir,
    #[strum(serialize = "GNU.volume.label")]
    GnuVolumeLabel,
    #[strum(serialize = "GNU.volume.filename")]
    GnuVolumeFilename,
    #[strum(serialize = "GNU.volume.size")]
    GnuVolumeSize,
    #[strum(serialize = "GNU.volume.offset")]
    GnuVolumeOffset,
}

impl XtHeader {
    pub fn parse_time(value: &str) -> ArchiveResult<Option<i64>> {
        if value.is_empty() {
            return Ok(None);
        }
        let secs: f64 = value.parse().map_err(|e| {
            ArchiveError::InvalidData(format!("time string {} was invalid: {}", value, e))
        })?;
        let secs = secs as i64;
        Ok(Some(secs))
    }

    pub fn parse_uid(value: &str) -> ArchiveResult<u32> {
        let uid: u32 = value.parse().map_err(|e| {
            ArchiveError::InvalidData(format!("uid string {} was invalid: {}", value, e))
        })?;
        Ok(uid)
    }
    pub fn parse_gid(value: &str) -> ArchiveResult<u32> {
        let gid: u32 = value.parse().map_err(|e| {
            ArchiveError::InvalidData(format!("gid string {} was invalid: {}", value, e))
        })?;
        Ok(gid)
    }

    pub fn apply(&self, entry: &mut ArchiveEntry, value: &str) -> ArchiveResult<()> {
        match self {
            XtHeader::Comment => {}
            XtHeader::Atime => entry.set_atime(Self::parse_time(value)?),
            XtHeader::Ctime => entry.set_ctime(Self::parse_time(value)?),
            XtHeader::Mtime => entry.set_mtime(Self::parse_time(value)?),
            XtHeader::Uid => entry.set_uid(Some(Self::parse_uid(value)?)),
            XtHeader::Gid => entry.set_gid(Some(Self::parse_gid(value)?)),
            XtHeader::Path => entry.append_name(value),
            XtHeader::Linkpath => entry.append_linkpath(value),
            _ => {
                return Err(ArchiveError::Unsupported(format!(
                    "{} Xheader tab is not supported",
                    self
                )))
            } /* Unsupported at this time

              XtHeader::Charset => todo!(),
              XtHeader::Uname => todo!(),
              XtHeader::Gname => todo!(),
              XtHeader::Size => todo!(),
              XtHeader::GnuSparseName => todo!(),
              XtHeader::GnuSparseMajor => todo!(),
              XtHeader::GnuSparseMinor => todo!(),
              XtHeader::GnuSparseRealSize => todo!(),
              XtHeader::GnuSparseNumBlocks => todo!(),
              XtHeader::GnuSparseSize => todo!(),
              XtHeader::GnuSparseOffset => todo!(),
              XtHeader::GnuSparseNumBytes => todo!(),
              XtHeader::GnuSparseMap => todo!(),
              XtHeader::GnuDumpDir => todo!(),
              XtHeader::GnuVolumeLabel => todo!(),
              XtHeader::GnuVolumeFilename => todo!(),
              XtHeader::GnuVolumeSize => todo!(),
              XtHeader::GnuVolumeOffset => todo!(),
              */
        }
        Ok(())
    }

    pub fn read(xh_str: &[u8]) -> ArchiveResult<Vec<(Self, String)>> {
        let mut result = vec![];

        for line in xh_str.split(|b| *b == b'\n').filter(|l| !l.is_empty()) {
            let linestr = String::from_utf8_lossy(line);
            let tmp: Vec<_> = line.splitn(2, |b| *b == b' ').collect();
            if tmp.len() != 2 {
                return Err(ArchiveError::InvalidData(format!(
                    "Invalid xheader line: Missing space in extended header: {:#x?}, xh_str: {}",
                    line,
                    String::from_utf8_lossy(xh_str)
                )));
            }
            let len = tmp[0];
            let lenstr = String::from_utf8_lossy(len);

            let len: usize = match lenstr.parse() {
                Ok(len) => len,
                Err(e) => {
                    return Err(ArchiveError::InvalidData(format!(
                        "Could not parse length in extended header {:#x?}; {}",
                        len, e
                    )))
                }
            };

            // +1 == \n we split out
            //Does this really care?  We read the length out already...
            if len != line.len() + 1 {
                return Err(ArchiveError::InvalidData(format!(
                    "Invalid length {} for line ({}) '{}', {:#x?}",
                    len,
                    line.len(),
                    linestr,
                    line,
                )));
            }
            let pair = tmp[1];
            let pairstr = String::from_utf8_lossy(pair);

            let tmp: Vec<_> = pair.splitn(2, |b| *b == b'=').collect();
            if tmp.len() != 2 {
                return Err(ArchiveError::InvalidData(format!(
                    "Invalid key/value pair format in extended header line: '{}', {:#x?}",
                    pairstr, pair
                )));
            }
            let key = String::from_utf8(tmp[0].to_vec())?;
            let value = String::from_utf8(tmp[1].to_vec())?;

            let tab = XtHeader::from_str(&key).map_err(|e| {
                ArchiveError::InvalidData(format!(
                    "Invalid xheader key/error parsing '{}'='{}': {}",
                    key, value, e
                ))
            })?;

            result.push((tab, value.to_owned()));
        }

        Ok(result)
    }
}
