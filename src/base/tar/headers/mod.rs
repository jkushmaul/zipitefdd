pub mod old_gnu;
pub mod posix;
pub mod star;
pub mod vseven;
pub mod xheader;

use crate::result::ArchiveResult;

use super::{constants, parse, version::VersionHeader};
use posix::PosixHeader;
use star::StarHeader;
use vseven::V7Header;

#[derive(Debug)]
pub enum HeaderFormat {
    Version7(V7Header),
    // I'm not entirely sure why these are all posix
    OldGnu(PosixHeader),
    Gnu(PosixHeader),
    Posix(PosixHeader),
    Star(StarHeader),
    Ustar(StarHeader),
}

impl HeaderFormat {
    pub fn detect_format(posix_header: PosixHeader, hbits: bool) -> ArchiveResult<Self> {
        if posix_header.get_magic() != constants::MAGIC_TAR_USTAR {
            //Version 7 has no magic

            return Ok(HeaderFormat::Version7(V7Header::new(
                posix_header.take_block(),
            )));
        }

        if posix_header.get_version() == constants::MAGIC_TAR_VERSION_OLDGNU {
            if hbits {
                return Ok(HeaderFormat::OldGnu(posix_header));
            } else {
                return Ok(HeaderFormat::Gnu(posix_header));
            }
        }

        let star_header = StarHeader::new(posix_header.take_block());

        if star_header.get_prefix()[130] == 0
            && parse::is_octal(star_header.get_atime()[0])
            && star_header.get_atime()[11] == b' '
            && parse::is_octal(star_header.get_ctime()[0])
            && star_header.get_ctime()[11] == b' '
        {
            return Ok(HeaderFormat::Star(star_header));
        }

        //When xattr is supported
        //if stat_info.xhdr.size > 0 {
        //    return Ok(TarVersion::Posix);
        //}

        Ok(HeaderFormat::Posix(PosixHeader::new(
            star_header.take_block(),
        )))
    }
}

pub enum HeaderResult {
    End,
    Entry(Box<dyn VersionHeader>),
    ExtendedHeader(Box<dyn VersionHeader>),
    LongName(Box<dyn VersionHeader>),
    LongLink(Box<dyn VersionHeader>),
}

pub enum BlockResult {
    End,
    Header(Box<dyn VersionHeader>),
}
