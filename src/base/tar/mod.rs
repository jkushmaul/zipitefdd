pub mod block;
pub mod headers;
pub mod parse;
pub mod type_flags;
pub mod version;

pub mod constants {
    pub const BLOCK_SIZE: usize = 512;
    //ustar
    pub const MAGIC_TAR_USTAR: [u8; 5] = [0x75, 0x73, 0x74, 0x61, 0x72];
    //\000
    pub const MAGIC_TAR_VERSION_POSIX: [u8; 3] = [0x00, 0x30, 0x30];
    //  \0
    pub const MAGIC_TAR_VERSION_OLDGNU: [u8; 3] = [0x20, 0x20, 0x00];

    pub const SUPPORTED_FILE_EXTENSIONS: [&str; 6] =
        ["tar", "tar.gz", "tar.xz", "tgz", "tar.bz2", "tar.bz"];
}
