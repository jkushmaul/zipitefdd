use crate::{
    result::{ArchiveError, ArchiveResult},
    tar::constants,
};

use super::{
    headers::{posix::PosixHeader, BlockResult, HeaderFormat},
    version::VersionHeader,
};

/**
 *
 * Tars are 512 byte blocks.
 * Each header is one block
 * Each file, may start on a block, and extend into a block with unused space to the next block.
 */

#[derive(Debug, Clone)]
pub struct Block {
    bytes: [u8; constants::BLOCK_SIZE],
}

impl Block {
    pub fn get_bytes(&self, offset: usize, len: usize) -> &[u8] {
        &self.bytes[offset..offset + len]
    }
    pub fn calculate_checksum(&self) -> i64 {
        let mut checksum: i64 = 0;
        for c in &self.bytes {
            checksum += *c as i64;
        }
        checksum
    }

    pub fn bytes_mut(&mut self) -> &mut [u8] {
        &mut self.bytes
    }

    pub fn decode_block(self) -> ArchiveResult<BlockResult> {
        let header = PosixHeader::new(self);

        let checksum = header.verify_checksum()?;
        if checksum == 0 {
            return Ok(BlockResult::End);
        }

        let (_, hbits) = header.parse_mode()?;
        let format = HeaderFormat::detect_format(header, hbits)?;
        let header: Box<dyn VersionHeader> = match format {
            HeaderFormat::Posix(h) => Box::new(h) as Box<dyn VersionHeader>,
            HeaderFormat::Gnu(h) => Box::new(h) as Box<dyn VersionHeader>,
            HeaderFormat::OldGnu(h) => Box::new(h) as Box<dyn VersionHeader>,
            HeaderFormat::Ustar(h) => Box::new(h) as Box<dyn VersionHeader>,
            HeaderFormat::Version7(h) => Box::new(h) as Box<dyn VersionHeader>,
            _ => {
                return Err(ArchiveError::Unsupported(format!(
                    "TarFormat {:?} not supported yet",
                    format
                )))
            }
        };

        Ok(BlockResult::Header(header))
    }
}

impl Default for Block {
    fn default() -> Self {
        Self {
            bytes: [0u8; constants::BLOCK_SIZE],
        }
    }
}
