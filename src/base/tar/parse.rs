use crate::result::{ArchiveError, ArchiveResult};
use std::ops::RangeInclusive;

/*
Some wonky shit going on in tar man.  I don't know who was driving the car when this was created but don't drink and drive.
*/

pub fn trim_byte(mut chars: &[u8], prefix: u8) -> &[u8] {
    //find first non prefix char from front
    for (i, c) in chars.iter().enumerate() {
        if *c != prefix {
            chars = &chars[i..];
            break;
        }
    }
    // find first non prefix char from back
    for (i, c) in chars.iter().enumerate().rev() {
        if *c != prefix {
            //because it's rev(), it's inclusive range.
            chars = &chars[0..=i];
            break;
        }
    }

    chars
}

pub fn is_octal(byte: u8) -> bool {
    (b'0'..b'8').contains(&byte)
}

pub fn parse_string(chars: &[u8]) -> String {
    let chars = match chars.iter().enumerate().find(|(_, c)| **c == 0) {
        Some((i, _)) => &chars[0..i],
        None => chars,
    };

    String::from_utf8_lossy(chars).to_string()
}

pub fn parse_octal(chars: &[u8], _range: &RangeInclusive<i64>) -> ArchiveResult<i64> {
    let mut overflow = false;
    let mut value: i64 = 0;
    for c in chars.iter() {
        if *c == 0 {
            break;
        } else if !is_octal(*c) {
            return Err(ArchiveError::InvalidData("Expected octal".to_string()));
        }
        value <<= 3;
        let digit = c - b'0';
        value += digit as i64;
        if !overflow {
            overflow = value != ((value << 3) >> 3);
        }
    }

    if overflow {
        return Err(ArchiveError::InvalidData("Overlfow detected".to_string()));
    }

    Ok(value)
}

fn parse_base64(mut chars: &[u8], _range: &RangeInclusive<i64>) -> ArchiveResult<i64> {
    let first_char = match chars.first() {
        Some(c) => {
            chars = &chars[1..];
            *c
        }
        None => return Ok(0),
    };

    let mut value: i64 = 0;
    for (i, c) in chars.iter().rev().enumerate() {
        let c = *c;
        let digit: u8 = if c.is_ascii_uppercase() {
            c - b'A'
        } else if c.is_ascii_lowercase() {
            c - b'a' + 26
        } else if c.is_ascii_digit() {
            c - b'0' + 26 * 2
        } else if c == b'+' {
            62
        } else if c == b'/' {
            63
        } else {
            return Err(ArchiveError::InvalidData(
                "Expected base64 digits".to_string(),
            ));
        };
        let digit: i64 = digit as i64;

        value += digit << (6 * i);
    }

    if first_char == b'-' {
        value *= -1;
    }

    Ok(value)
}

pub fn parse_base256(chars: &[u8], _range: &RangeInclusive<i64>) -> ArchiveResult<i64> {
    let first_char = match chars.first() {
        Some(c) => *c,
        None => return Ok(0),
    };

    const CHAR_BIT: u8 = 8;
    const LG_256: u8 = 8;
    const SIGN_BIT_MASK: u8 = 1 << (LG_256 - 2);
    /* Parse base-256 output.  A nonnegative number N is
    represented as (256**DIGS)/2 + N; a negative number -N is
    represented as (256**DIGS) - N, i.e. as two's complement.
    The representation guarantees that the leading bit is
    always on, so that we don't confuse this format with the
    others (assuming ASCII bytes of 8 bits or more).  */

    let signbit = first_char & SIGN_BIT_MASK;

    let topbits: i64 = (-(signbit as i64)) << (CHAR_BIT * 8 - 2 * LG_256 - 2);

    let mut value: i64 = ((first_char & (SIGN_BIT_MASK - 1)) - signbit) as i64;

    for (_, c) in chars.iter().enumerate().skip(1) {
        let c = *c;
        value = (value << LG_256) + c as i64;

        if ((value << LG_256 >> LG_256) | topbits) != value {
            return Err(ArchiveError::InvalidData(
                "Expected base256 data".to_string(),
            ));
        }
    }
    if signbit != 0 {
        value = -value;
    }
    Ok(value)
}

pub fn parse_time(seconds_chars: &[u8]) -> ArchiveResult<Option<i64>> {
    if seconds_chars.is_empty() {
        return Ok(None);
    }
    let seconds = parse_digits(seconds_chars, false, 0..=i64::MAX)?;
    Ok(Some(seconds))
}
/* Convert buffer at WHERE0 of size DIGS from external format to
intmax_t.  DIGS must be positive.  If TYPE is nonnull, the data are
of type TYPE.  The buffer must represent a value in the range
MINVAL through MAXVAL; if the mathematically correct result V would
be greater than INTMAX_MAX, return a negative integer V such that
(uintmax_t) V yields the correct result.  If OCTAL_ONLY, allow only octal
numbers instead of the other GNU extensions.  Return -1 on error,
diagnosing the error if TYPE is nonnull and if !SILENT.  */
pub fn parse_digits(
    chars: &[u8],
    octal_only: bool,
    range: RangeInclusive<i64>,
) -> ArchiveResult<i64> {
    //Accommodate buggy tar of unknown vintage, which outputs leading NUL if the previous field overflows.
    let chars = trim_byte(chars, 0);
    // Accommodate older tars, which output leading spaces.
    let chars = trim_byte(chars, b' ');

    if chars.is_empty() {
        return Ok(0);
    }

    let first_char = match chars.first() {
        Some(c) => *c,
        None => {
            return Err(ArchiveError::InvalidData(
                "Expected another char".to_string(),
            ))
        }
    };

    // octal
    let value = if is_octal(first_char) {
        parse_octal(chars, &range)?
    } else if octal_only {
        return Err(ArchiveError::InvalidData("Expected octal".to_string()));
    } else if first_char == b'+' || first_char == b'-' {
        /* Parse base-64 output produced only by tar test versions
        1.13.6 (1999-08-11) through 1.13.11 (1999-08-23).
        Support for this will be withdrawn in future releases.  */
        parse_base64(chars, &range)?
    } else if first_char == 0x80 || first_char == 0xFF {
        return Err(ArchiveError::Unsupported(
            "Entry found with base256 digits, which is not supported yet".to_string(),
        ));

        //parse_base256(chars, &range)?
    } else {
        return Err(ArchiveError::InvalidData("Unexpected data".to_string()));
    };

    if range.contains(&value) {
        return Ok(value);
    }

    Err(ArchiveError::InvalidData("Out of range".to_string()))
}

#[cfg(test)]
mod test {

    use super::{parse_base64, parse_digits, parse_octal, trim_byte};

    #[test]
    fn test_parse_octal() {
        let chars = "abcdef".as_bytes();
        let range = 0..=0xffff;
        let result = parse_octal(chars, &range);

        if let Ok(s) = result {
            panic!(
                "parse_octal({:?}) should have failed in error but resulted in {}",
                chars, s
            )
        };
    }

    #[test]
    fn test_parse_octal_zero() {
        let chars = "0".as_bytes();
        let range = 0..=0xffff;
        let result = parse_octal(chars, &range).unwrap();
        assert_eq!(result, 0);
    }

    #[test]
    fn test_parse_octal_seven() {
        let chars = "7".as_bytes();
        let range = 0..=0xffff;
        let result = parse_octal(chars, &range).unwrap();
        assert_eq!(result, 7);
    }

    #[test]
    fn test_parse_octal_normal() {
        let chars = "755".as_bytes();
        let range = 0..=0xffff;
        let result = parse_octal(chars, &range).unwrap();
        assert_eq!(result, 493);
    }

    #[test]
    fn test_parse_base64_pos() {
        let chars = "Ab0+/".as_bytes();
        let range = 0..=0xffff;
        let result = parse_base64(chars, &range).unwrap();
        assert_eq!(result, 7_294_911);
    }

    #[test]
    fn test_parse_base64_neg() {
        let chars = "-Ab0+/".as_bytes();
        let range = 0..=0xffff;
        let result = parse_base64(chars, &range).unwrap();
        assert_eq!(result, -7_294_911);
    }

    #[test]
    fn test_trim_byte_spaces() {
        let chars = "  1234   ".as_bytes();
        let result = trim_byte(chars, b' ');
        assert_eq!(result, "1234".as_bytes());
    }

    #[test]
    fn test_trim_byte_null() {
        let chars = "\x00\x001234\x00\x00".as_bytes();
        let result = trim_byte(chars, 0x0);
        assert_eq!(result, "1234".as_bytes());
    }
    /*
        #[test]
        fn test_parse_base256_pos() {
            let chars = &[0x80, 0xAA, 0xBB];
            let range = 0..=0xffff;
            let result = parse_base256(chars, &range).unwrap();
            assert_eq!(result, 8_432_315);
        }

        #[test]
        fn test_parse_base256_neg() {
            let chars = &[0xFF, 0xAA, 0xBB];
            let range = 0..=0xffff;
            let result = parse_base256(chars, &range).unwrap();
            assert_eq!(result, -7_294_911);
        }
    */
    #[test]
    fn test_parse_digits_within_range() {
        let chars = "2".as_bytes();
        let range = 0..=1;
        if let Ok(s) = parse_digits(chars, true, range) {
            panic!(
                "parse_digits should have failed by being out of range: {}",
                s
            )
        };
    }

    #[test]
    fn test_parse_digits_outof_range() {
        let chars = "2".as_bytes();
        let range = 0..=2;
        let result = parse_digits(chars, true, range).unwrap();
        assert_eq!(result, 2);
    }
}
