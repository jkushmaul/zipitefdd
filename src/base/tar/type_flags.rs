//! Flags used in tar files
use crate::result::{ArchiveError, ArchiveResult};
use strum::{Display, FromRepr};

/// TypeFlag seen on tar entries
#[derive(Display, FromRepr, Debug, PartialEq)]
#[repr(u8)]
pub enum TypeFlags {
    /// Regular file
    RegType = b'0',
    /// link
    LinkType = b'1',
    /// Reserved
    SymType = b'2',
    /// Character special
    ChrType = b'3',
    /// Block special
    BlkType = b'4',
    /// Directory
    DirType = b'5',
    /// Fifo special
    FifoType = b'6',
    /// Reserved
    ContType = b'7',
    /// Extended header referring to the next file in the archive
    XhdType = b'x',
    /// Global extended header
    XglType = b'g',
    /// Identifies the *next* file on the tape as having a long linkname.
    GnutypeLonglink = b'K',
    /// Identifies the *next* file on the tape as having a long name.
    GnutypeLongname = b'L',
    /// Solaris extended header
    SolarisXhdtype = b'X',
}
impl TypeFlags {
    /// Convert typeflag from string into TypeFlags
    pub fn parse(typeflag: u8) -> ArchiveResult<TypeFlags> {
        match TypeFlags::from_repr(typeflag) {
            Some(t) => Ok(t),
            None => Err(ArchiveError::InvalidData(format!(
                "Unknown type flag: {}",
                typeflag
            ))),
        }
    }
}
