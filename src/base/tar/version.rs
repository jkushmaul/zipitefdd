use effitfs::FileType;

use crate::{
    result::{ArchiveError, ArchiveResult},
    ArchiveEntry, CompressionCodec,
};

use super::{block::Block, headers::xheader::XtHeader, parse, type_flags::TypeFlags};

pub trait VersionHeader: Send {
    fn take_block(self) -> Block;

    fn get_block(&self) -> &Block;

    fn parse_mode(&self) -> ArchiveResult<(u16, bool)> {
        let chars = self.get_mode();
        let range: std::ops::RangeInclusive<i64> = i64::MIN..=i64::MAX;
        let mode = parse::parse_digits(chars, false, range)? as u32;
        let hbits = (mode & !0o7777) != 0;

        let mode = (mode & 0o777) as u16;

        Ok((mode, hbits))
    }

    // This is a fairly standard set of offsets (There is only oldGnu and ustar I believe that may differ)
    fn get_name(&self) -> &[u8] {
        self.get_block().get_bytes(0, 100)
    }
    fn get_mode(&self) -> &[u8] {
        self.get_block().get_bytes(100, 8)
    }
    fn get_uid(&self) -> &[u8] {
        self.get_block().get_bytes(108, 8)
    }
    fn get_gid(&self) -> &[u8] {
        self.get_block().get_bytes(116, 8)
    }
    fn get_size(&self) -> &[u8] {
        self.get_block().get_bytes(124, 12)
    }
    fn get_mtime(&self) -> &[u8] {
        self.get_block().get_bytes(136, 12)
    }
    fn get_chksum(&self) -> &[u8] {
        self.get_block().get_bytes(148, 8)
    }
    fn get_typeflag(&self) -> u8 {
        self.get_block()
            .get_bytes(156, 1)
            .first()
            .copied()
            .unwrap_or_default()
    }
    fn get_linkname(&self) -> &[u8] {
        self.get_block().get_bytes(157, 100)
    }
    fn get_magic(&self) -> &[u8] {
        self.get_block().get_bytes(257, 6)
    }
    fn get_version(&self) -> &[u8] {
        self.get_block().get_bytes(263, 2)
    }
    fn get_uname(&self) -> &[u8] {
        self.get_block().get_bytes(265, 32)
    }
    fn get_gname(&self) -> &[u8] {
        self.get_block().get_bytes(297, 32)
    }
    fn get_devmajor(&self) -> &[u8] {
        self.get_block().get_bytes(329, 8)
    }
    fn get_devminor(&self) -> &[u8] {
        self.get_block().get_bytes(337, 8)
    }

    fn get_prefix(&self) -> &[u8];
    fn get_atime(&self) -> &[u8];
    fn get_ctime(&self) -> &[u8];

    fn get_checksum64(&self) -> ArchiveResult<i64> {
        let checksum_digits = self.get_chksum();
        parse::parse_digits(checksum_digits, true, 0..=u32::MAX as i64)
    }
    fn verify_checksum(&self) -> ArchiveResult<i64> {
        let mut checksum: i64 = self.get_block().calculate_checksum();

        if checksum == 0 {
            return Ok(0);
        }

        let checksum_digits = self.get_chksum();
        for c in checksum_digits {
            checksum = checksum + b' ' as i64 - *c as i64;
        }

        let stored_checksum = self.get_checksum64()?;

        if checksum != stored_checksum {
            return Err(ArchiveError::CrcMismatch("Checksum mismatch".to_owned()));
        }
        if checksum < 0 {
            return Err(ArchiveError::CrcMismatch("Invalid checksum".to_owned()));
        }

        Ok(checksum)
    }

    fn parse_filepath(&self) -> String {
        let prefix = parse::parse_string(self.get_prefix());
        let filename = parse::parse_string(self.get_name());

        if prefix.is_empty() {
            filename
        } else {
            let filename = format!("{}/{}", prefix, filename);
            filename
        }
    }

    fn create_archive_entry(
        &self,
        index: usize,
        data_offset: usize,
        data_len: usize,
        xt_headers: &[(XtHeader, String)],
    ) -> ArchiveResult<ArchiveEntry> {
        let typeflag = TypeFlags::parse(self.get_typeflag())?;
        let filename = self.parse_filepath();

        let file_type = match typeflag {
            TypeFlags::DirType => FileType::Dir,
            TypeFlags::RegType => {
                if filename.ends_with("/") || filename.ends_with("\\") {
                    FileType::Dir
                } else {
                    FileType::File
                }
            }
            TypeFlags::SymType | TypeFlags::LinkType => FileType::SymLink,
            _ => {
                let name = parse::parse_string(self.get_name());
                return Err(ArchiveError::Unsupported(format!(
                    "typeflag: {} for entry index={}, data_offset={}, name={:x?};",
                    typeflag, index, data_offset, name
                )));
            }
        };

        // I don't see how this will ever match anything, or did - I screwed up way back
        let link_target = match file_type {
            FileType::SymLink => Some(parse::parse_string(self.get_linkname())),
            _ => None,
        };

        let mtime = parse::parse_time(self.get_mtime())?;
        let ctime = parse::parse_time(self.get_ctime())?;
        let atime = parse::parse_time(self.get_atime())?;

        let (mode, _) = self.parse_mode()?;
        let mut a = ArchiveEntry::new(
            String::new(),
            index,
            // The tar itself is compressed, I do not know of any tar that compresses the entries
            CompressionCodec::STORE,
            data_offset as u64,
            data_len,
            data_len,
            self.get_checksum64()? as u64,
            mode as u32,
            file_type,
            link_target,
        );
        a.set_mtime(mtime);
        a.set_ctime(ctime);
        a.set_atime(atime);

        for (t, v) in xt_headers {
            t.apply(&mut a, v)?;
        }

        if a.get_name().is_empty() {
            a.append_name(&filename);
        }

        Ok(a)
    }
}
