//! An attempt to convert msdos times, found here: <https://learn.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-dosdatetimetofiletime>

use chrono::NaiveDate;

use crate::result::{ArchiveError, ArchiveResult};

#[derive(Debug, Clone)]
pub struct MsdosDateTime {
    date: u16,
    time: u16,
}

impl MsdosDateTime {
    // Ms-dos uses 1980 for 0 vs 1970 and 2 second precision
    // Unix epoch to 2023-01-01 00:00:00 UTC
    pub const MSDOS_EPOCH: u64 = 315532800;

    /// Create a new msdos date time struct
    pub fn new(date: u16, time: u16) -> Self {
        Self { date, time }
    }
    /// DosDateTimeToFileTime  Year: 9-15 (offset from 1980)
    pub fn year(&self) -> i32 {
        (((self.date >> 9) & 0x7f) + 1980) as i32
    }

    /// DosDateTimeToFileTime  Month: 5-8
    pub fn month(&self) -> u8 {
        ((self.date >> 5) & 0x0f) as u8
    }

    /// DosDateTimeToFileTime  Day: 0-4
    pub fn day(&self) -> u8 {
        let day = self.date & 0x1f;
        day as u8
    }

    /// DosDateTimeToFileTime  Hour: 11-15
    pub fn hour(&self) -> u8 {
        ((self.time >> 11) & 0x1f) as u8
    }
    /// DosDateTimeToFileTime  Minute: 5-10
    pub fn minute(&self) -> u8 {
        ((self.time >> 5) & 0x3f) as u8
    }
    /// DosDateTimeToFileTime  Second: 0-4
    pub fn second(&self) -> u8 {
        // So gd stupid.  2 second precision, so multiply it by two.
        //Off by +1 for some reason ?
        ((self.time & 0x1f) << 1) as u8
    }

    // my off by one error: zipinfo -v  test_files/datatest_files/type\=zip\ compression\=store.zip
    // file last modified on (DOS date/time):          2023 Jun 19 11:06:42
    // file last modified on (UT extra field modtime): 2023 Jun 19 07:06:41 local
    // file last modified on (UT extra field modtime): 2023 Jun 19 11:06:41 UTC
    // Appears to be that there are some extended fields that refine the precision of the file time.
    // I think for now, I'm going to just account for that in the test.
    pub fn unix_epoch(&self) -> ArchiveResult<i64> {
        let year = self.year();
        let month = self.month();
        let day = self.day();

        let hour = self.hour();
        let minute = self.minute();
        let second = self.second();

        let dt = match NaiveDate::from_ymd_opt(year, month as u32, day as u32)
            .and_then(|d| d.and_hms_opt(hour as u32, minute as u32, second as u32))
            .map(|dt| dt.and_utc())
        {
            Some(d) => d,
            None => {
                return Err(ArchiveError::InvalidData(format!(
                    "Could not convert msdos date {:?} to unix_epoch",
                    self
                )))
            }
        };

        Ok(dt.timestamp())
    }
}

#[cfg(test)]
mod test {
    use super::MsdosDateTime;

    #[test]
    pub fn test_mdos_date_year() {
        let date = 22224;

        let actual = MsdosDateTime::new(date, 0).year();
        let expected_year = 2023;
        assert_eq!(actual, expected_year);
    }

    #[test]
    pub fn test_mdos_date_month() {
        let date = 22224;
        let actual = MsdosDateTime::new(date, 0).month();
        let expected = 6;
        assert_eq!(actual, expected);
    }

    #[test]
    pub fn test_mdos_date_day() {
        let date = 22224;

        let actual = MsdosDateTime::new(date, 0).day();
        let expected = 16;
        assert_eq!(actual, expected);
    }

    #[test]
    pub fn test_mdos_time_hour() {
        let time = 41070;

        let actual = MsdosDateTime::new(22224, time).hour();
        let expected = 20;
        assert_eq!(actual, expected);
    }

    #[test]
    pub fn test_mdos_time_minute() {
        let time = 41070;
        let actual = MsdosDateTime::new(22224, time).minute();
        let expected = 0x03;
        assert_eq!(actual, expected);
    }

    #[test]
    pub fn test_mdos_time_second() {
        let time = 41070;

        let actual = MsdosDateTime::new(22224, time).second();
        let expected = 28;
        assert_eq!(actual, expected);
    }

    #[test]
    pub fn test_msdos_date_time_epoch() {
        let time = 41070;
        let date = 22224;

        let actual_epoch = MsdosDateTime::new(date, time).unix_epoch().unwrap();

        // 1686945808 = 2023-06-17 20:03:28 GMT
        let expected_epoch = 1686945808_i64;

        assert_eq!(actual_epoch, expected_epoch);
    }
}
