#!/usr/bin/env bash

if [ -n "$DEBUG" ]; then
    set -x
fi

set -e

export TZ="UTC"

function die() {
    test -z "$1" || echo "$1"
    exit 1
}


function check_reqs() {
    uname -s | grep -vq "_NT" || die "Windows test file generation not supported"

    xz --version > /dev/null || die "xz is required"
    gzip --version > /dev/null || die "gunzip required"

    # Need gnu because test gnu specific extensions *and std tar
    tar --version | grep -q "GNU tar" || die "GNU tar required"

    declare ZIP_DETAILS=""
    ZIP_DETAILS="$(zip --version)"
    echo "$ZIP_DETAILS" | grep -q "Info-ZIP" || die "info-zip required"
    echo "$ZIP_DETAILS" | grep -q "BZIP2_SUPPORT" || die "zip BZIP2_SUPPORT required"
    echo "$ZIP_DETAILS" | grep -q "SYMLINK_SUPPORT" || die "zip SYMLINK_SUPPORT required"

    # When 7z is ready
    #7z -h | grep -q "7-Zip" || die "7z required"
}

function test_bytes() {
    declare -i LEN="${1-26}"
    seq 0 "$LEN" | awk  '{ y = $0 % 255; printf("%c", y) }' | head -c "$LEN"
}

function test_hex() {
    declare -i LEN="${1-26}"
    seq 0 "$LEN" | awk  '{ y = $0 % 255; printf("%x", y) }' | head -c "$LEN"
}

function generate_archive_data() {
    echo "Generating archive test files"

    mkdir -p "test_dir"

    # less than 1 block
    test_bytes 511   > "a_file_511.txt"
    # exactly 1 block
    test_bytes 512   > "b_file_512.txt"
    # more than 1 block
    test_bytes 4099  > "c_file_4099.txt"
    

    # nested
    test_bytes 1  > "test_dir/a_file_1.txt"
    test_bytes 1  > "test_dir/b_file_1.txt"

    # links
    ln -s "c_file_4099.txt" "c_link_4099.ln"

    generate_manifest
}

function generate_manifest() {
  
    # Get checksums first, so manifest reflects correct last access time
    find ./ -type f ! -name "MANIFEST.txt" ! -name "CHECKSUMS.txt" -printf '%P\n' |\
         xargs -I {} bash -c "echo \"\$(sha256sum - < {} | cut -d\" \" -f 1) "{}"\"" > "CHECKSUMS.txt"
  
    # On cygwin this actively updates the last access time...  kind of lame.
    find ./ ! -name "MANIFEST.txt" ! -name "CHECKSUMS.txt" -printf '%P\n' | \
        xargs -I {} stat --printf "type=%F\tmode=%a\tgid=%g\tgname=%G\tuid=%u\tuname=%U\tatime=%X\tctime=%W\tmtime=%Y\tsize=%s\tname=%N\n" "{}" > "MANIFEST.txt"

    # Finally append the manifest checksum
    find ./ -maxdepth 1 -name "MANIFEST.txt" -printf '%P\n' | \
        xargs -I {} bash -c "echo \"\$(sha256sum - < {} | cut -d\" \" -f 1) "{}"\"" >> "CHECKSUMS.txt"

    # There has got to be a better way...
    # Restore access times

    awk $'match($0,/atime=([0-9]+).*name=\'([^\']*)\'/,ary) { printf("\\"@%s\\" \\"%s\\"\\n", ary[1], ary[2]); }' MANIFEST.txt | xargs -I {} bash -c "touch -amchd {}"  > /dev/null
}


function save_archive() {
    local EXT="$1"
    shift
    local OUTPUT_FILE="${GENERATED_ARCHIVE_DIR}/${@}.${EXT}"
    if [ -f "$OUTPUT_FILE" ]; then
         die "$OUTPUT_FILE already exists"
    fi
    tee "$OUTPUT_FILE" > /dev/null
}


function make_zip() {
    local TMP_FILE="$TARGET_DIR/tmp.zip"
    if [ -f "$TMP_FILE" ]; then
        rm -f "$TMP_FILE"
    fi
    ARGS="$@"

    (cd "$TMP_ARCHIVE_DIR" && zip -q -y $ARGS "$TMP_FILE" -r ./)

    cat "$TMP_FILE" && rm -f "$TMP_FILE"
}

function make_tar() {
    local TAR_FILE="$TARGET_DIR/tmp.tar"
     if [ -f "$TAR_FILE" ]; then
        rm -f "$TAR_FILE"
    fi
    local -a ARGS="$1"
    shift

    tar -C "$TMP_ARCHIVE_DIR" $ARGS -cf "$TAR_FILE" ./

    cat "$TAR_FILE" | save_archive "tar" "type=tar" "compression=none" "$@"
    gzip -c <  "$TAR_FILE" | save_archive "tar.gz" "type=tar"  "compression=gzip" "$@"
    xz -z < "$TAR_FILE" | save_archive "tar.xz" "type=tar" "compression=xz" "$@"
    bzip2 -z < "$TAR_FILE" | save_archive "tar.bz2" "type=tar" "compression=bzip2" "$@"
    rm "$TAR_FILE"
}

function generate_archives() {
    # Generate the skeleton test dir
    # Files will have byte sequence of 0-length (binary and ascii possible)
    mkdir -p "$GENERATED_ARCHIVE_DIR"
    gzip -c < "$TMP_ARCHIVE_DIR/c_file_4099.txt" | save_archive "gz" "type=compressed" "compression=gzip"
    xz -z < "$TMP_ARCHIVE_DIR/c_file_4099.txt" | save_archive "xz" "type=compressed" "compression=xz"
    bzip2 -z < "$TMP_ARCHIVE_DIR/c_file_4099.txt" | save_archive "bz2" "type=compressed" "compression=bzip2"

    ################ Tar
    #v7
    # filenames: 99 char max
    # links: 99 char max
    # no special files
    # max group id 7777777 octal
    # no user/group ownership
    #echo "Generating v7 tar"
    #make_tar "-H v7" "type=tar" "version=v7"


    # ustar
    # special files
    # user/group info
    # file length: 256 split before 155 by dir name
    # max sym link 100
    local LONG_FILE_NAME=""
    local LONG_LINK_NAME=""
    echo "Creating ustar long file/link"
    (
        cd "$TMP_ARCHIVE_DIR"
        LONG_FILE_NAME="dir_$(test_hex 49)"
        LONG_FILE_NAME="$LONG_FILE_NAME/dir_$(test_hex 45)"
        LONG_FILE_NAME="$LONG_FILE_NAME/dir_$(test_hex 45)"
        mkdir -p "$LONG_FILE_NAME"
        LONG_FILE_NAME="$LONG_FILE_NAME/long_file_ustar_$(test_hex 80).dat"
        test_bytes 1 > "$LONG_FILE_NAME"
        LONG_LINK_NAME="long_link_ustar_$(test_hex 81).ln"

        # I think this is an MSYS2/windows limitation I am hitting where the symbolic link target cannot be too long
        ln -s "a_file_511.txt" "$LONG_LINK_NAME"

        generate_manifest
    )
    echo "Generating ustar tar"
    make_tar "-H ustar" "type=tar" "version=ustar"

    echo "Generating gnu/pax long file/links"
    (
        cd "$TMP_ARCHIVE_DIR"
        LONG_FILE_NAME="dir_$(test_hex 196)"
        LONG_FILE_NAME="$LONG_FILE_NAME/dir_$(test_hex 196)"
        mkdir -p "$LONG_FILE_NAME"
        LONG_FILE_NAME="$LONG_FILE_NAME/long_file_$(test_hex 200).dat"
        test_bytes 1 > "$LONG_FILE_NAME"

        # max length 100 - I think windows/msys2 limitation...
        LONG_LINK_NAME="long_link_$(test_hex 87).ln"

        ln -s "a_file_511.txt" "$LONG_LINK_NAME"

        generate_manifest
    )

    #echo "Generating pax tar"
    # posix/pax seems to just truncate them
    #make_tar "-H pax" "version=pax"
    echo "Generating posix tar"
    make_tar "-H posix" "version=posix"

    # These handle them fine as gnulongname/link
    echo "Generating default tar"
    make_tar "" "version=default"
    echo "Generating gnu tar"
    make_tar "-H gnu" "version=gnu"
    echo "Generating oldgnu tar"
    make_tar "-H oldgnu"  "version=oldgnu"


    # Multi volume?
    # Sparse

    make_zip -Z deflate | save_archive "zip" "type=zip" "compression=deflate"
    make_zip -Z store | save_archive "zip" "type=zip" "compression=store"
    make_zip -Z bzip2 | save_archive "zip" "type=zip" "compression=bzip2"

    #zip64
    # Not yet - I can't make a reliable test file... (without making one that's >4g...)
    #rm -f tmp.zip
    #zip -q -y -fz - d_file_16000.txt |\
    #     save_archive "zip" "len=16000_compression=deflate_type=zip64"

    # Encrypted Zip
    # Not yet...
    #zip -P 1234 -y -Z deflate tmp.zip -r ./
    #cat tmp.zip | save_archive "zip" "compression=deflate_encrypted=1234" && rm tmp.zip

    ##### Self extracting zip (sfx)?
    # Not yet

    ##### Multi volume/disk zips
    # Not yet

    ##### 7z files
    # Not yet

    ##### Rar files
    # Not yet
}



declare SRC_DIR=""
SRC_DIR="$(readlink -f "$0")"
SRC_DIR="$(dirname "$SRC_DIR")"

declare GENERATED_ARCHIVE_DIR="$SRC_DIR/test_files/datatest_files"
TARGET_DIR="$SRC_DIR/target"
TARGET_DIR="$(readlink -f "$TARGET_DIR")"
test -d "$TARGET_DIR" || { echo "$TARGET_DIR does not exist"; exit 1; }

declare TMP_ARCHIVE_DIR="$TARGET_DIR/tmp-archive"

trap 'failure ${LINENO} "$BASH_COMMAND"' ERR
trap 'test "$?" -eq "0" && exit 0 || echo  "Unexpected exit code: $?"; exit 1; ' EXIT


check_reqs

(
    rm -rf "$GENERATED_ARCHIVE_DIR" "$TMP_ARCHIVE_DIR"
    mkdir -p "$GENERATED_ARCHIVE_DIR" "$TMP_ARCHIVE_DIR"
    cd "$TMP_ARCHIVE_DIR"
    generate_archive_data
)
generate_archives

echo "TMP_ARCHIVE_DIR=$TMP_ARCHIVE_DIR:"
ls "$TMP_ARCHIVE_DIR"
echo ""
echo "GENERATED_ARCHIVE_DIR=$GENERATED_ARCHIVE_DIR:"
ls "$GENERATED_ARCHIVE_DIR"
