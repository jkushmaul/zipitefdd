# Zip-it-E-F-doo-dah

An archive file extraction crate.  Either there was unsupported crates, framework enforcing async crates.  All of which missed my needs so I wrote my own.

## Supported archive types

* tar - posix/pax, gnu, old gnu, v7
* zip - 32 only
* gz, xz, bz2

## Usage

### Blocking IO

```rust
let extract_options = ExtractOptions {
   target_directory: "destination/"
   strip_prefix: Some(1)
};
let archive_path = "test.tar.xz"
// test.tar.xz:
//    someprefix/
//         somefile

// Create a fs provider
let fs = SyncFsFileProvider::default();
// Use it briefly, to read a manifest
let archive_file: ArchiveManifest =
            SyncArchiveReader::read_manifest(&archive_path_str, &fs)?;
// Then use the manifest to enumerate entries etc without any IO


// Create a reader for the manifest for the fs
let archive_reader = SyncArchiveReader::new(&archive_file, &mut fs);

// And unpack to another fs
let out_fs = SyncFsFileProvider::default();
archive_reader
            .unpack_all(&mut out_fs, &extract_options)?;

```

### Non-Blocking IO

```rust
let extract_options = ExtractOptions {
   target_directory: "destination/"
   strip_prefix: Some(1)
};
let archive_path = "test.tar.xz"
// test.tar.xz:
//    someprefix/
//         somefile

// Create a fs provider
let fs = AsyncFsFileProvider::default();
// Use it briefly, to read a manifest
let archive_file: ArchiveManifest =
            AsyncArchiveReader::read_manifest(&archive_path_str, &fs).await?;
// Then use the manifest to enumerate entries etc without any IO


// Create a reader for the manifest for the fs
let archive_reader = AsyncArchiveReader::new(&archive_file, &mut fs).await?;

// And unpack to another fs
let out_fs = AsyncFsFileProvider::default();
archive_reader
            .unpack_all(&mut out_fs, &extract_options).await?;

```

`ArchiveManifest` is independent of how it was initially read; it may be read by Sync - and then used with an Async reader or VV.

## Next

* 7z - seems like a good thing to provide support for this too
* RAR - get your warez ready
* zip64 - there is no convenient way to generate a zip64 that I've found, without zipping a 4g file.
* Archive creation

* Could potentially use <https://docs.rs/trait_enum/latest/trait_enum/>
  to wrap the different file type enums

### Seek

It's really lame to have to decompress the entire file over again.  I don't need to be able to seek to random places.
I need to be able to seek to exact positions identified by the headers.  and that sounds like, I should be able to save
some kind of decompression state on that first read.  And then just re-use that next time.
//File stream -> Gzip data - seekable
//Gzip stream -> Tar entry - not seekable
// Tar entry -> bytes - not seekable as a result

I want to be able to index the archive, save positions - with the decompression state,
and then at a later time,

1. Open file.
2. Seek to position in file, for the gzip stream for the entry
3. Restore gzip decompression state for the entry
4. Read the entry data without reading entire file.
5. If a decompression is seekable already, then there isn't any state to remember other than entry position so this would be per compression type.
